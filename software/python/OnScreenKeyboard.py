import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox

from datetime import date
import time



class OnScreenKeyboard:
    def __init__(self, master, buttonFieldToModify):
            self.master = master
            self.frame = tk.Frame(self.master)
            self.isCapital = 0
            self.frame.grid()
            self.returnValue = 0
            self.valueHasBeenEntered = 0
            self.configure_run_keyboard(buttonFieldToModify)
        #modify to include what is currently on the button as starting text in keyboard. Shouldn't be too hard to do

    def keyboardClose(self):
            print("Keyboard should close now")
            self.master.destroy()

    def configure_run_keyboard(self , buttonFieldToModify):
            #isCapital = 0
            buttonWidth = 7
            buttonHeight = 4

            lbl_tempInputField = Label(self.frame, text="")
            lbl_tempInputField.grid(row=0, column=5)

            def toggleCaps():
                tempSwitchVar = 0
                if(self.isCapital == 0):
                    tempSwitchVar = 1
                    btn_keyboard_q.configure(text="Q")
                    btn_keyboard_w.configure(text="W")
                    btn_keyboard_e.configure(text="E")
                    btn_keyboard_r.configure(text="R")
                    btn_keyboard_t.configure(text="T")
                    btn_keyboard_y.configure(text="Y")
                    btn_keyboard_u.configure(text="U")
                    btn_keyboard_i.configure(text="I")
                    btn_keyboard_o.configure(text="O")
                    btn_keyboard_p.configure(text="P")

                    btn_keyboard_a.configure(text="A")
                    btn_keyboard_s.configure(text="S")
                    btn_keyboard_d.configure(text="D")
                    btn_keyboard_f.configure(text="F")
                    btn_keyboard_g.configure(text="G")
                    btn_keyboard_h.configure(text="H")
                    btn_keyboard_j.configure(text="J")
                    btn_keyboard_k.configure(text="K")
                    btn_keyboard_l.configure(text="L")

                    btn_keyboard_z.configure(text="Z")
                    btn_keyboard_x.configure(text="X")
                    btn_keyboard_c.configure(text="C")
                    btn_keyboard_v.configure(text="V")
                    btn_keyboard_b.configure(text="B")
                    btn_keyboard_n.configure(text="N")
                    btn_keyboard_m.configure(text="M")


                elif(self.isCapital == 1):
                    tempSwitchVar = 0
                    btn_keyboard_q.configure(text="q")
                    btn_keyboard_w.configure(text="w")
                    btn_keyboard_e.configure(text="e")
                    btn_keyboard_r.configure(text="r")
                    btn_keyboard_t.configure(text="t")
                    btn_keyboard_y.configure(text="y")
                    btn_keyboard_u.configure(text="u")
                    btn_keyboard_i.configure(text="i")
                    btn_keyboard_o.configure(text="o")
                    btn_keyboard_p.configure(text="p")

                    btn_keyboard_a.configure(text="a")
                    btn_keyboard_s.configure(text="s")
                    btn_keyboard_d.configure(text="d")
                    btn_keyboard_f.configure(text="f")
                    btn_keyboard_g.configure(text="g")
                    btn_keyboard_h.configure(text="h")
                    btn_keyboard_j.configure(text="j")
                    btn_keyboard_k.configure(text="k")
                    btn_keyboard_l.configure(text="l")

                    btn_keyboard_z.configure(text="z")
                    btn_keyboard_x.configure(text="x")
                    btn_keyboard_c.configure(text="c")
                    btn_keyboard_v.configure(text="v")
                    btn_keyboard_b.configure(text="b")
                    btn_keyboard_n.configure(text="n")
                    btn_keyboard_m.configure(text="m")

                self.isCapital = tempSwitchVar

            def keyboard_cmd_a():
                # get text from input field label as string.
                # append letter associated with button
                # update current label value
                currentInput = ""
                if(self.isCapital==0):
                    currentInput = lbl_tempInputField.cget("text") + "a"
                elif(self.isCapital==1):
                    currentInput = lbl_tempInputField.cget("text") + "A"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_b():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "b"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "B"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_c():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "c"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "C"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_d():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "d"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "D"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_e():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "e"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "E"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_f():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "f"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "F"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_g():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "g"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "G"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_h():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "h"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "H"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_i():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "i"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "I"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_j():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "j"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "J"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_k():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "k"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "K"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_l():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "l"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "L"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_m():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "m"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "M"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_n():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "n"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "N"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_o():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "o"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "O"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_p():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "p"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "P"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_q():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "q"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "Q"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_r():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "r"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "R"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_s():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "s"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "S"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_t():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "t"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "T"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_u():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "u"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "U"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_v():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "v"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "V"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_w():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "w"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "W"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_x():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "x"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "X"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_y():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "y"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "Y"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_z():
                currentInput = ""
                if(self.isCapital == 0):
                    currentInput = lbl_tempInputField.cget("text") + "z"
                elif(self.isCapital == 1):
                    currentInput = lbl_tempInputField.cget("text") + "Z"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_delete():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text")[:-1]

                lbl_tempInputField.configure(text = currentInput)

            def keyboard_cmd_underscore():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "_"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_0():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "0"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_1():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "1"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_2():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "2"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_3():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "3"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_4():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "4"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_5():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "5"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_6():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "6"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_7():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "7"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_8():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "8"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_9():
                currentInput = ""
                currentInput = lbl_tempInputField.cget("text") + "9"

                lbl_tempInputField.configure(text=currentInput)

            def keyboard_cmd_todaysDate():
                #get string for todays date followed by underscore yyyymmdd_
                currentInput = ""
                tempCurrentDate = date.today().strftime("%Y%m%d")
                #print(tempCurrentDate)
                currentInput = lbl_tempInputField.cget("text") + tempCurrentDate + "_"

                lbl_tempInputField.configure(text = currentInput)


            def keyboard_enter(buttonFieldToModify):
                tempString = lbl_tempInputField.cget("text")
                #print(tempString)
                buttonFieldToModify.configure(text = tempString)
                #Maybe just have button to open the keyboard. Enter brings back to main window. Then hit login to pass info to user class object and pull directory and file info.
                self.master.destroy()

            #numbersRowOfKeyboard
            btn_keyboard_1 = Button(self.frame, text = "1" , command=keyboard_cmd_1 , width=buttonWidth)
            btn_keyboard_1.grid(row=1 , column = 0)
            btn_keyboard_2 = Button(self.frame, text="2", command=keyboard_cmd_2, width=buttonWidth)
            btn_keyboard_2.grid(row=1, column=1)
            btn_keyboard_3 = Button(self.frame, text="3", command=keyboard_cmd_3, width=buttonWidth)
            btn_keyboard_3.grid(row=1, column=2)
            btn_keyboard_4 = Button(self.frame, text="4", command=keyboard_cmd_4, width=buttonWidth)
            btn_keyboard_4.grid(row=1, column=3)
            btn_keyboard_5 = Button(self.frame, text="5", command=keyboard_cmd_5, width=buttonWidth)
            btn_keyboard_5.grid(row=1, column=4)
            btn_keyboard_6 = Button(self.frame, text="6", command=keyboard_cmd_6, width=buttonWidth)
            btn_keyboard_6.grid(row=1, column=5)
            btn_keyboard_7 = Button(self.frame, text="7", command=keyboard_cmd_7, width=buttonWidth)
            btn_keyboard_7.grid(row=1, column=6)
            btn_keyboard_8 = Button(self.frame, text="8", command=keyboard_cmd_8, width=buttonWidth)
            btn_keyboard_8.grid(row=1, column=7)
            btn_keyboard_9 = Button(self.frame, text="9", command=keyboard_cmd_9, width=buttonWidth)
            btn_keyboard_9.grid(row=1, column=8)
            btn_keyboard_0 = Button(self.frame, text="0", command=keyboard_cmd_0, width=buttonWidth)
            btn_keyboard_0.grid(row=1, column=9)
            btn_keyboard_underscore = Button(self.frame, text="_", command=keyboard_cmd_underscore, width=buttonWidth)
            btn_keyboard_underscore.grid(row=1, column=10)

            #top row of keyboard
            btn_keyboard_q = Button(self.frame, text="q", command=keyboard_cmd_q, width=buttonWidth)
            btn_keyboard_q.grid(row = 2 , column = 0)
            btn_keyboard_w = Button(self.frame, text="w", command=keyboard_cmd_w, width=buttonWidth)
            btn_keyboard_w.grid(row = 2 , column = 1)
            btn_keyboard_e = Button(self.frame, text="e", command=keyboard_cmd_e, width=buttonWidth)
            btn_keyboard_e.grid(row = 2 , column = 2)
            btn_keyboard_r = Button(self.frame, text="r", command=keyboard_cmd_r, width=buttonWidth)
            btn_keyboard_r.grid(row = 2 , column = 3)
            btn_keyboard_t = Button(self.frame, text="t", command=keyboard_cmd_t, width=buttonWidth)
            btn_keyboard_t.grid(row = 2 , column = 4)
            btn_keyboard_y = Button(self.frame, text="y", command=keyboard_cmd_y, width=buttonWidth)
            btn_keyboard_y.grid(row = 2 , column = 5)
            btn_keyboard_u = Button(self.frame, text="u", command=keyboard_cmd_u, width=buttonWidth)
            btn_keyboard_u.grid(row = 2 , column = 6)
            btn_keyboard_i = Button(self.frame, text="i", command=keyboard_cmd_i, width=buttonWidth)
            btn_keyboard_i.grid(row = 2 , column = 7)
            btn_keyboard_o = Button(self.frame, text="o", command=keyboard_cmd_o, width=buttonWidth)
            btn_keyboard_o.grid(row = 2 , column = 8)
            btn_keyboard_p = Button(self.frame, text="p", command=keyboard_cmd_p, width=buttonWidth)
            btn_keyboard_p.grid(row = 2 , column = 9)

            btn_delete = Button(self.frame, text = "Delete" , command = keyboard_cmd_delete , width = buttonHeight)
            btn_delete.grid(row = 2 , column = 10)

            #put delete key next to P button
            #second row of keyboard
            btn_keyboard_a = Button(self.frame, text="a", command=keyboard_cmd_a, width=buttonWidth)
            btn_keyboard_a.grid(row = 3 , column = 1)
            btn_keyboard_s = Button(self.frame, text="s", command=keyboard_cmd_s, width=buttonWidth)
            btn_keyboard_s.grid(row = 3 , column = 2)
            btn_keyboard_d = Button(self.frame, text="d", command=keyboard_cmd_d, width=buttonWidth)
            btn_keyboard_d.grid(row = 3 , column = 3)
            btn_keyboard_f = Button(self.frame, text="f", command=keyboard_cmd_f, width=buttonWidth)
            btn_keyboard_f.grid(row = 3 , column = 4)
            btn_keyboard_g = Button(self.frame, text="g", command=keyboard_cmd_g, width=buttonWidth)
            btn_keyboard_g.grid(row = 3 , column = 5)
            btn_keyboard_h = Button(self.frame, text="h", command=keyboard_cmd_h, width=buttonWidth)
            btn_keyboard_h.grid(row = 3 , column = 6)
            btn_keyboard_j = Button(self.frame, text="j", command=keyboard_cmd_j, width=buttonWidth)
            btn_keyboard_j.grid(row = 3 , column = 7)
            btn_keyboard_k = Button(self.frame, text="k", command=keyboard_cmd_k, width=buttonWidth)
            btn_keyboard_k.grid(row = 3 , column = 8)
            btn_keyboard_l = Button(self.frame, text="l", command=keyboard_cmd_l, width=buttonWidth)
            btn_keyboard_l.grid(row = 3 , column = 9)
            btn_caps_left = Button(self.frame , text = "Caps" , command = toggleCaps , width = buttonWidth)
            btn_caps_left.grid(row = 3, column = 0)
            btn_caps_right = Button(self.frame , text = "Caps" , command = toggleCaps , width = buttonWidth)
            btn_caps_right.grid(row = 3 , column = 10)

            #third row of keyboard
            btn_keyboard_z = Button(self.frame, text="z", command=keyboard_cmd_z, width=buttonWidth)
            btn_keyboard_z.grid(row = 4 , column = 2)
            btn_keyboard_x = Button(self.frame, text="x", command=keyboard_cmd_x, width=buttonWidth)
            btn_keyboard_x.grid(row = 4 , column = 3)
            btn_keyboard_c = Button(self.frame, text="c", command=keyboard_cmd_c, width=buttonWidth)
            btn_keyboard_c.grid(row = 4 , column = 4)
            btn_keyboard_v = Button(self.frame, text="v", command=keyboard_cmd_v, width=buttonWidth)
            btn_keyboard_v.grid(row = 4 , column = 5)
            btn_keyboard_b = Button(self.frame, text="b", command=keyboard_cmd_b, width=buttonWidth)
            btn_keyboard_b.grid(row = 4 , column = 6)
            btn_keyboard_n = Button(self.frame, text="n", command=keyboard_cmd_n, width=buttonWidth)
            btn_keyboard_n.grid(row = 4 , column = 7)
            btn_keyboard_m = Button(self.frame, text="m", command=keyboard_cmd_m, width=buttonWidth)
            btn_keyboard_m.grid(row = 4 , column = 8)
            btn_keyboard_date = Button(self.frame, text="Date" , command=keyboard_cmd_todaysDate , width = buttonWidth)
            btn_keyboard_date.grid(row=4 , column = 0)

            #any other keys
            btn_keyboard_enter = Button(self.frame , text = "Enter" , command = lambda: keyboard_enter(buttonFieldToModify) , width = buttonWidth)
            btn_keyboard_enter.grid(row = 5 , column = 5)