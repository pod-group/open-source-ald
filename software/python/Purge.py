from Parameter import Parameter


from MFC import MFC

from Solenoid import Solenoid

class Purge:
    def __init__(self):
        self.number = 0
        self.delay = 0
        self.duration = 0
        self.stabilisationTime = 0
        self.massFlowController = MFC()
        self.percentMassFlowRate = 0.0
        self.shutoffSolenoid = Solenoid()
        self.shutoffSolenoid.number = 13

    def updatePurgeParameters(self , inputPrecursor, inputMFC):
        self.delay = inputPrecursor.purgeDelay.value
        self.duration = inputPrecursor.purgeTime.value
        self.stabilisationTime = inputPrecursor.stabilisationTime.value
        self.percentMassFlowRate = inputMFC.currentSetPointFlowRateParam.value

    def purgeStart(self , whichArduino , tempCurrentUser):
        #set MFC value to set point
        self.massFlowController.setFlowrate(self.percentMassFlowRate , whichArduino , tempCurrentUser)
        self.shutoffSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)    #currently Normally Open vavles on MFCs. Change this to 100 for when these are changed to Normally Closed


    def purgeStop(self, whichArduino , tempCurrentUser):
        self.massFlowController.setFlowrate( 0, whichArduino, tempCurrentUser)
        self.shutoffSolenoid.actuateSolenoid(100, whichArduino, tempCurrentUser)    #currently Normally Open vavles on MFCs. Change this to 0 for when these are changed to Normally Closed

