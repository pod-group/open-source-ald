import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox
import array

import numpy as np

# #from Tkinter import *
# import matplotlib
# matplotlib.use("TkAgg")
# from matplotlib import pyplot as plt
# from matplotlib.figure import Figure
# import matplotlib.animation as animation
# from matplotlib import style
# from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import serial
from serial import Serial
import serial.tools.list_ports
import re

from datetime import date
import time
import os
import shutil
import csv
import multiprocessing
import threading
import logging

from Parameter import Parameter
from MFC import MFC
from Precursor import Precursor
from Purge import Purge
from Gauge import Gauge
from Solenoid import Solenoid
from cmd import cmd
from User import User
from ArduinoObject import ArduinoObject
from DepositionParameterClass import DepositionParameterClass
from SystemConfiguration import SystemConfiguration
from SystemState import SystemState
from NumPad import NumPad
from OnScreenKeyboard import OnScreenKeyboard
from SearchForAvailableSerial import searchForAvailableSerial
from processThread import depositionProcessThreadClass





#define and initialise any global variables I need for the program
#-----------------------------------
globalStorageVar = 0
actionBrackets = ['<' , '>']
queryBrackets = ['?' , '?']
panicBrackets = ['!' , '!']
startBrackets = [actionBrackets[0] , queryBrackets[0] , panicBrackets[0]]
endBrackets = [actionBrackets[1] , queryBrackets[1] , panicBrackets[1]]
solenoid_MFC_0 = 0
solenoid_MFC_1 = 1
solenoid_MFC_2 = 2
solenoid_MFC_3 = 3

#___________________________________


class mainWindow:
    def __init__(self , master):
        self.currentUser = User()
        self.depositionParams = DepositionParameterClass()
        self.depositionStarted = 0
        self.depositionStopped = 0
        self.depositionLogDirectory = ""
        self.master = master
        self.frame = tk.Frame(master)
        self.frame.grid()
        #self.master.title = "Main Window Title"
        self.tab_control = ttk.Notebook(self.frame)
        self.tab_0 = ttk.Frame(self.tab_control)
        self.tab_1 = ttk.Frame(self.tab_control)
        self.tab_2 = ttk.Frame(self.tab_control)
        self.tab_3 = ttk.Frame(self.tab_control)
        self.tab_4 = ttk.Frame(self.tab_control)
        self.tab_5 = ttk.Frame(self.tab_control)  # Pressure Graphing
        self.tab_6 = ttk.Frame(self.tab_control)  # MFC Graphing
        self.tab_control.add(self.tab_0, text="Setup")
        self.tab_control.add(self.tab_1, text="Deposition Setup")
        self.tab_control.add(self.tab_2, text="Precursor Parameters")
        self.tab_control.add(self.tab_3, text="Gas Flow Parameters")
        self.tab_control.add(self.tab_4, text="Start and Monitor")
        #self.tab_control.add(self.tab_5, text="Pressure Graphing")
        #self.tab_control.add(self.tab_6, text="MFC Graphing")
        # show tab bar
        self.tab_control.pack(expand=1, fill="both")
        self.availableSerialPorts = searchForAvailableSerial()
        self.systemState = SystemState()
        self.arduinoSerialPort = ""
        self.arduinoBaudRate = 2000000
        self.arduino = ArduinoObject(self.arduinoSerialPort , self.arduinoBaudRate)
        self.configure_GUI()





    def configure_GUI(self):
        # tab_control = ttk.Notebook(self.frame)
        # tab_0 = ttk.Frame(tab_control)
        # tab_1 = ttk.Frame(tab_control)
        # tab_2 = ttk.Frame(tab_control)
        # tab_3 = ttk.Frame(tab_control)
        # tab_4 = ttk.Frame(tab_control)
        # tab_5 = ttk.Frame(tab_control)#Pressure Graphing
        # tab_6 = ttk.Frame(tab_control)#MFC Graphing
        # tab_control.add(tab_0, text="Setup")
        # tab_control.add(tab_1, text="Deposition Setup")
        # tab_control.add(tab_2, text="Precursor Parameters")
        # tab_control.add(tab_3, text="Gas Flow Parameters")
        # tab_control.add(tab_4, text="Start and Monitor")
        # tab_control.add(tab_5, text="Pressure Graphing")
        # tab_control.add(tab_6, text="MFC Graphing")
        # # show tab bar
        # tab_control.pack(expand=1, fill="both")

        # add menu bar, not sure what I will need it for but it makes me look pro
        # not sure how this works in mac os
        # menuTitle.add_separator()
        # add separator line in menu bar.
        # new_item.add_command(label='New', command=clicked)
        # syntax for assigning commands to menu items

        menu_00 = Menu(self.frame)  # define menu bar

        file_menu = Menu(menu_00)  # add item to menu bar
        file_menu.add_command(
            label="New Command")  # add item to drop down menu, then need to generate instance of a cascade

        program_menu = Menu(menu_00)
        program_menu.add_command(label="Information")
        program_menu.add_command(label="Quit")
        program_menu.add_command(label="Maintenance Login")

        edit_menu = Menu(menu_00)
        edit_menu.add_command(label="First label here")

        menu_00.add_cascade(label="ALD-AZ", menu=program_menu)  # generate a dropdown menu from items defined before
        menu_00.add_cascade(label="File", menu=file_menu)
        menu_00.add_cascade(label="Edit", menu=edit_menu)
        #self.frame.configure(menu=menu_00)

        #any other setup stuff for main window here
        #------------------------------------------

        #------------------------------------------

        #First Tab
        #------------------------------------------
        def logIn():
            #gets username from currentUser.name and checks to see if the path exists. Creates path if not.
            #if path needs to be created, creates path and then copies all recipes from standardRecipes folder. Maybe also check to see if any other ones have been added to the standardRecipes folder in the meantime and then add them if necessary
            #Check directory and then generate list of process files and save as currentUser.processList use this to generate drop down menu later
            #Change "not logged in"
            #change button that pulls up keyboard to reflect currentuser.name and change state to disabled
            #change state of login button to disabled.
            self.currentUser.name = btn_enterUsername.cget("text")
            #check to see if username is valid - In theory can have a list of approved users and ban people from using it
            #Basically make sure to see if there is a name321
            #print(currentUser.name)
            btn_enterUsername.configure(state = "disabled")
            btn_logInUser.configure(state = "disabled")
            lbl_currentUser.configure(text = "Logged in as: ")
            self.currentUser.baseDirectory = os.path.join(self.currentUser.parentDirectory, "users" , self.currentUser.name)
            #print(self.currentUser.baseDirectory)
            self.currentUser.processDirectory = os.path.join(self.currentUser.baseDirectory ,  "processes")
            #print(self.currentUser.processDirectory)
            self.currentUser.logDirectory = os.path.join(self.currentUser.baseDirectory , "logs")
            self.currentUser.experimentDirectory = os.path.join(self.currentUser.baseDirectory , "experiments")
            #print(currentUser.logDirectory)
            if (os.path.exists(self.currentUser.baseDirectory) != 1):
                os.mkdir(self.currentUser.baseDirectory)  # not sure what to do if directory already exists? Maybe add some logic to append a number to it until the directory is no longer there
                os.mkdir(self.currentUser.processDirectory)
                os.mkdir(self.currentUser.logDirectory)
                os.mkdir(self.currentUser.experimentDirectory)
                #in here copy anything from standard recipes folder (in parentDirectory/standardRecipes/ into currentUser.name/processes/
                standardProcessFilesDirectory = os.path.join(self.currentUser.parentDirectory , "standardProcesses")
                standardProcessFilesList = os.listdir(standardProcessFilesDirectory)
                for fileName in standardProcessFilesList:
                    tempFilename = os.path.join(standardProcessFilesDirectory , fileName)
                    if(os.path.isfile(tempFilename)):
                        shutil.copy(tempFilename , self.currentUser.processDirectory)
            else:
                #print("Path already exists")  # Ultimately want to add in some logic here to continually generate new directories.
                #check to see if there are any files in standardProcesses that arent in currentUser's directory
                #TODO Add in some logic to replace directories that have been removed from users folder. Also overwrite any standard processes that have been edited over time
                standardProcessFilesDirectory = os.path.join(self.currentUser.parentDirectory, "standardProcesses")
                standardProcessFilesList = os.listdir(standardProcessFilesDirectory)
                ##print(standardProcessFilesList)
                for fileName in standardProcessFilesList:
                    tempOriginalFile = os.path.join(standardProcessFilesDirectory , fileName)
                    tempDestinationFile = os.path.join(self.currentUser.processDirectory , fileName)
                    if(os.path.isfile(tempDestinationFile) != 1):
                        shutil.copy(tempOriginalFile , self.currentUser.processDirectory)

            self.currentUser.processList = os.listdir(self.currentUser.processDirectory)
            #print(currentUser.processList)
            #Here reconfigure dropdown menu with the process list of files. Probably need to have that button as a
            btn_startDeposition.configure(text = "Start!" , state="normal")
            updateGUI()

            messagebox.showinfo("Log-In", "Task Failed Successfully")

        def logInAsDifferent():
            #reenable login and usernameEntry buttons
            btn_enterUsername.configure(state = "normal")
            btn_logInUser.configure(state = "normal")

            pass

        def defineExperimentName():
            # have input section for experiment name
            # start with date, then username, then experiment name.
            # concatenate strings and generate filename - will be used for log directory.
            # logs stored in /username/Logs/experimentName/
            # Might just do this in the startProcess() function later on in the setup process.
            pass




        # require login. All other buttons disabled until user logged in.
        btn_enterUsername = Button(self.tab_0 , text = "" , command = lambda:self.getInputFromKeyboard(btn_enterUsername))
        btn_enterUsername.grid(column=1, row=0)
        lbl_currentUser = Label(self.tab_0, text="Not logged in!")
        lbl_currentUser.grid(column=0, row=0)
        btn_logInUser = Button(self.tab_0, text="Log In", command = logIn)
        #btn_openingTime_1 = Button(tab_2, text=(depositionParams.openingTime_1.paramName + ": "),
         #                          command=lambda: self.getInputFromNumPad(depositionParams.openingTime_1,
          #                                                                 btn_openingTime_1))
        btn_logInUser.grid(column=2, row=0)
        btn_usernameReset = Button(self.tab_0, text="Log in as different User", command=logInAsDifferent)
        btn_usernameReset.grid(column=3, row=0)

        lbl_experimentName = Label(self.tab_0, text="Experiment Name: ")
        lbl_experimentName.grid(column=0, row=1)
        btn_experimentName = Button(self.tab_0 , text ="" , command = lambda:self.getInputFromKeyboard(btn_experimentName))
        btn_experimentName.grid(column=1, row=1)
        # build in reset switch. call it log in as someone else. Basically reset to beginning.

        # radio button to either pick existing process OR create new process
        # choose existing process from dropdown menu, will look in username directory and list all processes. All buttons disabled except for layer count.
        processSourceSelect_var = IntVar()  # will use a switch statement on this later.

        def processSourceSelect():
            temp_processSourceSelect_var = processSourceSelect_var.get()
            print(temp_processSourceSelect_var)
            # switch case statements not available in python. Can implement same thing with if-else statements. Its ugly but it should work.
            if (temp_processSourceSelect_var == 0):  # open existing process, do not change params, do not pass go, do not collect $200
                # enable/disable buttons and text fields
                self.currentUser.isNewProcess = 0
                combo_existingFiles.configure(state="normal")
                btn_openExistingFile.configure(state="normal")
                btn_newProcessFilename.configure(state="disabled")
                # also disable all entry fields in other tabs.
            elif (temp_processSourceSelect_var == 1):  # create new process from scratch
                # enable/disable buttons and text fields
                self.currentUser.isNewProcess = 1
                combo_existingFiles.configure(state="disabled")
                btn_openExistingFile.configure(state="disabled")
                btn_newProcessFilename.configure(state="normal")
            elif (temp_processSourceSelect_var == 2):  # edit existing process
                # enable/disable buttons and text fields
                self.currentUser.isNewProcess = 0
                combo_existingFiles.configure(state="normal")
                btn_openExistingFile.configure(state="normal")
                btn_newProcessFilename.configure(state="disabled")
            elif (temp_processSourceSelect_var == 3):  # create new process from existing process
                # enable/disable buttons and text fields
                self.currentUser.isNewProcess = 1
                combo_existingFiles.configure(state="normal")
                btn_openExistingFile.configure(state="normal")
                btn_newProcessFilename.configure(state="normal")

        def openExistingFile():
            # open existing file and populate deposition params
            tempFilenameToOpen = combo_existingFiles.get()
            print(tempFilenameToOpen)
            self.currentUser.currentProcessName = tempFilenameToOpen
            self.depositionParams.readParamsFromFile_setup(self.currentUser , tempFilenameToOpen)
            #opens file, checks that it is properly formatted. Unless people have been fucking with it then no error checking necessary...
            #reads in parameters and assigns them to the correct one. Initially just do it by formatting, eventually build in auto assignment so that can be formatted in any order
            #paramName (as string)      #value (string parsed to float)
            updateGUI()

        def updateGUI():
            #update any buttons that are controlled by class object variables. Broken down into tabs for simplicity
            #tab_0
            combo_existingFiles["values"] = self.currentUser.processList
            #tab_1
            #radio button for deposition mode
            #radio button for deposition type

            #tab_2
            #all the buttons - button label name and then a number reflecting current value of that parameter.
            btn_openingTime_0.configure(text=(self.depositionParams.precursors[0].openingTime.paramName + ": " + str(self.depositionParams.precursors[0].openingTime.value)))
            btn_doseTime_0.configure(text=(self.depositionParams.precursors[0].doseTime.paramName + ": " + str(self.depositionParams.precursors[0].doseTime.value)))
            btn_refillTime_0.configure(text=(self.depositionParams.precursors[0].refillTime.paramName + ": " + str(self.depositionParams.precursors[0].refillTime.value)))
            btn_numDoses_0.configure(text=(self.depositionParams.precursors[0].numDoses.paramName + ": " + str(self.depositionParams.precursors[0].numDoses.value)))
            btn_residenceTime_0.configure(text=(self.depositionParams.precursors[0].residenceTime.paramName + ": " + str(self.depositionParams.precursors[0].residenceTime.value)))
            btn_purgeDelay_0.configure(text=(self.depositionParams.precursors[0].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[0].purgeDelay.value)))
            btn_purgeDuration_0.configure(text=(self.depositionParams.precursors[0].purgeTime.paramName + ": " + str(self.depositionParams.precursors[0].purgeTime.value)))
            btn_stabilisationTime_0.configure(text=(self.depositionParams.precursors[0].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[0].stabilisationTime.value)))
            # ------------------------------------------
            btn_openingTime_1.configure(text=(self.depositionParams.precursors[1].openingTime.paramName + ": " + str(self.depositionParams.precursors[1].openingTime.value)))
            btn_doseTime_1.configure(text=(self.depositionParams.precursors[1].doseTime.paramName + ": " + str(self.depositionParams.precursors[1].doseTime.value)))
            btn_refillTime_1.configure(text=(self.depositionParams.precursors[1].refillTime.paramName + ": " + str(self.depositionParams.precursors[1].refillTime.value)))
            btn_numDoses_1.configure(text=(self.depositionParams.precursors[1].numDoses.paramName + ": " + str(self.depositionParams.precursors[1].numDoses.value)))
            btn_residenceTime_1.configure(text=(self.depositionParams.precursors[1].residenceTime.paramName + ": " + str(self.depositionParams.precursors[1].residenceTime.value)))
            btn_purgeDelay_1.configure(text=(self.depositionParams.precursors[1].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[1].purgeDelay.value)))
            btn_purgeDuration_1.configure(text=(self.depositionParams.precursors[1].purgeTime.paramName + ": " + str(self.depositionParams.precursors[1].purgeTime.value)))
            btn_stabilisationTime_1.configure(text=(self.depositionParams.precursors[1].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[1].stabilisationTime.value)))
            # ------------------------------------------
            btn_openingTime_2.configure(text=(self.depositionParams.precursors[2].openingTime.paramName + ": " + str(self.depositionParams.precursors[2].openingTime.value)))
            btn_doseTime_2.configure(text=(self.depositionParams.precursors[2].doseTime.paramName + ": " + str(self.depositionParams.precursors[2].doseTime.value)))
            btn_refillTime_2.configure(text=(self.depositionParams.precursors[2].refillTime.paramName + ": " + str(self.depositionParams.precursors[2].refillTime.value)))
            btn_numDoses_2.configure(text=(self.depositionParams.precursors[2].numDoses.paramName + ": " + str(self.depositionParams.precursors[2].numDoses.value)))
            btn_residenceTime_2.configure(text=(self.depositionParams.precursors[2].residenceTime.paramName + ": " + str(self.depositionParams.precursors[2].residenceTime.value)))
            btn_purgeDelay_2.configure(text=(self.depositionParams.precursors[2].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[2].purgeDelay.value)))
            btn_purgeDuration_2.configure(text=(self.depositionParams.precursors[2].purgeTime.paramName + ": " + str(self.depositionParams.precursors[2].purgeTime.value)))
            btn_stabilisationTime_2.configure(text=(self.depositionParams.precursors[2].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[2].stabilisationTime.value)))
            # ------------------------------------------
            btn_openingTime_3.configure(text=(self.depositionParams.precursors[3].openingTime.paramName + ": " + str(self.depositionParams.precursors[3].openingTime.value)))
            btn_doseTime_3.configure(text=(self.depositionParams.precursors[3].doseTime.paramName + ": " + str(self.depositionParams.precursors[3].doseTime.value)))
            btn_refillTime_3.configure(text=(self.depositionParams.precursors[3].refillTime.paramName + ": " + str(self.depositionParams.precursors[3].refillTime.value)))
            btn_numDoses_3.configure(text=(self.depositionParams.precursors[3].numDoses.paramName + ": " + str(self.depositionParams.precursors[3].numDoses.value)))
            btn_residenceTime_3.configure(text=(self.depositionParams.precursors[3].residenceTime.paramName + ": " + str(self.depositionParams.precursors[3].residenceTime.value)))
            btn_purgeDelay_3.configure(text=(self.depositionParams.precursors[3].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[3].purgeDelay.value)))
            btn_purgeDuration_3.configure(text=(self.depositionParams.precursors[3].purgeTime.paramName + ": " + str(self.depositionParams.precursors[3].purgeTime.value)))
            btn_stabilisationTime_3.configure(text=(self.depositionParams.precursors[3].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[3].stabilisationTime.value)))
            # ------------------------------------------

            #tab_3
            #probably all the MFC parameters

            #tab_4
            #get system state from Arduino and update gauge pressures, MFC flowrates etc
            #try and add some blinking lights to reflect solenoids etc, maybe another tab with system schematic with gauge locations and pressures

            #maybe update GUI with new graphs as that part is integrated

        def imSuperCereal():
            cerealPortToUse = combo_availableSerial.get()
            print(cerealPortToUse)
            self.arduinoSerialPort = cerealPortToUse
            self.arduino.comms.port = cerealPortToUse
            self.arduino.comms.baudrate = self.arduino.baudrate
            self.arduino.initialise()


            if(self.arduino.isInitialised == 1):
                btn_checkSystemState.configure(state = "normal")
                btn_pumpDown.configure(state="normal")


        def updateCerealList():
            self.availableSerialPorts = searchForAvailableSerial()
            print(self.availableSerialPorts)
            combo_availableSerial["values"] = self.availableSerialPorts[0]
            print("I'm super cereal")


        # define fields for different things
        lbl_openExistingFile = Label(self.tab_0, text="Open Existing File")
        lbl_openExistingFile.grid(column=2, row=3)
        # drop down menu for opening existing files
        combo_existingFiles = Combobox(self.tab_0)
        combo_existingFiles.grid(column=3, row=3)
        combo_existingFiles["values"] = self.currentUser.processList
        #combo_existingFiles.current(0)
        #somehow here figure out how to use currentUser.processList as argument for combobox dropdown menu. Might need to reconfigure this in the function attached to the login button.

        btn_openExistingFile = Button(self.tab_0, text="Open File", command=openExistingFile)
        btn_openExistingFile.grid(column=4, row=3)
        lbl_newProcessFilename = Label(self.tab_0, text="New Process Name")
        lbl_newProcessFilename.grid(column=2, row=4)
        btn_newProcessFilename = Button(self.tab_0 , text = "" , command = lambda:self.getInputFromKeyboard(btn_newProcessFilename))
        btn_newProcessFilename.grid(column=3, row=4)

        lbl_arduinoSerial = Label(self.tab_0 , text = "Arduino Serial Port")
        lbl_arduinoSerial.grid(column=2, row=5)
        combo_availableSerial = Combobox(self.tab_0)
        combo_availableSerial.grid(column = 3, row = 5)
        combo_availableSerial["values"] = self.availableSerialPorts[0]
        btn_useSelectedPort = Button(self.tab_0 , text = "Use Selected" , command = imSuperCereal)
        btn_useSelectedPort.grid(column = 4 , row = 5)
        btn_updateSerialPortList = Button(self.tab_0 , text = "Update List" , command = updateCerealList)
        btn_updateSerialPortList.grid(column = 4 , row = 6)


        processSourceSelect_0 = Radiobutton(self.tab_0, text="Existing Process", value=0, variable=processSourceSelect_var, command=processSourceSelect)
        processSourceSelect_0.grid(column=0, row=2)
        processSourceSelect_1 = Radiobutton(self.tab_0, text="Create New Process", value=1, variable=processSourceSelect_var, command=processSourceSelect)
        processSourceSelect_1.grid(column=0, row=3)
        processSourceSelect_2 = Radiobutton(self.tab_0, text="Edit Existing Process", value=2, variable=processSourceSelect_var, command=processSourceSelect)
        processSourceSelect_2.grid(column=0, row=4)
        processSourceSelect_3 = Radiobutton(self.tab_0, text="Create New Process From Existing", value=3, variable=processSourceSelect_var, command=processSourceSelect)
        processSourceSelect_3.grid(column=0, row=5)
        #------------------------------------------



        #Third Tab
        #------------------------------------------
        lbl_precursor_0 = Label(self.tab_2, text="Precursor 0")
        lbl_precursor_0.grid(column=1, row=0)
        lbl_precursor_1 = Label(self.tab_2, text="Precursor 1")
        lbl_precursor_1.grid(column=2, row=0)
        lbl_precursor_2 = Label(self.tab_2, text="Precursor 2")
        lbl_precursor_2.grid(column=3, row=0)
        lbl_precursor_3 = Label(self.tab_2, text="Precursor 3")
        lbl_precursor_3.grid(column=4, row=0)

        lbl_precursorLocation = Label(self.tab_2 , text = "Precursor Location")
        lbl_precursorLocation.grid(column = 0 , row = 1)
        lbl_openingTime = Label(self.tab_2, text="Opening Time (ms)")
        lbl_openingTime.grid(column=0, row=2)
        lbl_doseTime = Label(self.tab_2, text="Dose Time (s)")
        lbl_doseTime.grid(column=0, row=3)
        lbl_refillTime = Label(self.tab_2, text="Refill Time(s)")
        lbl_refillTime.grid(column=0, row=4)
        lbl_numDoses = Label(self.tab_2, text="numDoses (-)")
        lbl_numDoses.grid(column=0, row=5)
        lbl_residenceTime = Label(self.tab_2, text="Residence Time (s)")
        lbl_residenceTime.grid(column=0, row=6)
        lbl_purgeDelay = Label(self.tab_2, text="Purge Delay (s)")
        lbl_purgeDelay.grid(column=0, row=7)
        lbl_purgeTime = Label(self.tab_2, text="Purge Duration (s_")
        lbl_purgeTime.grid(column=0, row=8)
        lbl_stablilisationTime = Label(self.tab_2, text="Stabilisation Time (s)")
        lbl_stablilisationTime.grid(column=0, row=9)

        #Now all the buttons for Precursor 0
        btn_precursorLocation_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].location.paramName + ": " + str(self.depositionParams.precursors[0].location.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].location , btn_precursorLocation_0))
        btn_precursorLocation_0.grid(column = 1 , row = 1)
        btn_openingTime_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].openingTime.paramName + ": " + str(self.depositionParams.precursors[0].openingTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].openingTime , btn_openingTime_0))
        btn_openingTime_0.grid(column = 1 , row = 2)
        btn_doseTime_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].doseTime.paramName + ": " + str(self.depositionParams.precursors[0].doseTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].doseTime , btn_doseTime_0))
        btn_doseTime_0.grid(column = 1 , row = 3)
        btn_refillTime_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].refillTime.paramName + ": " + str(self.depositionParams.precursors[0].refillTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].refillTime , btn_refillTime_0))
        btn_refillTime_0.grid(column = 1 , row = 4)
        btn_numDoses_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].numDoses.paramName + ": " + str(self.depositionParams.precursors[0].numDoses.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].numDoses , btn_numDoses_0))
        btn_numDoses_0.grid(column = 1 , row = 5)
        btn_residenceTime_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].residenceTime.paramName + ": " + str(self.depositionParams.precursors[0].residenceTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].residenceTime , btn_residenceTime_0))
        btn_residenceTime_0.grid(column = 1 , row = 6)
        btn_purgeDelay_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[0].purgeDelay.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].purgeDelay , btn_purgeDelay_0))
        btn_purgeDelay_0.grid(column = 1 , row = 7)
        btn_purgeDuration_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].purgeTime.paramName + ": " + str(self.depositionParams.precursors[0].purgeTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].purgeTime , btn_purgeDuration_0))
        btn_purgeDuration_0.grid(column = 1 , row = 8)
        btn_stabilisationTime_0 = Button(self.tab_2 , text = (self.depositionParams.precursors[0].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[0].stabilisationTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[0].stabilisationTime , btn_stabilisationTime_0))
        btn_stabilisationTime_0.grid(column = 1 , row = 9)
        #------------------------------------------
        btn_precursorLocation_1 = Button(self.tab_2, text=(self.depositionParams.precursors[1].location.paramName + ": " + str(self.depositionParams.precursors[1].location.value)), command=lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].location, btn_precursorLocation_1))
        btn_precursorLocation_1.grid(column=2, row=1)
        btn_openingTime_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].openingTime.paramName + ": " + str(self.depositionParams.precursors[1].openingTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].openingTime , btn_openingTime_1))
        btn_openingTime_1.grid(column = 2 , row = 2)
        btn_doseTime_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].doseTime.paramName + ": " + str(self.depositionParams.precursors[1].doseTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].doseTime , btn_doseTime_1))
        btn_doseTime_1.grid(column = 2 , row = 3)
        btn_refillTime_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].refillTime.paramName + ": " + str(self.depositionParams.precursors[1].refillTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].refillTime , btn_refillTime_1))
        btn_refillTime_1.grid(column = 2 , row = 4)
        btn_numDoses_1 = Button(self.tab_2, text = (self.depositionParams.precursors[1].numDoses.paramName + ": " + str(self.depositionParams.precursors[1].numDoses.value)), command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].numDoses , btn_numDoses_1))
        btn_numDoses_1.grid(column = 2 , row = 5)
        btn_residenceTime_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].residenceTime.paramName + ": " + str(self.depositionParams.precursors[1].residenceTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].residenceTime , btn_residenceTime_1))
        btn_residenceTime_1.grid(column = 2 , row = 6)
        btn_purgeDelay_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[1].purgeDelay.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].purgeDelay , btn_purgeDelay_1))
        btn_purgeDelay_1.grid(column = 2 , row = 7)
        btn_purgeDuration_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].purgeTime.paramName +": " + str(self.depositionParams.precursors[1].purgeTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].purgeTime , btn_purgeDuration_1))
        btn_purgeDuration_1.grid(column = 2 , row = 8)
        btn_stabilisationTime_1 = Button(self.tab_2 , text = (self.depositionParams.precursors[1].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[1].stabilisationTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[1].stabilisationTime , btn_stabilisationTime_1))
        btn_stabilisationTime_1.grid(column=2, row=9)
        # ------------------------------------------
        btn_precursorLocation_2 = Button(self.tab_2, text=(self.depositionParams.precursors[2].location.paramName + ": " + str(self.depositionParams.precursors[2].location.value)), command=lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].location, btn_precursorLocation_2))
        btn_precursorLocation_2.grid(column=3, row=1)
        btn_openingTime_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].openingTime.paramName + ": " + str(self.depositionParams.precursors[2].openingTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].openingTime , btn_openingTime_2))
        btn_openingTime_2.grid(column = 3 , row = 2)
        btn_doseTime_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].doseTime.paramName + ": " + str(self.depositionParams.precursors[2].doseTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].doseTime , btn_doseTime_2))
        btn_doseTime_2.grid(column = 3 , row = 3)
        btn_refillTime_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].refillTime.paramName + ": " + str(self.depositionParams.precursors[2].refillTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].refillTime , btn_refillTime_2))
        btn_refillTime_2.grid(column = 3 , row = 4)
        btn_numDoses_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].numDoses.paramName + ": " + str(self.depositionParams.precursors[2].numDoses.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].numDoses , btn_numDoses_2))
        btn_numDoses_2.grid(column = 3 , row = 5)
        btn_residenceTime_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].residenceTime.paramName + ": " + str(self.depositionParams.precursors[2].residenceTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].residenceTime , btn_residenceTime_2))
        btn_residenceTime_2.grid(column = 3 , row = 6)
        btn_purgeDelay_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[2].purgeDelay.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].purgeDelay , btn_purgeDelay_2))
        btn_purgeDelay_2.grid(column = 3 , row = 7)
        btn_purgeDuration_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].purgeTime.paramName + ": " + str(self.depositionParams.precursors[2].purgeTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].purgeTime , btn_purgeDuration_2))
        btn_purgeDuration_2.grid(column = 3 , row = 8)
        btn_stabilisationTime_2 = Button(self.tab_2 , text = (self.depositionParams.precursors[2].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[2].stabilisationTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[2].stabilisationTime , btn_stabilisationTime_2))
        btn_stabilisationTime_2.grid(column = 3 , row = 9)
        # ------------------------------------------
        btn_precursorLocation_3 = Button(self.tab_2, text=(self.depositionParams.precursors[3].location.paramName + ": " + str(self.depositionParams.precursors[3].location.value)), command=lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].location, btn_precursorLocation_3))
        btn_precursorLocation_3.grid(column=4, row=1)
        btn_openingTime_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].openingTime.paramName + ": " + str(self.depositionParams.precursors[3].openingTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].openingTime , btn_openingTime_3))
        btn_openingTime_3.grid(column = 4 , row = 2)
        btn_doseTime_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].doseTime.paramName + ": " + str(self.depositionParams.precursors[3].doseTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].doseTime , btn_doseTime_3))
        btn_doseTime_3.grid(column = 4 , row = 3)
        btn_refillTime_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].refillTime.paramName + ": " + str(self.depositionParams.precursors[3].refillTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].refillTime , btn_refillTime_3))
        btn_refillTime_3.grid(column = 4 , row = 4)
        btn_numDoses_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].numDoses.paramName + ": " + str(self.depositionParams.precursors[3].numDoses.value)), command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].numDoses , btn_numDoses_3))
        btn_numDoses_3.grid(column = 4 , row = 5)
        btn_residenceTime_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].residenceTime.paramName + ": " + str(self.depositionParams.precursors[3].residenceTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].residenceTime , btn_residenceTime_3))
        btn_residenceTime_3.grid(column = 4 , row = 6)
        btn_purgeDelay_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].purgeDelay.paramName + ": " + str(self.depositionParams.precursors[3].purgeDelay.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].purgeDelay , btn_purgeDelay_3))
        btn_purgeDelay_3.grid(column = 4 , row = 7)
        btn_purgeDuration_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].purgeTime.paramName + ": " + str(self.depositionParams.precursors[3].purgeTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].purgeTime , btn_purgeDuration_3))
        btn_purgeDuration_3.grid(column = 4 , row = 8)
        btn_stabilisationTime_3 = Button(self.tab_2 , text = (self.depositionParams.precursors[3].stabilisationTime.paramName + ": " + str(self.depositionParams.precursors[3].stabilisationTime.value)) , command = lambda: self.getInputFromNumPad(self.depositionParams.precursors[3].stabilisationTime , btn_stabilisationTime_3))
        btn_stabilisationTime_3.grid(column = 4 , row = 9)

        # Second Tab
        # ------------------------------------------
        # operation mode
        depositionType_var = IntVar()
        depositionMode_var = IntVar()

        def selectDepositionType():
            # some sort of switch statement to pass the deposition type to the params struct
            pass

        def selectDepositionMode():
            # blank out fields irrelevant to whichever deposition mode
            temp_depositionMode = depositionMode_var.get()
            if (temp_depositionMode == 0):
                # ------------------------------------------
                btn_openingTime_0.configure(state="normal")
                btn_doseTime_0.configure(state="disabled")
                btn_refillTime_0.configure(state="disabled")
                btn_numDoses_0.configure(state="disabled")
                btn_residenceTime_0.configure(state="disabled")
                btn_purgeDelay_0.configure(state="normal")
                btn_purgeDuration_0.configure(state="normal")
                btn_stabilisationTime_0.configure(state="normal")
                # ------------------------------------------
                btn_openingTime_1.configure(state="normal")
                btn_doseTime_1.configure(state="disabled")
                btn_refillTime_1.configure(state="disabled")
                btn_numDoses_1.configure(state="disabled")
                btn_residenceTime_1.configure(state="disabled")
                btn_purgeDelay_1.configure(state="normal")
                btn_purgeDuration_1.configure(state="normal")
                btn_stabilisationTime_1.configure(state="normal")
                # ------------------------------------------
                btn_openingTime_2.configure(state="normal")
                btn_doseTime_2.configure(state="disabled")
                btn_refillTime_2.configure(state="disabled")
                btn_numDoses_2.configure(state="disabled")
                btn_residenceTime_2.configure(state="disabled")
                btn_purgeDelay_2.configure(state="normal")
                btn_purgeDuration_2.configure(state="normal")
                btn_stabilisationTime_2.configure(state="normal")
                # ------------------------------------------
                btn_openingTime_3.configure(state="normal")
                btn_doseTime_3.configure(state="disabled")
                btn_refillTime_3.configure(state="disabled")
                btn_numDoses_3.configure(state="disabled")
                btn_residenceTime_3.configure(state="disabled")
                btn_purgeDelay_3.configure(state="normal")
                btn_purgeDuration_3.configure(state="normal")
                btn_stabilisationTime_3.configure(state="normal")
                # ------------------------------------------
            elif (temp_depositionMode == 1):
                btn_openingTime_0.configure(state="disabled")
                btn_doseTime_0.configure(state="normal")
                btn_refillTime_0.configure(state="normal")
                btn_numDoses_0.configure(state="normal")
                btn_residenceTime_0.configure(state="normal")
                btn_purgeDelay_0.configure(state="normal")
                btn_purgeDuration_0.configure(state="normal")
                btn_stabilisationTime_0.configure(state="normal")
                # ------------------------------------------
                btn_openingTime_1.configure(state="disabled")
                btn_doseTime_1.configure(state="normal")
                btn_refillTime_1.configure(state="normal")
                btn_numDoses_1.configure(state="normal")
                btn_residenceTime_1.configure(state="normal")
                btn_purgeDelay_1.configure(state="normal")
                btn_purgeDuration_1.configure(state="normal")
                btn_stabilisationTime_1.configure(state="normal")
                # ------------------------------------------
                btn_openingTime_2.configure(state="disabled")
                btn_doseTime_2.configure(state="normal")
                btn_refillTime_2.configure(state="normal")
                btn_numDoses_2.configure(state="normal")
                btn_residenceTime_2.configure(state="normal")
                btn_purgeDelay_2.configure(state="normal")
                btn_purgeDuration_2.configure(state="normal")
                btn_stabilisationTime_2.configure(state="normal")
                # ------------------------------------------
                btn_openingTime_3.configure(state="disabled")
                btn_doseTime_3.configure(state="normal")
                btn_refillTime_3.configure(state="normal")
                btn_numDoses_3.configure(state="normal")
                btn_residenceTime_3.configure(state="normal")
                btn_purgeDelay_3.configure(state="normal")
                btn_purgeDuration_3.configure(state="normal")
                btn_stabilisationTime_3.configure(state="normal")
                # ------------------------------------------


        btn_continuousFlow = Radiobutton(self.tab_1, text="Continuous Flow Mode", value=0, variable=depositionMode_var, command=selectDepositionMode)
        btn_continuousFlow.grid(column=0, row=0)
        btn_continuousFlow = Radiobutton(self.tab_1, text="Exposure Mode", value=1, variable=depositionMode_var, command=selectDepositionMode)
        btn_continuousFlow.grid(column=0, row=1)

        btn_twoPrecursorDeposition = Radiobutton(self.tab_1, text="2 Precursor Deposition", value=0, variable=depositionType_var, command=selectDepositionType)
        btn_twoPrecursorDeposition.grid(column=1, row=0)
        btn_dopedTwoPrecurrsorDeposition = Radiobutton(self.tab_1, text="Doped Two precursor Deposition", value=1, variable=depositionType_var, command=selectDepositionType)
        btn_dopedTwoPrecurrsorDeposition.grid(column=1, row=1)
        btn_threePrecursorSequentialDeposition = Radiobutton(self.tab_1, text="3 Precursor Sequential", value=2, variable=depositionType_var, command=selectDepositionMode())
        btn_threePrecursorSequentialDeposition.grid(column=1, row=2)
        btn_threePrecursorAlternatingDeposition = Radiobutton(self.tab_1, text="3 Precursor Alternating", value=3, variable=depositionType_var, command=selectDepositionMode())
        btn_threePrecursorAlternatingDeposition.grid(column=1, row=3)

        lbl_numCycles = Label(self.tab_1, text="Number of Cycles")
        lbl_numCycles.grid(column=3, row=0)
        lbl_dopantFrequency = Label(self.tab_1, text="Dopant Frequency")
        lbl_dopantFrequency.grid(column=3, row=1)
        btn_numCycles = Button(self.tab_1, width = 10 , text = "0" , command = lambda: self.getInputFromNumPad(self.depositionParams.numCycles , btn_numCycles))
        btn_numCycles.grid(column=4, row=0)
        btn_dopantFrequency = Button(self.tab_1, width = 10 , text = "0" , command = lambda: self.getInputFromNumPad(self.depositionParams.supercycleFrequency_1 , btn_dopantFrequency))
        btn_dopantFrequency.grid(column=4, row=1)

        # ------------------------------------------
        #Fourth Tab
        #------------------------------------------
        lbl_MFC_0 = Label(self.tab_3, text="MFC_0 Setpoint")
        lbl_MFC_0.grid(column=0, row=0)
        lbl_MFC_1 = Label(self.tab_3, text="MFC_0 Setpoint")
        lbl_MFC_1.grid(column=0, row=1)
        lbl_MFC_2 = Label(self.tab_3, text="MFC_0 Setpoint")
        lbl_MFC_2.grid(column=0, row=2)
        lbl_MFC_3 = Label(self.tab_3, text="MFC_0 Setpoint")
        lbl_MFC_3.grid(column=0, row=3)

        btn_MFC_0 = Button(self.tab_3, width=25 , text = "0" , command = lambda: self.getInputFromNumPad(self.depositionParams.massFlowControllers[0].currentSetPointFlowRateParam , btn_MFC_0))
        btn_MFC_0.grid(column=1, row=0)
        btn_MFC_1 = Button(self.tab_3, width=25 , text = "0" , command = lambda: self.getInputFromNumPad(self.depositionParams.massFlowControllers[1].currentSetPointFlowRateParam , btn_MFC_1))
        btn_MFC_1.grid(column=1, row=1)
        btn_MFC_2 = Button(self.tab_3, width=25 , text = "0" , command = lambda: self.getInputFromNumPad(self.depositionParams.massFlowControllers[2].currentSetPointFlowRateParam , btn_MFC_2))
        btn_MFC_2.grid(column=1, row=2)
        btn_MFC_3 = Button(self.tab_3, width=25 , text = "0" , command = lambda: self.getInputFromNumPad(self.depositionParams.massFlowControllers[3].currentSetPointFlowRateParam , btn_MFC_3))
        btn_MFC_3.grid(column=1, row=3)
        #------------------------------------------

        #Fifth Tab
        #------------------------------------------

        def pumpDown():
            self.systemState.solenoids[15].actuateSolenoidNoLog(100, self.arduino, self.currentUser)
            self.systemState.solenoids[12].actuateSolenoidNoLog(100, self.arduino, self.currentUser)
            self.systemState.solenoids[13].actuateSolenoidNoLog(100, self.arduino, self.currentUser)


        def cmd_startDeposition():
            #update self.depositionMode
            self.depositionParams.depositionMode.value = depositionMode_var.get()
            btn_endDeposition.configure(state = "normal")

            self.currentUser.experimentName = btn_experimentName.cget("text")

            tempNewProcessName = btn_newProcessFilename.cget("text")
            self.currentUser.newProcessName = tempNewProcessName

            tempExistingProcessName = combo_existingFiles.cget("text")
            self.currentUser.existingProcessName = tempExistingProcessName



            #print(self.currentUser.experimentName)
            btn_experimentName.configure(state = "disabled")

            #regardless of new process or not - create directory within user/experiments/ with experiment name and appended with Epoch time.
            #create unique location for experiment - will save parameters and logs into this directory.
            self.currentUser.experimentDate = date.today().strftime("%Y%m%d")
            self.currentUser.experimentTime = int(time.time())
            experimentLocationString = self.currentUser.experimentDate + "_" + self.currentUser.experimentName + "_" + str(self.currentUser.experimentTime)
            print(experimentLocationString)
            experimentLocationDirectory = os.path.join(self.currentUser.parentDirectory , 'users' , self.currentUser.name , self.currentUser.experimentDirectory , experimentLocationString)
            print(experimentLocationDirectory)
            os.mkdir(experimentLocationDirectory)
            self.depositionLogDirectory = experimentLocationDirectory #Need this for later in case I need to dump a quit flag/file into it



            #logic based on whether or not saving a new process
            #dump parameters to new file in processes menu
            print(processSourceSelect_var.get())
            if(processSourceSelect_var.get() == 0):
                #run existing process. Do not save new file to processes folder
                self.depositionParams.dumpToFile(experimentLocationDirectory , experimentLocationString)

                #call standalone process here with args [username , experimentName , experimentDate , experimentTime]
                #standalone process creates User object with args
                #from here, User object operated on by params.readFromFile_run
                #params.runDeposition then runs the deposition.
                #Build in some error handling and pause / quit functionality into run function.

            elif(processSourceSelect_var.get() == 1):
                #create brand new process. Save new file to processes folder
                tempDir = os.path.join(self.currentUser.parentDirectory , "users" , self.currentUser.processDirectory)
                self.depositionParams.dumpToFile(tempDir , self.currentUser.newProcessName)
                self.depositionParams.dumpToFile(experimentLocationDirectory, experimentLocationString)

            elif(processSourceSelect_var.get() == 0):
                #edit existing process. Overwrite file in processes folder.
                tempDir = os.path.join(self.currentUser.parentDirectory, "users" , self.currentUser.processDirectory)
                self.depositionParams.dumpToFile(tempDir, self.currentUser.existingProcessName)
                self.depositionParams.dumpToFile(experimentLocationDirectory, experimentLocationString)
            elif(processSourceSelect_var.get() == 3):
                #Create new process from existing file. Save new file to processes folder.
                tempDir = os.path.join(self.currentUser.parentDirectory, "users" , self.currentUser.processDirectory)
                self.depositionParams.dumpToFile(tempDir, self.currentUser.newProcessName)
                self.depositionParams.dumpToFile(experimentLocationDirectory, experimentLocationString)

            else:
                print("dunno how you got here. \n Do not start process. \n Go straight to Jail \n Do not pass Go \n Do not collect $200")



            #dump parameters to here. Will also be the directory where the log happens

            #call main deposition process with file location above.



            print("Deposition process started - Not really because I haven't written that part yet. But GUI side complete!")
            self.arduino.comms = serial.Serial()
            self.arduino.comms.port = self.arduinoSerialPort
            self.arduino.comms.baudrate = self.arduinoBaudRate

            #stringToPass = "python3 runDeposition_standalone.py"  + " " + self.currentUser.name + " " + self.currentUser.experimentName + " " + self.currentUser.experimentDate + " " + str(self.currentUser.experimentTime) + " " + self.arduinoSerialPort + " " + str(self.arduinoBaudRate)
            #print(stringToPass)

            #Pass string to command line - runs other program. Does not create separate instance of python like I thought it would. Works, but not ideal. Want to create threaded process here.
            #os.system(stringToPass)

            depositionThread = threading.Thread(target = self.depositionParams.runDeposition , args=(self.arduino , self.currentUser , self.systemState))
            depositionThread.start()

            #wait for other thread I guess






            #open new window for monitoring deposition - tab for each pressure and flow rate, maybe also temperature
            #one window where current value for all things is shown, as well as current cycle and current cycle state@
            #depending on deposition type, generate different windows for this. Shoudlnt be too hard once I have one working...
            #Figure out a good way of doing a stacked deposition - program automagically starts another deposition at the end of each process.
            #Radio button in GUI. Basically deposition calls itself at end of process to start a new one with new set of parameters. Expt paramSet_0, paramSet_1 etc

            #when its done, switch panik button back to disabled.

        def cmd_endDepositionButton():
            #Deposition program checks every so often to see if there is a file labelled STOP.txt or whatever in the experiment directory.
            print("Panik")
            tempStopFile = open(os.path.join(self.depositionLogDirectory , "stahp.txt") , "a+")

            tempStopFile.write("ALD \nWat r u doin \nALD \nStahp\n")
            tempStopFile.close()



            #in future if I can get this to work:
                #writes file pause.txt to experiment directory
                #other program sees this and throws up a messagebox to continue or abort process


            #return ALD system to a safe state.
            #write LOW to all precursor solenoids and open vacuum pump valve
            #Maybe include option to push nitrogen through as well.
        def updatePressuresFlowRates():
            print("Checking systemState")
            self.systemState.checkNoLog(self.arduino , self.currentUser)
            MFC_0_flowrate = self.systemState.MFCs[0].currentFlowRate
            MFC_1_flowrate = self.systemState.MFCs[1].currentFlowRate
            MFC_2_flowrate = self.systemState.MFCs[2].currentFlowRate
            MFC_3_flowrate = self.systemState.MFCs[3].currentFlowRate
            lbl_MFC_0_flowrate.configure(text = MFC_0_flowrate)
            lbl_MFC_1_flowrate.configure(text = MFC_1_flowrate)
            lbl_MFC_2_flowrate.configure(text = MFC_2_flowrate)
            lbl_MFC_3_flowrate.configure(text = MFC_3_flowrate)

            gauge_0_pressure = self.systemState.gauges[0].currentPressure
            print(gauge_0_pressure)
            gauge_1_pressure = self.systemState.gauges[1].currentPressure
            print(gauge_1_pressure)
            gauge_2_pressure = self.systemState.gauges[2].currentPressure
            print(gauge_2_pressure)
            gauge_3_pressure = self.systemState.gauges[3].currentPressure
            print(gauge_3_pressure)
            lbl_gauge_0_pressure.configure(text=gauge_0_pressure)
            lbl_gauge_1_pressure.configure(text=gauge_1_pressure)
            lbl_gauge_2_pressure.configure(text=gauge_2_pressure)
            lbl_gauge_3_pressure.configure(text=gauge_3_pressure)



        lbl_startDeposition = Label(self.tab_4, text="Start Deposition")
        lbl_startDeposition.grid(column=0, row=0)
        btn_startDeposition = Button(self.tab_4, text="Login First!" , command = cmd_startDeposition)
        btn_startDeposition.grid(column=1, row=0)
        btn_startDeposition.configure(state = "disabled")
        btn_endDeposition = Button(self.tab_4 , text = "panik" , command = cmd_endDepositionButton)
        btn_endDeposition.grid(column = 2 , row = 0)
        btn_endDeposition.configure(state = "disabled")

        #checks system state while no process is running. Then simply updates GUI based on current state given by deposition thread. Chamge lambda:function after self.runDeposition
        btn_checkSystemState = Button(self.tab_4 , text = "Check System State" , command = updatePressuresFlowRates)
        btn_checkSystemState.grid(column = 3 , row = 0)
        btn_checkSystemState.configure(state = "disabled") #disabled until arduino is initialised - i.e. cant ping arduino for system state until it's been initialised. Have this as part of the initialisation function

        btn_pumpDown = Button(self.tab_4 , text = "Pump Down" , command = pumpDown)
        btn_pumpDown.grid(column = 4 , row = 0)
        btn_pumpDown.configure(state = "disabled")


        #btn_stopDeposition = Button(tab_4, text="Stop Deposition")
        #btn_stopDeposition.grid(column=2, row=0)

        lbl_nitrogenFeedPressure = Label(self.tab_4, text="Nitrogen Feed Pressure (bar)")
        lbl_nitrogenFeedPressure.grid(column=0, row=1)
        lbl_nitrogenFeedPressure_value = Label(self.tab_4, text="0.0")
        lbl_nitrogenFeedPressure_value.grid(column=1, row=1)

        MFC_0_flowrate = 0.0
        MFC_1_flowrate = 0.0
        MFC_2_flowrate = 0.0
        MFC_3_flowrate = 0.0

        gauge_0_pressure = 0.0
        gauge_1_pressure = 0.0
        gauge_2_pressure = 0.0
        gauge_3_pressure = 0.0

        lbl_MFC_0 = Label(self.tab_4, text="MFC_0 Flowrate (sccm): ")
        lbl_MFC_0.grid(column=0, row=2)
        lbl_MFC_1 = Label(self.tab_4, text="MFC_1 Flowrate (sccm): ")
        lbl_MFC_1.grid(column=0, row=3)
        lbl_MFC_2 = Label(self.tab_4, text="MFC_2 Flowrate (sccm): ")
        lbl_MFC_2.grid(column=0, row=4)
        lbl_MFC_3 = Label(self.tab_4, text="MFC_3 Flowrate (sccm): ")
        lbl_MFC_3.grid(column=0, row=5)
        # -----------------------------------------------------------
        lbl_MFC_0_flowrate = Label(self.tab_4, text=MFC_0_flowrate)
        lbl_MFC_0_flowrate.grid(column=1, row=2)
        lbl_MFC_1_flowrate = Label(self.tab_4, text=MFC_1_flowrate)
        lbl_MFC_1_flowrate.grid(column=1, row=3)
        lbl_MFC_2_flowrate = Label(self.tab_4, text=MFC_2_flowrate)
        lbl_MFC_2_flowrate.grid(column=1, row=4)
        lbl_MFC_3_flowrate = Label(self.tab_4, text=MFC_3_flowrate)
        lbl_MFC_3_flowrate.grid(column=1, row=5)
        # -----------------------------------------------------------
        lbl_gauge_0 = Label(self.tab_4, text="Gauge_0 Pressure (mbar):")
        lbl_gauge_0.grid(column=2, row=2)
        lbl_gauge_1 = Label(self.tab_4, text="Gauge_1 Pressure (mbar):")
        lbl_gauge_1.grid(column=2, row=3)
        lbl_gauge_2 = Label(self.tab_4, text="Gauge_2 Pressure (mbar):")
        lbl_gauge_2.grid(column=2, row=4)
        lbl_gauge_3 = Label(self.tab_4, text="Gauge_3 Pressure (mbar):")
        lbl_gauge_3.grid(column=2, row=5)
        # -----------------------------------------------------------
        lbl_gauge_0_pressure = Label(self.tab_4, text=gauge_0_pressure)
        lbl_gauge_0_pressure.grid(column=3, row=2)
        lbl_gauge_1_pressure = Label(self.tab_4, text=gauge_0_pressure)
        lbl_gauge_1_pressure.grid(column=3, row=3)
        lbl_gauge_2_pressure = Label(self.tab_4, text=gauge_0_pressure)
        lbl_gauge_2_pressure.grid(column=3, row=4)
        lbl_gauge_3_pressure = Label(self.tab_4, text=gauge_0_pressure)
        lbl_gauge_3_pressure.grid(column=3, row=5)

        #------------------------------------------
    #     btn_graphGauge = Button(self.tab_4, text="Graph Gauge Press",command=lambda: self.graphGaugePressure(self.currentUser, self.currentState))
    #     btn_graphGauge.grid(row=0, column=3)
    #
    # def graphGaugePressure(self , tempCurrentUser , tempSystemState):#probably have graphInterval stored as a paraemter somewhere.
    #     #check to see if log file exists. If yes, open and do graphing. Otherwise skip process and just blindly loop for a hwile until file appears
    #     logFileExists = 0
    #     #currentLogHistory = [][]
    #     #assuming file exits from now on
    #     # if(logFileExists):
    #     #     currentLogHistory = tempSystemState.readInHistory(tempCurrentUser) #get info
    #
    #     #switch statement / whole bunch of if else statements to show what gets plotted on each graph.
    #
    #     #need to start a new thread for this
    #     graphingQuitFlag = multiprocessing.Value('i', int(False))
    #     graphingThread = threading.Thread()
    #
    #     gaugeData = [ [1, 2, 3, 4, 5, 6, 7, 8] , [5, 6, 1, 3, 8, 9, 3, 5] ]
    #     mfcData = [[1, 2, 3, 4, 5, 6, 7, 8], [7, 2, 3, 5, 11, 4, 1, 6]]
    #
    #     def graphingThreadFunction(tempSystemState):
    #         graphGauge = Figure(figsize=(3, 3), dpi=200)
    #         graphGauge_plot = graphGauge.add_subplot(111)
    #         graphGauge_plot.plot(tempSystemState.currentSystemElapsedTime_log[:], tempSystemState.gaugeLog_0[:])
    #
    #         # graphGauge_line, = graphGauge_plot.plot(gaugeData[0][:] , gaugeData[1][:], 'r', marker='o')
    #
    #         def graphGauge_update():
    #             # gauges are 18, 19, 20, 21
    #             graphGauge_plot.plot.set_data(tempSystemState.currentSystemElapsedTime_log, tempSystemState.gaugeLog_0)
    #             print(tempSystemState.currentSystemElapsedTime_log)
    #             print(tempSystemState.gaugeLog_0)
    #
    #         canvas_gauge = FigureCanvasTkAgg(graphGauge, self.tab_5)
    #         canvas_gauge.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)
    #         canvas_gauge._tkcanvas.pack(side=TOP, fill=BOTH, expand=True)
    #         gauge_animation = matplotlib.animation.FuncAnimation(graphGauge, graphGauge_update, interval=1000)
    #
    #         #do the same thing with the MFC data. Build in some features do do different things
    #
    #
    #     #build another thread for the graphing process
    #     graphingQuitFlag.value = False
    #     graphingThread = threading.Thread(target=lambda: graphingThreadFunction(tempSystemState))
    #     graphingThread.start()
    #
    #
    #
    #
    #
    #
    #     #
    #     # f = Figure(figsize=(5, 5), dpi=100)
    #     # a = f.add_subplot(111)
    #     # a.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])
    #     #
    #     # canvas = FigureCanvasTkAgg(f, self.tab_6)
    #     # # canvas.show()
    #     # canvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)
    #     # toolbar = NavigationToolbar2TkAgg(canvas, self)
    #     # toolbar.update()
    #
    #     #pass
    #
    # def threadedGraphParameters(self , tempSystemState , tempCurrentUser):
    #     #start new thread that runs graphing function every tempSystemState.logInterval
    #     #maybe bake looping and updating of graph into other function, this might just start a thread that runs this indefinitely.
    #     #when deposition process is over, thread should end.
    #     pass
    #
    # def graphMFC(self , tempSystemState , tempCurrentUser , graphInterval):
    #         #maybe combine this with the gauge pressure one so I only need to read in the log history once. Will be updating graphs at same time anyway so may as well.
    #     pass
    #
    #
    #     #syntax for doing numpad etnry fields.
    #     # self.btn_01 = Button(self.frame , text = "Input From NumPad" , command = lambda: self.getInputFromNumPad(depositionParams.numCycles , self.btn_01))
    #     # self.btn_01.grid(row = 0 , column = 0)
    #
    #     # test graphing
    #
    def getInputFromNumPad(self , parameterToBeAssigned , buttonToModify):
        #print("buttonToModify")
        #print(type(buttonToModify))
        #function in here. Needs to open new window for numPad interface
        #self.app = numPad(self.newWindow)
        tempWindow_0 = tk.Toplevel(self.master)
        self.app = NumPad(tempWindow_0 , parameterToBeAssigned, buttonToModify)

        #print(toBeAssigned)

    def getInputFromKeyboard(self , buttonFieldToModify):
        tempKeyboardWindow = tk.Toplevel(self.master)
        self.app = OnScreenKeyboard(tempKeyboardWindow , buttonFieldToModify)





#define functions for operation - first functions for setup, then for deposition
def main():
    #root = tkinter.Tk()
    #numpad = NumPad()
    #numPadWindow = tkinter.Tk()
    window_0 = Tk()
    app = mainWindow(window_0)
    window_0.mainloop()
    tempVar = 1







main()