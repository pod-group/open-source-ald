from MFC import MFC
from Gauge import Gauge
from Solenoid import Solenoid
from User import User

from datetime import date
import time
import os
import shutil
import numpy as np

class SystemState:
    def __init__(self):
        self.logInterval = 1
        self.numGauges = 4
        self.numMFCs = 4
        self.numSolenoids = 16
        self.solenoids = [Solenoid() for _ in range(self.numSolenoids)]
        # fill in solenoid information here if necessary
        for i in range(0 , self.numSolenoids):
            self.solenoids[i].number = i
        self.gauges = [Gauge() for _ in range(self.numGauges)]
        #fill in gauge information here if necessary
        for i in range(0 , self.numGauges):
            self.gauges[i].number = i
        self.MFCs = [MFC() for _ in range(self.numMFCs)]
        #fill in MFC information here for use later if necessary
        for i in range(0 , self.numMFCs):
            self.MFCs[i].number = i
        self.currentSystemTime = time.time()
        self.currentSystemElapsedTime = 0
        self.previousElapsedTime = 0
        self.processStartTime = 0
        self.checkStateElapsedTime = 0
        self.logProcessElapsedTime = 0
        #print(self.systemStateHistory)
        self.systemStateHistoryLength = 0

        self.solenoidLog_0 = [0]
        self.solenoidLog_1 = [0]
        self.solenoidLog_2 = [0]
        self.solenoidLog_3 = [0]
        self.solenoidLog_4 = [0]
        self.solenoidLog_5 = [0]
        self.solenoidLog_6 = [0]
        self.solenoidLog_7 = [0]
        self.solenoidLog_8 = [0]
        self.solenoidLog_9 = [0]
        self.solenoidLog_10 = [0]
        self.solenoidLog_11 = [0]
        self.solenoidLog_12 = [0]
        self.solenoidLog_13 = [0]
        self.solenoidLog_14 = [0]
        self.solenoidLog_15 = [0]

        self.gaugeLog_0 = [0]
        self.gaugeLog_1 = [0]
        self.gaugeLog_2 = [0]
        self.gaugeLog_3 = [0]

        self.MFCactualLog_0 = [0]
        self.MFCsetpointLog_0 = [0]
        self.MFCactualLog_1 = [0]
        self.MFCsetpointLog_1 = [0]
        self.MFCactualLog_2 = [0]
        self.MFCsetpointLog_2 = [0]
        self.MFCactualLog_3 = [0]
        self.MFCsetpointLog_3 = [0]

        self.currentSystemTime_log = [0]
        self.currentSystemElapsedTime_log = [0]
        #need to add something here to assign the numbers for the different solenoids, gauges and MFCs

    def check(self ,whichArduino , tempCurrentUser):
        functionStartTime = time.time()
        self.currentSystemTime = time.time()
        self.currentSystemElapsedTime = self.currentSystemTime - self.processStartTime
        for j in range(self.numSolenoids):
            pass
        for j in range(self.numGauges):
            self.gauges[j].getPressure(whichArduino , tempCurrentUser)
        for j in range(self.numMFCs):
            self.MFCs[j].getFlowrate(whichArduino , tempCurrentUser)

        self.checkStateElapsedTime = time.time() - functionStartTime

    def checkNoLog(self ,whichArduino , tempCurrentUser):
        functionStartTime = time.time()
        self.currentSystemTime = time.time()
        self.currentSystemElapsedTime = self.currentSystemTime - self.processStartTime
        for j in range(self.numSolenoids):
            pass
        for j in range(self.numGauges):
            self.gauges[j].getPressureNoLog(whichArduino , tempCurrentUser)
        for j in range(self.numMFCs):
            self.MFCs[j].getFlowrateNoLog(whichArduino , tempCurrentUser)

        self.checkStateElapsedTime = time.time() - functionStartTime

    def updateHistory(self):
        #populate history array with solenoid positions etc
        #has same structure as whats in log file. Consider rewriting logging part to just use the self.systemStateHistory

        #generate temporary list of array elements, then append temporary list to systemStateHistory
        tempList = np.zeros((1 , 30))

        tempList[0][0] = self.currentSystemTime
        tempList[0][1] = self.currentSystemElapsedTime
        #increment through solenoids
        self.solenoidLog_0.append(self.solenoids[0].currentValue)
        self.solenoidLog_1.append(self.solenoids[1].currentValue)
        self.solenoidLog_2.append(self.solenoids[2].currentValue)
        self.solenoidLog_3.append(self.solenoids[3].currentValue)
        self.solenoidLog_4.append(self.solenoids[4].currentValue)
        self.solenoidLog_5.append(self.solenoids[5].currentValue)
        self.solenoidLog_6.append(self.solenoids[6].currentValue)
        self.solenoidLog_7.append(self.solenoids[7].currentValue)
        self.solenoidLog_8.append(self.solenoids[8].currentValue)
        self.solenoidLog_9.append(self.solenoids[9].currentValue)
        self.solenoidLog_10.append(self.solenoids[10].currentValue)
        self.solenoidLog_11.append(self.solenoids[11].currentValue)
        self.solenoidLog_12.append(self.solenoids[12].currentValue)
        self.solenoidLog_13.append(self.solenoids[13].currentValue)
        self.solenoidLog_14.append(self.solenoids[14].currentValue)
        self.solenoidLog_15.append(self.solenoids[15].currentValue)


        self.gaugeLog_0.append(self.gauges[0].currentValue)
        self.gaugeLog_1.append(self.gauges[1].currentValue)
        self.gaugeLog_2.append(self.gauges[2].currentValue)
        self.gaugeLog_3.append(self.gauges[3].currentValue)

        self.MFCactualLog_0.append(self.MFCs[0].currentFlowRate)
        self.MFCsetpointLog_0.append(self.MFCs[0].currentSetPointFlowRateParam.value)
        self.MFCactualLog_1.append(self.MFCs[1].currentFlowRate)
        self.MFCsetpointLog_1.append(self.MFCs[1].currentSetPointFlowRateParam.value)
        self.MFCactualLog_2.append(self.MFCs[2].currentFlowRate)
        self.MFCsetpointLog_2.append(self.MFCs[2].currentSetPointFlowRateParam.value)
        self.MFCactualLog_3.append(self.MFCs[3].currentFlowRate)
        self.MFCsetpointLog_3.append(self.MFCs[3].currentSetPointFlowRateParam.value)

        self.currentSystemTime_log.append(self.currentSystemTime)
        self.currentSystemElapsedTime_log.append(self.currentSystemElapsedTime)

        self.systemStateHistoryLength += 1

        #increment length of history array - ie dimension that stores the history thing

    def logToFile(self , tempCurrentUser):
        # open file
        tempFunctionStartTime = time.time()
        self.previousElapsedTime = self.currentSystemElapsedTime
        #check to see if directory/path exists
        tempSystemStateLogDirectory = os.path.join(tempCurrentUser.parentDirectory , "users" ,  tempCurrentUser.name , tempCurrentUser.experimentDirectory , (str(tempCurrentUser.experimentDate) + "_" + tempCurrentUser.experimentName +"_" + str(tempCurrentUser.experimentTime)) )
        if(os.path.exists(tempSystemStateLogDirectory) == 0):
            print("Path does not exist")
            os.mkdir(tempSystemStateLogDirectory)
        tempSystemStateLogFilename = os.path.join(tempSystemStateLogDirectory , (tempCurrentUser.experimentName + "_log.txt"))
        tempSystemLog = open(tempSystemStateLogFilename, "a+")
        tempSystemLog.write(str(self.currentSystemTime) + "\t")
        tempSystemLog.write(str(self.currentSystemElapsedTime) + "\t")
        # tab delimited? Comma delimited?
        # write all solenoid positions - for loop and iterate through systemState.solenoids[]
        for j in range(self.numSolenoids):
            tempSystemLog.write(str(self.solenoids[j].currentValue) + "\t")
        # write all gauge pressures - iterate through systemState.gaugePressures
        for j in range(self.numGauges):
            tempSystemLog.write(str(self.gauges[j].currentPressure) + "\t")
        for j in range(self.numMFCs):
            tempSystemLog.write(str(self.MFCs[j].currentFlowRate) + "\t")
            tempSystemLog.write(str(self.MFCs[j].currentSetPointFlowRateParam.value) + "\t")

        tempFunctionElapsedTime = time.time() - tempFunctionStartTime
        tempSystemLog.write(str(tempFunctionElapsedTime) + "\r\n")  # prints elapsed time for writing system state to file, cant hurt to have this here and keep an eye on it
        tempSystemLog.close()
        self.updateHistory()
        self.logProcessElapsedTime = time.time() - tempFunctionStartTime

    def loadHistory(self , tempCurrentUser):
        #read in params from file to 2D array.
        #plot all different params in various matplotlib structures maybe have radio buttons for which thing I'm plotting. Turn on auto-range for all graphs.
        #radio button to select either whole timescale or just like last 5 mins or so.
        #MFC has liner vertical axis
        #gauge has logarithmic y axis
        #may have to move this to the tkinter processes in case I cant have it here. Would be sad :(
        tempLogFilename = os.path.join(tempCurrentUser.parentDirectory , tempCurrentUser.logDirectory , (tempCurrentUser.experimentName + "_log.txt") )
        #check to see if file exists. If does not, assign tempLogHistory to a row of zeroes, so can still graph but doesnt actually show anything. Once file is generated, will read from fiel and start plotting actual data.
        tempSystemLogFile = open(tempLogFilename)
        #read in file somehow. Have it so that it appends an array containing all the values for one specific time to a big array containing all previous values
        #Basically a 2D array that can be addressed as systemStateValues[i][j] - i describes which time/instance j describes which parameter.
        #tab delimited and newline denoted by \n
        tempLogHistory = np.loadtxt(tempLogFilename , delimiter= "\t")
        print(tempLogFilename[0][:])

        #once whole thing is read in, can plot. Introduce all sorts of switching behavior via radio buttons in GUI. Maybe have one tab to do all pressures, one tab to do all flow rates
        #buttons to change which one is displayed, with option to do all at once.

        return tempLogHistory
