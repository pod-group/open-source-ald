#import anything that needs importing
import sys #allows me to access command line arguments in sys.argv (list/array) sys.argv[0] is program name, sys.argv[n] is nth program argument.
import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox
import array

import numpy as np

# #from Tkinter import *
# import matplotlib
# matplotlib.use("TkAgg")
# from matplotlib import pyplot as plt
# from matplotlib.figure import Figure
# import matplotlib.animation as animation
# from matplotlib import style
# from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


import multiprocessing
import threading
import logging

import serial
from serial import Serial
import serial.tools.list_ports
import re

from datetime import date
import time
import os
import shutil
import csv

from Parameter import Parameter
from MFC import MFC
from Precursor import Precursor
from Purge import Purge
from Gauge import Gauge
from Solenoid import Solenoid
from cmd import cmd
from User import User
from ArduinoObject import ArduinoObject
from DepositionParameterClass import DepositionParameterClass
from SystemConfiguration import SystemConfiguration
from SystemState import SystemState
from NumPad import NumPad
from OnScreenKeyboard import OnScreenKeyboard
from SearchForAvailableSerial import searchForAvailableSerial




#parse arguments given to function in cmd line (or GUI program)

#navigate through directory structure to find the correct file(s)

#run process as normal

#win





#everything should be defined above
#run deposition here

#generate system configuration
#username passed as sys.argv[1]
#experiment name passed as sys.argv[2]
#arduino serial port passed as sys.argv[3]
userData = [sys.argv[1] , sys.argv[2] , sys.argv [3] , sys.argv[4]]
arduinoData = [sys.argv[5] , sys.argv[6]]

print(userData)
print(arduinoData)


deposition_arduino = ArduinoObject(arduinoData[0] , arduinoData[1])


deposition_user = User()
deposition_user.name = userData[0]
deposition_user.experimentName = userData[1]
deposition_user.experimentDate = userData[2]
deposition_user.experimentTime = userData[3]

deposition_systemState = SystemState()

tempInputFilename = deposition_user.experimentDate + "_" + deposition_user.experimentName + "_" + deposition_user.experimentTime + ".txt"
depositionParams = DepositionParameterClass()
depositionParams.readParamsFromFile_run(deposition_user , tempInputFilename)

depositionParams.runDeposition( deposition_arduino , deposition_user , deposition_systemState)



