from Parameter import Parameter
from Solenoid import Solenoid
import time
from Purge import Purge

class Precursor:
    def __init__(self):
        self.precursorNumber = 0
        self.currentState = 0
        self.upperSolenoid = Solenoid()
        self.lowerSolenoid = Solenoid()
        self.name = ""
        self.location = Parameter("location")
        self.numDoses = Parameter("numDoses")
        self.doseTime = Parameter("doseTime")
        self.refillTime = Parameter("refillTime")
        self.residenceTime = Parameter("residenceTime")
        self.openingTime = Parameter("openingTime")
        self.purgeDelay = Parameter("purgeDelay")
        self.purgeTime = Parameter("purgeTime")
        self.stabilisationTime = Parameter("stabilisationTime")

        self.purge = Purge()
        #probably need more stuff in here re deposition params probably in the depostion params section

    def openUpperSolenoid(self):
        pass
    def closeUpperSolenoid(self):
        pass
    def openLowersolenoid(self):
        pass
    def closeLowersolenoid(self , whichArduino):
        pass
    def cycle_flowMode(self , whichArduino , tempCurrentUser):
        #logic for choosing solenoids based on location
        if(self.precursorNumber == 0):
            self.upperSolenoid.number = 0
            self.upperSolenoid.number = 1
        elif(self.precursorNumber == 1):
            self.upperSolenoid.number = 2
            self.upperSolenoid.number = 3
        elif (self.precursorNumber == 2):
            self.upperSolenoid.number = 4
            self.upperSolenoid.number = 5
        elif (self.precursorNumber == 3):
            self.upperSolenoid.number = 6
            self.upperSolenoid.number = 7
        else:
            pass
        #do the actual cycling
        self.upperSolenoid.actuateSolenoid(100 , whichArduino , tempCurrentUser)
        print(self.upperSolenoid.kineticsDelay)
        time.sleep(self.upperSolenoid.kineticsDelay)
        self.lowerSolenoid.actuateSolenoid(100 , whichArduino , tempCurrentUser)
        time.sleep(0.001*self.openingTime.value)
        self.upperSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)
        self.lowerSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)

    def cycle_exposureMode(self , whichArduino , tempCurrentUser):
        # logic for choosing solenoids based on location
        if (self.location.value == 0):
            self.lowerSolenoid.number = 0
            self.upperSolenoid.number = 1
        elif (self.location.value == 1):
            self.lowerSolenoid.number = 2
            self.upperSolenoid.number = 3
        elif (self.location.value == 2):
            self.lowerSolenoid.number = 4
            self.upperSolenoid.number = 5
        elif (self.location.value == 3):
            self.lowerSolenoid.number = 6
            self.upperSolenoid.number = 7
        else:
            pass
        # do the actual cycling
        tempDoseCount = 0
        while(tempDoseCount < self.numDoses.value):
            self.upperSolenoid.actuateSolenoid(100 , whichArduino , tempCurrentUser)
            time.sleep(self.doseTime.value)
            self.upperSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)
            time.sleep(0.1)
            self.lowerSolenoid.actuateSolenoid(100 , whichArduino , tempCurrentUser)
            time.sleep(self.refillTime.value)
            self.lowerSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)
            tempDoseCount += 1
