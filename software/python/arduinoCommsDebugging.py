import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox
import array

import numpy as np

# #from Tkinter import *
# import matplotlib
# matplotlib.use("TkAgg")
# from matplotlib import pyplot as plt
# from matplotlib.figure import Figure
# import matplotlib.animation as animation
# from matplotlib import style
# from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import serial
from serial import Serial
import serial.tools.list_ports
import re

from datetime import date
import time
import os
import shutil
import csv
import multiprocessing
import threading
import logging

from Parameter import Parameter
from MFC import MFC
from Precursor import Precursor
from Purge import Purge
from Gauge import Gauge
from Solenoid import Solenoid
from cmd import cmd
from User import User
from ArduinoObject import ArduinoObject
from DepositionParameterClass import DepositionParameterClass
from SystemConfiguration import SystemConfiguration
from SystemState import SystemState
from NumPad import NumPad
from OnScreenKeyboard import OnScreenKeyboard
from SearchForAvailableSerial import searchForAvailableSerial
from processThread import depositionProcessThreadClass


arduino = ArduinoObject("/dev/cu.usbmodem1D1201" , 2000000)
arduino.comms.open()
testSendString = '?tst_01_001?'
initialisedConfirmString = '?tst_1_001?\r\n'

# while (arduino.isInitialised == 0):
#     print("Probing Arduino")
#     print(testSendString)
#     arduino.send(testSendString)
#     tempResponse = arduino.receiveAll()
#     print(tempResponse)
#     print(repr(tempResponse))
#     print(len(initialisedConfirmString))
#     print(len(tempResponse))
#     if(tempResponse == initialisedConfirmString):
#         arduino.isInitialised = 1
#         print("Arduino Initialised! Congratulations")
#     else:
#         arduino.isInitialised = 0
#         print("not initialised yet :( ")
#
#     time.sleep(1)

arduino.promptSend()

arduino.receiveAll()

print("loop done")
