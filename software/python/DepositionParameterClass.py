import os
import shutil
import csv

from Parameter import Parameter
from Precursor import Precursor
from Solenoid import Solenoid
from User import User
from Purge import Purge
from MFC import MFC
import time
from tkinter import messagebox


class DepositionParameterClass:
    def  __init__(self):
        #rewrite this to have precursor class objects to store variables related to each precursor
        #precursor objects contain parameter objects so I can still use the dictionary for assigning stuff from being read in
        #will still need to have some standalone variables but can be done. Will also need to rewrite
        self.numPrecursors = Parameter("numPrecursors")
        self.numPrecursors.value = 4
        self.numMFCs = 8
        self.vacuumShutoffSolenoid = Solenoid()
        self.vacuumShutoffSolenoid.number = 15

        self.purgeMFC_solenoid = Solenoid()
        self.purgeMFC_solenoid.number = 12
        self.carrierGasMFC_solenoid = Solenoid()
        self.carrierGasMFC_solenoid.number = 13

        self.cycleCount = 0

        self.precursors = [Precursor() for _ in range(self.numPrecursors.value)]
        for i in range(self.numPrecursors.value):
            self.precursors[i].location.paramName = "location_" + str(i)
            self.precursors[i].location.value = i
            self.precursors[i].numDoses.paramName = "numDoses_" + str(i)
            self.precursors[i].doseTime.paramName =  "doseTime_" + str(i)
            self.precursors[i].refillTime.paramName = "refillTime_"  + str(i)
            self.precursors[i].residenceTime.paramName = "residenceTime_" + str(i)
            self.precursors[i].openingTime.paramName = "openingTime_" + str(i)
            self.precursors[i].purgeDelay.paramName = "purgeDelay_" + str(i)
            self.precursors[i].purgeTime.paramName = "purgeTime_" + str(i)
            self.precursors[i].stabilisationTime.paramName = "stabilisationTime_" + str(i)

            #assign upper and lower solenoids
            if(i==0):
                self.precursors[i].upperSolenoid.number = 0
                self.precursors[i].lowerSolenoid.number = 1
            if(i==1):
                self.precursors[i].upperSolenoid.number = 2
                self.precursors[i].lowerSolenoid.number = 3
            if(i==2):
                self.precursors[i].upperSolenoid.number = 4
                self.precursors[i].lowerSolenoid.number = 5
            if(i==3):
                self.precursors[i].upperSolenoid.number = 6
                self.precursors[i].lowerSolenoid.number = 7


        #purge follows release of precursor of same index
        self.purges = [Purge() for _ in range(self.numPrecursors.value)]

        tempCount = 0
        self.massFlowControllers = [MFC() for _ in range(self.numMFCs)]
        #print(self.massFlowControllers)
        #assign stuff to mass flow controllers
        #MFC_0
        self.massFlowControllers[0].number = 0
        self.massFlowControllers[0].currentSetPointFlowRateParam.value = 0

        #MFC_1
        self.massFlowControllers[1].number = 1
        self.massFlowControllers[1].currentSetPointFlowRateParam.value = 0

        #MFC_2
        self.massFlowControllers[2].number = 2
        self.massFlowControllers[2].currentSetPointFlowRateParam.value = 0

        #MFC_3
        self.massFlowControllers[3].number = 3
        self.massFlowControllers[3].currentSetPointFlowRateParam.value = 0

        #MFC_4
        self.massFlowControllers[4].number = 4
        self.massFlowControllers[4].currentSetPointFlowRateParam.value = 0

        #MFC_5
        self.massFlowControllers[5].number = 5
        self.massFlowControllers[5].currentSetPointFlowRateParam.value = 0

        #MFC_6
        self.massFlowControllers[6].number = 6
        self.massFlowControllers[6].currentSetPointFlowRateParam.value = 0

        #MFC_7
        self.massFlowControllers[7].number = 7
        self.massFlowControllers[7].currentSetPointFlowRateParam.value = 0

        self.canBeEdited = 1
        self.numCycles = Parameter("numCycles")
        self.material = Parameter("material")

        self.supercycleFrequency_1 = Parameter("supercycleFrequency_1")
        self.supercycleFrequency_2 = Parameter("supercycleFrequency_2")
        self.supercycleFrequency_3 = Parameter("supercycleFrequency_3")
        self.dopantPrecursorLocation = Parameter("dopantPrecursorLocation")

        self.depositionType = Parameter("depositionType") #for determining whether or not supercycles are necessary
        self.depositionMode = Parameter("depositionMode") #0 for continuous flow mode, 1 for exposure mode


    def dumpToFile(self , tempDirectory , tempInputFilename):
        #dumps Deposition parameters to file in directory. Directory and filename (without extension) passed to function through arguments.
        tempParamFile = open(os.path.join(tempDirectory , (tempInputFilename + ".txt")) , "a+")

        # Need a header for opening as dictionary later:
        tempParamFile.write("paramName" + "\t" + "paramValue" + "\n")
        tempParamFile.write("canBeEdited" + "\t" + str(self.canBeEdited) + "\n")
        tempParamFile.write(self.numCycles.paramName + "\t" + str(self.numCycles.value) + "\n")
        tempParamFile.write(self.material.paramName + "\t" + str(self.material.value) + "\n")
        tempParamFile.write(self.supercycleFrequency_1.paramName + "\t" + str(self.supercycleFrequency_1.value) + "\n")
        tempParamFile.write(self.supercycleFrequency_2.paramName + "\t" + str(self.supercycleFrequency_2.value) + "\n")
        tempParamFile.write(self.supercycleFrequency_3.paramName + "\t" + str(self.supercycleFrequency_3.value) + "\n")
        tempParamFile.write(self.dopantPrecursorLocation.paramName + "\t" + str(self.dopantPrecursorLocation.value) + "\n")
        tempParamFile.write(self.depositionType.paramName + "\t" + str(self.depositionType.value) + "\n")
        tempParamFile.write(self.depositionMode.paramName + "\t" + str(self.depositionMode.value) + "\n")
        tempParamFile.write(self.numPrecursors.paramName + "\t" + str(self.numPrecursors.value) + "\n")
        ##----------------------------
        tempParamFile.write(self.precursors[0].location.paramName + "\t" + str(self.precursors[0].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[0].numDoses.paramName + "\t" + str(self.precursors[0].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[0].doseTime.paramName + "\t" + str(self.precursors[0].doseTime.value) + "\n")
        tempParamFile.write(self.precursors[0].refillTime.paramName + "\t" + str(self.precursors[0].refillTime.value) + "\n")
        tempParamFile.write(self.precursors[0].residenceTime.paramName + "\t" + str(self.precursors[0].residenceTime.value) + "\n")
        tempParamFile.write(self.precursors[0].openingTime.paramName + "\t" + str(self.precursors[0].openingTime.value) + "\n")
        tempParamFile.write(self.precursors[0].purgeDelay.paramName + "\t" + str(self.precursors[0].purgeDelay.value) + "\n")
        tempParamFile.write(self.precursors[0].purgeTime.paramName + "\t" + str(self.precursors[0].purgeTime.value) + "\n")
        tempParamFile.write(self.precursors[0].stabilisationTime.paramName + "\t" + str(self.precursors[0].stabilisationTime.value) + "\n")
        ##----------------------------
        tempParamFile.write(self.precursors[1].location.paramName + "\t" + str(self.precursors[1].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[1].numDoses.paramName + "\t" + str(self.precursors[1].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[1].doseTime.paramName + "\t" + str(self.precursors[1].doseTime.value) + "\n")
        tempParamFile.write(self.precursors[1].refillTime.paramName + "\t" + str(self.precursors[1].refillTime.value) + "\n")
        tempParamFile.write(self.precursors[1].residenceTime.paramName + "\t" + str(self.precursors[1].residenceTime.value) + "\n")
        tempParamFile.write(self.precursors[1].openingTime.paramName + "\t" + str(self.precursors[1].openingTime.value) + "\n")
        tempParamFile.write(self.precursors[1].purgeDelay.paramName + "\t" + str(self.precursors[1].purgeDelay.value) + "\n")
        tempParamFile.write(self.precursors[1].purgeTime.paramName + "\t" + str(self.precursors[1].purgeTime.value) + "\n")
        tempParamFile.write(self.precursors[1].stabilisationTime.paramName + "\t" + str(self.precursors[1].stabilisationTime.value) + "\n")
        ##----------------------------
        tempParamFile.write(self.precursors[2].location.paramName + "\t" + str(self.precursors[2].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[2].numDoses.paramName + "\t" + str(self.precursors[2].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[2].doseTime.paramName + "\t" + str(self.precursors[2].doseTime.value) + "\n")
        tempParamFile.write(self.precursors[2].refillTime.paramName + "\t" + str(self.precursors[2].refillTime.value) + "\n")
        tempParamFile.write(self.precursors[2].residenceTime.paramName + "\t" + str(self.precursors[2].residenceTime.value) + "\n")
        tempParamFile.write(self.precursors[2].openingTime.paramName + "\t" + str(self.precursors[2].openingTime.value) + "\n")
        tempParamFile.write(self.precursors[2].purgeDelay.paramName + "\t" + str(self.precursors[2].purgeDelay.value) + "\n")
        tempParamFile.write(self.precursors[2].purgeTime.paramName + "\t" + str(self.precursors[2].purgeTime.value) + "\n")
        tempParamFile.write(self.precursors[2].stabilisationTime.paramName + "\t" + str(self.precursors[2].stabilisationTime.value) + "\n")
        ##----------------------------
        tempParamFile.write(self.precursors[3].location.paramName + "\t" + str(self.precursors[3].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[3].numDoses.paramName + "\t" + str(self.precursors[3].numDoses.value) + "\n")
        tempParamFile.write(self.precursors[3].doseTime.paramName + "\t" + str(self.precursors[3].doseTime.value) + "\n")
        tempParamFile.write(self.precursors[3].refillTime.paramName + "\t" + str(self.precursors[3].refillTime.value) + "\n")
        tempParamFile.write(self.precursors[3].residenceTime.paramName + "\t" + str(self.precursors[3].residenceTime.value) + "\n")
        tempParamFile.write(self.precursors[3].openingTime.paramName + "\t" + str(self.precursors[3].openingTime.value) + "\n")
        tempParamFile.write(self.precursors[3].purgeDelay.paramName + "\t" + str(self.precursors[3].purgeDelay.value) + "\n")
        tempParamFile.write(self.precursors[3].purgeTime.paramName + "\t" + str(self.precursors[3].purgeTime.value) + "\n")
        tempParamFile.write(self.precursors[3].stabilisationTime.paramName + "\t" + str(self.precursors[3].stabilisationTime.value) + "\n")


        tempParamFile.close()
        #if statement here based on tempCurrentUser.isNewProcess, if new process, copy over to log directory as well. Gonna need to put an on-screen keyboard in for that field.

    def readParamsFromFile_setup(self , tempUser , inputFilename):
        # add to parameter class object
        tempUser.processDirectory = os.path.join(tempUser.parentDirectory , "users" ,  tempUser.name , "processes")
        #some logic here in case this directory doesnt exist. Error handling.
        tempParamFile = open(os.path.join(tempUser.processDirectory , inputFilename ), newline="")
        paramsList = []
        paramsReader = csv.DictReader(tempParamFile, delimiter="\t")
        # populate list of dicts containing parameter info as paramName and paramValue, can access each list entry by paramsList[i]
        for param in paramsReader:
            paramsList.append(dict(param))

        # can access each individual entry in params list by either paramsList[i]["paramName"] or paramsList[i]["paramValue']
        # print(paramsList[1]["paramName"])

        self.canBeEdited = int(paramsList[0]["paramValue"])
        self.numCycles.value = float(paramsList[1]["paramValue"])
        self.material.value = float(paramsList[2]["paramValue"])
        self.supercycleFrequency_1.value = float(paramsList[3]["paramValue"])
        self.supercycleFrequency_2.value = float(paramsList[4]["paramValue"])
        self.supercycleFrequency_3.value = float(paramsList[5]["paramValue"])
        self.dopantPrecursorLocation.value = float(paramsList[6]["paramValue"])
        self.depositionType.value = float(paramsList[7]["paramValue"])
        self.depositionMode.value = float(paramsList[8]["paramValue"])
        self.depositionMode.value = float(paramsList[9]["paramValue"])
        ##----------------------------
        self.precursors[0].location.value = float(paramsList[10]["paramValue"])
        self.precursors[0].numDoses.value = float(paramsList[11]["paramValue"])
        self.precursors[0].doseTime.value = float(paramsList[12]["paramValue"])
        self.precursors[0].refillTime.value = float(paramsList[13]["paramValue"])
        self.precursors[0].residenceTime.value = float(paramsList[14]["paramValue"])
        self.precursors[0].openingTime.value = float(paramsList[15]["paramValue"])
        self.precursors[0].purgeDelay.value = float(paramsList[16]["paramValue"])
        self.precursors[0].purgeTime.value = float(paramsList[17]["paramValue"])
        self.precursors[0].stabilisationTime.value = float(paramsList[18]["paramValue"])
        ##----------------------------
        self.precursors[1].location.value = float(paramsList[19]["paramValue"])
        self.precursors[1].numDoses.value = float(paramsList[20]["paramValue"])
        self.precursors[1].doseTime.value = float(paramsList[21]["paramValue"])
        self.precursors[1].refillTime.value = float(paramsList[22]["paramValue"])
        self.precursors[1].residenceTime.value = float(paramsList[23]["paramValue"])
        self.precursors[1].openingTime.value = float(paramsList[24]["paramValue"])
        self.precursors[1].purgeDelay.value = float(paramsList[25]["paramValue"])
        self.precursors[1].purgeTime.value = float(paramsList[26]["paramValue"])
        self.precursors[1].stabilisationTime.value = float(paramsList[27]["paramValue"])
        ##----------------------------
        self.precursors[2].location.value = float(paramsList[28]["paramValue"])
        self.precursors[2].numDoses.value = float(paramsList[29]["paramValue"])
        self.precursors[2].doseTime.value = float(paramsList[30]["paramValue"])
        self.precursors[2].refillTime.value = float(paramsList[31]["paramValue"])
        self.precursors[2].residenceTime.value = float(paramsList[32]["paramValue"])
        self.precursors[2].openingTime.value = float(paramsList[33]["paramValue"])
        self.precursors[2].purgeDelay.value = float(paramsList[34]["paramValue"])
        self.precursors[2].purgeTime.value = float(paramsList[35]["paramValue"])
        self.precursors[2].stabilisationTime.value = float(paramsList[36]["paramValue"])
        ##----------------------------
        self.precursors[3].location.value = float(paramsList[37]["paramValue"])
        self.precursors[3].numDoses.value = float(paramsList[38]["paramValue"])
        self.precursors[3].doseTime.value = float(paramsList[39]["paramValue"])
        self.precursors[3].refillTime.value = float(paramsList[40]["paramValue"])
        self.precursors[3].residenceTime.value = float(paramsList[41]["paramValue"])
        self.precursors[3].openingTime.value = float(paramsList[42]["paramValue"])
        self.precursors[3].purgeDelay.value = float(paramsList[43]["paramValue"])
        self.precursors[3].purgeTime.value = float(paramsList[44]["paramValue"])
        self.precursors[3].stabilisationTime.value = float(paramsList[45]["paramValue"])

    def readParamsFromFile_run(self, tempUser, inputFilename):
        # add to parameter class object
        paramFolder = tempUser.experimentDate + "_" + tempUser.experimentName +"_" + tempUser.experimentTime
        paramFileLocation = os.path.join(tempUser.parentDirectory , "users" , tempUser.name , tempUser.experimentDirectory , paramFolder)
        tempParamFile = open(os.path.join(paramFileLocation, inputFilename), newline="")
        paramsList = []
        paramsReader = csv.DictReader(tempParamFile, delimiter="\t")
        # populate list of dicts containing parameter info as paramName and paramValue, can access each list entry by paramsList[i]
        for param in paramsReader:
            paramsList.append(dict(param))

        # can access each individual entry in params list by either paramsList[i]["paramName"] or paramsList[i]["paramValue']
        # print(paramsList[1]["paramName"])

        self.canBeEdited = int(paramsList[0]["paramValue"])
        self.numCycles.value = float(paramsList[1]["paramValue"])
        self.material.value = float(paramsList[2]["paramValue"])
        self.supercycleFrequency_1.value = float(paramsList[3]["paramValue"])
        self.supercycleFrequency_2.value = float(paramsList[4]["paramValue"])
        self.supercycleFrequency_3.value = float(paramsList[5]["paramValue"])
        self.dopantPrecursorLocation.value = float(paramsList[6]["paramValue"])
        self.depositionType.value = float(paramsList[7]["paramValue"])
        self.depositionMode.value = float(paramsList[8]["paramValue"])
        self.depositionMode.value = float(paramsList[9]["paramValue"])
        ##----------------------------
        self.precursors[0].location.value = float(paramsList[10]["paramValue"])
        self.precursors[0].numDoses.value = float(paramsList[11]["paramValue"])
        self.precursors[0].doseTime.value = float(paramsList[12]["paramValue"])
        self.precursors[0].refillTime.value = float(paramsList[13]["paramValue"])
        self.precursors[0].residenceTime.value = float(paramsList[14]["paramValue"])
        self.precursors[0].openingTime.value = float(paramsList[15]["paramValue"])
        self.precursors[0].purgeDelay.value = float(paramsList[16]["paramValue"])
        self.precursors[0].purgeTime.value = float(paramsList[17]["paramValue"])
        self.precursors[0].stabilisationTime.value = float(paramsList[18]["paramValue"])
        ##----------------------------
        self.precursors[1].location.value = float(paramsList[19]["paramValue"])
        self.precursors[1].numDoses.value = float(paramsList[20]["paramValue"])
        self.precursors[1].doseTime.value = float(paramsList[21]["paramValue"])
        self.precursors[1].refillTime.value = float(paramsList[22]["paramValue"])
        self.precursors[1].residenceTime.value = float(paramsList[23]["paramValue"])
        self.precursors[1].openingTime.value = float(paramsList[24]["paramValue"])
        self.precursors[1].purgeDelay.value = float(paramsList[25]["paramValue"])
        self.precursors[1].purgeTime.value = float(paramsList[26]["paramValue"])
        self.precursors[1].stabilisationTime.value = float(paramsList[27]["paramValue"])
        ##----------------------------
        self.precursors[2].location.value = float(paramsList[28]["paramValue"])
        self.precursors[2].numDoses.value = float(paramsList[29]["paramValue"])
        self.precursors[2].doseTime.value = float(paramsList[30]["paramValue"])
        self.precursors[2].refillTime.value = float(paramsList[31]["paramValue"])
        self.precursors[2].residenceTime.value = float(paramsList[32]["paramValue"])
        self.precursors[2].openingTime.value = float(paramsList[33]["paramValue"])
        self.precursors[2].purgeDelay.value = float(paramsList[34]["paramValue"])
        self.precursors[2].purgeTime.value = float(paramsList[35]["paramValue"])
        self.precursors[2].stabilisationTime.value = float(paramsList[36]["paramValue"])
        ##----------------------------
        self.precursors[3].location.value = float(paramsList[37]["paramValue"])
        self.precursors[3].numDoses.value = float(paramsList[38]["paramValue"])
        self.precursors[3].doseTime.value = float(paramsList[39]["paramValue"])
        self.precursors[3].refillTime.value = float(paramsList[40]["paramValue"])
        self.precursors[3].residenceTime.value = float(paramsList[41]["paramValue"])
        self.precursors[3].openingTime.value = float(paramsList[42]["paramValue"])
        self.precursors[3].purgeDelay.value = float(paramsList[43]["paramValue"])
        self.precursors[3].purgeTime.value = float(paramsList[44]["paramValue"])
        self.precursors[3].stabilisationTime.value = float(paramsList[45]["paramValue"])

    def runDeposition(self , whichArduino , tempCurrentUser , tempCurrentState):
        #use functions built into Precursor class basically grab the parameters from self
        #whenever its time to cycle a precursor have an if-statement to decide what kind of cycle
        #take some time to pump down and make sure system is down to pressure and clean.
        #start cycling like lance armstrong
        #each second - check system state and log to file. Account for any latency in communications and opening/closing files. Have code for this in original standalone deposition
        #keep looping until its time to release precursor
        #if statement to see what kind of dose
        #purging while still logging all parameters
        #repeat for second precursor
        #then continue until cycleCount = numCycles
        #when deposition finished, have similar purging/cleaning
        # already have all the deposition parameters loaded and stored in a struct/class object
        # Somehow figure out if I can have two things going in parallel? Maybe just log system state once every second.

        #pumpdown process


        purgeMFC = 0
        carrierGasMFC = 1








        #populate precursors[i].purge parameters
        tempCount = 0
        while(tempCount < self.numPrecursors.value):
            self.purges[tempCount].updatePurgeParameters(self.precursors[tempCount] , self.massFlowControllers[purgeMFC])
            tempCount += 1

        tempCurrentTime = time.time();
        tempPreviousTime = 0;
        tempGetSystemStateDuration = 0
        tempGetSystemStateStartTime = 0
        tempDoseDuration = 0
        tempDoseStartTime = 0

        print("started thread")
        print(whichArduino.comms.isOpen())
        if(whichArduino.comms.isOpen() != True):
            whichArduino.isInitialised = 0
            whichArduino.comms.open()
            time.sleep(1)
            whichArduino.initialise()

            if(whichArduino.isInitialised == 0):
                self.cycleCount = self.numCycles.value
                print("Initialisation Failed. Deposition aborted.")


        print(whichArduino.port)

        #currently just using Pirani gauges, enabled by default. Bridging these contacts sets vacuum level when below range, and atmosphere when at atmosphere
        # tempCurrentState.gauges[0].enable(whichArduino, tempCurrentUser)
        # tempCurrentState.gauges[1].enable(whichArduino, tempCurrentUser)
        # tempCurrentState.gauges[2].enable(whichArduino, tempCurrentUser)
        # tempCurrentState.gauges[3].enable(whichArduino, tempCurrentUser)

        self.vacuumShutoffSolenoid.actuateSolenoid(100, whichArduino, tempCurrentUser)
        self.purgeMFC_solenoid.actuateSolenoid(100, whichArduino, tempCurrentUser)
        self.carrierGasMFC_solenoid.actuateSolenoid(100, whichArduino, tempCurrentUser)

        print("open messagebox for pumpdown delay")
        messagebox.showinfo("delay", "Delay for pumpDown")
        print("delay over")

        systemStateLogInterval = 1
        print("depositionFunctionStarted")
        print(self.depositionMode.value)



        temp_purge1_start = 0
        temp_purge2_start = 0

        # start deposition process.
        while (self.cycleCount < self.numCycles.value):
            # log system state.
            # do precursor 1
            #print("inside deposition loop")
            #print(self.precursors[0].upperSolenoid.number)
            #print(self.precursors[0].lowerSolenoid.number)
            print("depositionMode")
            print(self.depositionMode.value)
            if (self.depositionMode.value == 0):
                #cycle precursor in flow mode
                self.precursors[0].cycle_flowMode(whichArduino , tempCurrentUser)
            elif (self.depositionMode.value == 1):
                #cycle precursor in exposure mode
                self.vacuumShutoffSolenoid.actuateSolenoid( 0 , whichArduino , tempCurrentUser)
                self.precursors[0].cycle_exposureMode(whichArduino , tempCurrentUser)
            else:
                #do nothing
                pass
            print("precursor_0 released")

            #delay for residence time if necessary, continue to log system state

            if(self.depositionMode.value == 0):
                #do nothing
                pass
            elif(self.depositionMode.value == 1):
                #pause for residence time/purge delay
                tempResidenceTime_0 = time.time()
                while ((tempCurrentTime - tempResidenceTime_0) < self.precursors[0].residenceTime.value):
                    tempCurrentTime = time.time()
                    if (tempCurrentTime - (
                            tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                        tempGetSystemStateStartTime = time.time()
                        tempCurrentState.check(whichArduino, tempCurrentUser)
                        tempCurrentState.logToFile(tempCurrentUser)
                        tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                        # print(tempGetSystemStateDuration)
                        tempPreviousTime = time.time()
            else:
                #do nothing
                pass

            #delay for purge delay time if necessary, continue to log system state
            if(self.depositionMode.value == 0):
                tempPurgeDelay_0 = time.time()
                while ((tempCurrentTime - tempPurgeDelay_0) < self.precursors[0].purgeDelay.value):
                    tempCurrentTime = time.time()
                    if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                        tempGetSystemStateStartTime = time.time()
                        tempCurrentState.check(whichArduino, tempCurrentUser)
                        tempCurrentState.logToFile(tempCurrentUser)
                        tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                        # print(tempGetSystemStateDuration)
                        tempPreviousTime = time.time()
            elif(self.depositionMode.value == 1):
                tempPurgeDelay_0 = time.time()
                while ((tempCurrentTime - tempPurgeDelay_0) < self.precursors[0].purgeDelay.value):
                    tempCurrentTime = time.time()
                    if (tempCurrentTime - (
                            tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                        tempGetSystemStateStartTime = time.time()
                        tempCurrentState.check(whichArduino, tempCurrentUser)
                        tempCurrentState.logToFile(tempCurrentUser)
                        tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                        # print(tempGetSystemStateDuration)
                        tempPreviousTime = time.time()
            else:
                #do nothing
                pass


            #start purge
            # log system state every logInterval until need to stop purge

            #open vacuum isolation valve if closed.
            if(self.vacuumShutoffSolenoid.currentValue == 0):
                self.vacuumShutoffSolenoid.actuateSolenoid(100 , whichArduino , tempCurrentUser)

            print("purge0 start")
            temp_purge0_start = time.time()
            self.precursors[0].upperSolenoid.actuateSolenoid(0, whichArduino , tempCurrentUser)
            self.purges[0].purgeStart(whichArduino , tempCurrentUser)
            while ((tempCurrentTime - temp_purge0_start) < self.precursors[0].purgeTime.value):
                tempCurrentTime = time.time()
                if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                    tempGetSystemStateStartTime = time.time()
                    tempCurrentState.check(whichArduino , tempCurrentUser)
                    tempCurrentState.logToFile(tempCurrentUser)
                    tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                    #print(tempGetSystemStateDuration)
                    tempPreviousTime = time.time()
                    #print("state logged")

            self.purges[0].purgeStop(whichArduino , tempCurrentUser)
            #self.precursors[0].upperSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)
            print("purge0 finished")

            #actuate cutoff solenoid for purge.
            # continue logging system state every second until time for next precursor - have some stabilisation time
            tempStabilisation_1_start = time.time()
            while ((tempCurrentTime - tempStabilisation_1_start) < self.precursors[0].stabilisationTime.value):
                tempCurrentTime = time.time()
                if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                    tempGetSystemStateStartTime = time.time()
                    tempCurrentState.check(whichArduino , tempCurrentUser)
                    tempCurrentState.logToFile(tempCurrentUser)
                    tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                    #print(tempGetSystemStateDuration)
                    tempPreviousTime = time.time()
            # do precursor 2
            self.precursors[0].upperSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)


            #logic here for cycling exposure vs flow mode
            if (self.depositionMode.value == 0):
                #cycle precursor in flow mode
                self.precursors[1].cycle_flowMode(whichArduino , tempCurrentUser)
            elif (self.depositionMode.value == 1):
                #cycle precursor in exposure mode
                self.vacuumShutoffSolenoid.actuateSolenoid(0 , whichArduino , tempCurrentUser)
                self.precursors[1].cycle_exposureMode(whichArduino , tempCurrentUser)
            else:
                #do nothing
                pass
            print("precursor_1 released")

            #delay for residence time if necessary, continue to log system state
            if(self.depositionMode.value == 0):
                #do nothing
                pass
            elif(self.depositionMode.value == 1):
                #pause for residence time/purge delay
                tempResidenceTime_1 = time.time()
                while ((tempCurrentTime - tempResidenceTime_1) < self.precursors[1].residenceTime.value):
                    tempCurrentTime = time.time()
                    if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                        tempGetSystemStateStartTime = time.time()
                        tempCurrentState.check(whichArduino, tempCurrentUser)
                        tempCurrentState.logToFile(tempCurrentUser)
                        tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                        # print(tempGetSystemStateDuration)
                        tempPreviousTime = time.time()
            else:
                #do nothing
                pass

            #delay for purge delay time if necessary, continue to log system state
            if(self.depositionMode.value == 0):
                tempPurgeDelay_1 = time.time()
                while ((tempCurrentTime - tempPurgeDelay_0) < self.precursors[1].purgeDelay.value):
                    tempCurrentTime = time.time()
                    if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                        tempGetSystemStateStartTime = time.time()
                        tempCurrentState.check(whichArduino, tempCurrentUser)
                        tempCurrentState.logToFile(tempCurrentUser)
                        tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                        # print(tempGetSystemStateDuration)
                        tempPreviousTime = time.time()
            elif(self.depositionMode.value == 1):
                tempPurgeDelay_1 = time.time()
                while ((tempCurrentTime - tempPurgeDelay_0) < self.precursors[1].purgeDelay.value):
                    tempCurrentTime = time.time()
                    if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                        tempGetSystemStateStartTime = time.time()
                        tempCurrentState.check(whichArduino, tempCurrentUser)
                        tempCurrentState.logToFile(tempCurrentUser)
                        tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                        # print(tempGetSystemStateDuration)
                        tempPreviousTime = time.time()
            else:
                #do nothing
                pass


            # start purge
            # log system state every second until its time to stop purge

            #open vacuum isolation valve if not already open
            if (self.vacuumShutoffSolenoid.currentValue == 0):
                self.vacuumShutoffSolenoid.actuateSolenoid(100, whichArduino, tempCurrentUser)
            temp_purge2_start = time.time()
            self.precursors[0].upperSolenoid.actuateSolenoid(0, whichArduino, tempCurrentUser)
            self.purges[1].purgeStart(whichArduino , tempCurrentUser)
            while ((tempCurrentTime - temp_purge2_start) < self.precursors[1].purgeTime.value):
                tempCurrentTime = time.time()
                if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                    #print(time.time())
                    tempGetSystemStateStartTime = time.time()
                    tempCurrentState.check(whichArduino , tempCurrentUser)
                    tempCurrentState.logToFile(tempCurrentUser)
                    tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                    #print(tempGetSystemStateDuration)
                    tempPreviousTime = time.time()
            # stop purge
            self.purges[1].purgeStop(whichArduino , tempCurrentUser)
            #self.precursors[0].upperSolenoid.actuateSolenoid(0, whichArduino, tempCurrentUser)
            print("purge 2 end")

            #actuateSolenoid(purgeMFC_solenoid, 0, inputSystemState, tempCommandLogFilename)
            #do the next stabilisation time
            # log system state evey second until next precursor
            tempStabilisation_2_start = time.time()
            while ((tempCurrentTime - tempStabilisation_2_start) < self.precursors[1].stabilisationTime.value):
                tempCurrentTime = time.time()
                if (tempCurrentTime - (tempPreviousTime - tempGetSystemStateDuration) > tempCurrentState.logInterval):
                    tempGetSystemStateStartTime = time.time()
                    tempCurrentState.check(whichArduino , tempCurrentUser)
                    tempCurrentState.logToFile(tempCurrentUser)
                    tempGetSystemStateDuration = time.time() - tempGetSystemStateStartTime
                    #print(tempGetSystemStateDuration)
                    tempPreviousTime = time.time()
            print("stab 2 done")
            self.precursors[0].upperSolenoid.actuateSolenoid(0, whichArduino, tempCurrentUser)

            print(self.cycleCount)
            self.cycleCount += 1

            #somewhere here put in logic for doped deposition

            #somewehre here out in logic for 3 precursor deposition


        #purging here to end deposition and clear chamber.


        print("Deposition Finished")
        messagebox.showinfo("Deposition", "Deposition Finished. Congratulations")
        #reset cycle count to zero for next deposition.
        self.cycleCount = 0

        #end of actual depostiion part.
        #Same purge and clean process as in current ALD code, flush with N2 for a while, then open upper solenoid of each precursor sequentially.

    # def startDeposition(self , whichArduino , tempCurrentUser , tempCurrentState):
    #     self.depositionQuitFlag.value = False
    #     self.depositionThread = threading.Thread(target = lambda: self.runDeposition(whichArduino , tempCurrentUser , tempCurrentState))
    #     self.depositionThread.start()


