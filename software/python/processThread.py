import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox
import array

import numpy as np
import serial
from serial import Serial
import serial.tools.list_ports
import re
import threading
from datetime import date
import time
import os
import shutil
import csv

from Parameter import Parameter
from MFC import MFC
from Precursor import Precursor
from Purge import Purge
from Gauge import Gauge
from Solenoid import Solenoid
from cmd import cmd
from User import User
from ArduinoObject import ArduinoObject
from DepositionParameterClass import DepositionParameterClass
from SystemConfiguration import SystemConfiguration
from SystemState import SystemState
from NumPad import NumPad
from OnScreenKeyboard import OnScreenKeyboard
from SearchForAvailableSerial import searchForAvailableSerial

class depositionProcessThreadClass(threading.Thread):
    #not sure what arguments to pass to thread just yet, just probing whats possible atm.
    def __int__(self, inputThreadID , inputName , inputCounter):
        threading.Thread.__init__(self)
        self.threadID = inputThreadID
        self.name = inputName
        self.counter = inputCounter

    def run(self):
        print("Started Process")
        #Just put in counter while loop to test some stuff here
        tempCounter = 0
        while(tempCounter <= 40):
            print(tempCounter)
            time.sleep(1)
            tempCounter +=1






