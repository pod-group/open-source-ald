from User import User
from ArduinoObject import ArduinoObject



class SystemConfiguration:
    def __init__(self , inputUsername , inputExperimentName , inputArduinoPort , inputArduionBaudRate):
        #inputUsername and inputExperimentName come from arguments passed to this program via command line from GUI
        self.dataDirectory = "/Users/Augustin/googleDrive/storage/Oxford/equipmentProjects/vapourTreatmentDevice/software/snek"
        #create User object and populate. helps with compatability with structure of GUI program by taking same classes. Ultimately move these into own files.
        self.currentUser = User()
        self.currentUser.name = inputUsername
        self.currentUser.experimentName = inputExperimentName

        self.experimentDirectory =  os.path.join(self.dataDirectory , self.currentUser , self.currentExperiment )#subdirectory within user/experiment - append epoch time to self.currentExperiment
        self.ArduinoSerial = inputArduinoPort
        self.arduino = ArduinoObject()
        self.arduino.port = inputArduinoPort
        self.arduino.baudrate = inputArduionBaudRate


        #something with Arduino objects here to reference later.
