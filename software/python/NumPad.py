import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter import Menu
from tkinter import messagebox

class NumPad:
    def __init__(self , master , parameterToBeAssigned , buttonToModify):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.frame.grid()
        self.returnValue = 0
        self.valueHasBeenEntered = 0
        self.configure_run_GUI(parameterToBeAssigned , buttonToModify)
        # currentInputValue_float = 0

    def configure_run_GUI(self , parameterToBeAssigned , buttonToModify):
        #global globalStorageVar
        def numPad_cmd_0():
            # get text from input field label as string.
            # append number associated with button
            # update current label value
            currentInputValue = lbl_tempInputField.cget("text") + "0"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_1():
            currentInputValue = lbl_tempInputField.cget("text") + "1"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_2():
            currentInputValue = lbl_tempInputField.cget("text") + "2"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_3():
            currentInputValue = lbl_tempInputField.cget("text") + "3"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_4():
            currentInputValue = lbl_tempInputField.cget("text") + "4"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_5():
            currentInputValue = lbl_tempInputField.cget("text") + "5"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_6():
            currentInputValue = lbl_tempInputField.cget("text") + "6"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_7():
            currentInputValue = lbl_tempInputField.cget("text") + "7"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_8():
            currentInputValue = lbl_tempInputField.cget("text") + "8"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_9():
            currentInputValue = lbl_tempInputField.cget("text") + "9"
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_decimal():
            currentInputValue = lbl_tempInputField.cget("text") + "."
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_delete():
            currentInputValue = lbl_tempInputField.cget("text")[:-1]
            lbl_tempInputField.configure(text=currentInputValue)

        def numPad_cmd_enterValue(parameterToBeAssigned , buttonToModify):
            currentInputValue_str = lbl_tempInputField.cget("text")
            currentInputValue_float = float(currentInputValue_str)
            #self.returnValue = currentInputValue_float
            #print("Before Change\t" + str(processParamToBeAssigned.value))
            self.valueEntered = 1
            parameterToBeAssigned.value = currentInputValue_float
            buttonToModify.configure(text = parameterToBeAssigned.paramName + ": " + str(currentInputValue_float))
            #print("After Change\t" + str(processParamToBeAssigned.value))
            #globalStorageVar = self.returnValue
            #print("Global Storage Var " + str(globalStorageVar))
            #print(currentInputValue_float)
            #print(self.returnValue)
            #print(self.valueEntered)
            #return currentInputValue_float
            #self.numPad_close()
            self.master.destroy()

        lbl_tempInputField = Label(self.frame, text="")
        lbl_tempInputField.grid(row=1, column=1)

        button_0 = Button(self.frame, text="0", command=numPad_cmd_0)
        button_0.grid(row=5, column=1)
        button_1 = Button(self.frame, text="1", command=numPad_cmd_1)
        button_1.grid(row=4, column=0)
        button_2 = Button(self.frame, text="2", command=numPad_cmd_2)
        button_2.grid(row=4, column=1)
        button_3 = Button(self.frame, text="3", command=numPad_cmd_3)
        button_3.grid(row=4, column=2)
        button_4 = Button(self.frame, text="4", command=numPad_cmd_4)
        button_4.grid(row=3, column=0)
        button_5 = Button(self.frame, text="5", command=numPad_cmd_5)
        button_5.grid(row=3, column=1)
        button_6 = Button(self.frame, text="6", command=numPad_cmd_6)
        button_6.grid(row=3, column=2)
        button_7 = Button(self.frame, text="7", command=numPad_cmd_7)
        button_7.grid(row=2, column=0)
        button_8 = Button(self.frame, text="8", command=numPad_cmd_8)
        button_8.grid(row=2, column=1)
        button_9 = Button(self.frame, text="9", command=numPad_cmd_9)
        button_9.grid(row=2, column=2)
        button_decimal = Button(self.frame, text=".", command=numPad_cmd_decimal)
        button_decimal.grid(row=5, column=0)
        button_delete = Button(self.frame, text="Delete", command=numPad_cmd_delete)
        button_delete.grid(row=5, column=2)

        button_enterValue = Button(self.frame, text="Enter Value", command= lambda: numPad_cmd_enterValue(parameterToBeAssigned , buttonToModify))
        button_enterValue.grid(row=6, column=1)



    def numPad_close(self):
        print("NumPad should close now")
        #self.destroy()
