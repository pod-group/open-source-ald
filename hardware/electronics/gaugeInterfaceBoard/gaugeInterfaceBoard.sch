EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1950 4600 0    60   Input ~ 0
digital_signal
Text GLabel 1950 4500 0    60   Input ~ 0
analog_signal
Text GLabel 1950 4400 0    60   Input ~ 0
digital_gnd
Text GLabel 1950 4300 0    60   Input ~ 0
analog_gnd
Text GLabel 7100 2650 0    60   Input ~ 0
gaugePin_1
Text GLabel 7100 2750 0    60   Input ~ 0
gaugePin_2
Text GLabel 5900 2850 0    60   Input ~ 0
gaugePin_3
Text GLabel 5900 3050 0    60   Input ~ 0
gaugePin_5
Text GLabel 7100 3150 0    60   Input ~ 0
gaugePin_6
Text GLabel 7100 3250 0    60   Input ~ 0
gaugePin_7
Text GLabel 7100 3350 0    60   Input ~ 0
gaugePin_8
Text GLabel 2850 1750 2    60   Input ~ 0
gaugePin_7
Text GLabel 2850 1650 2    60   Input ~ 0
gaugePin_2
NoConn ~ 2850 1850
Wire Wire Line
	5900 2850 6150 2850
Wire Wire Line
	4100 2250 3950 2250
Connection ~ 6150 2850
Text GLabel 4500 2050 0    60   Input ~ 0
analog_signal
Text GLabel 3950 2250 0    60   Input ~ 0
analog_gnd
Wire Wire Line
	6150 3350 5900 3350
Text GLabel 5900 3350 0    60   Input ~ 0
analog_gnd
Wire Wire Line
	4950 2250 5200 2250
Wire Wire Line
	5200 2350 5000 2350
Wire Wire Line
	5000 2350 5000 2600
Wire Wire Line
	5000 2600 4500 2600
Wire Wire Line
	6150 2250 6150 2350
Wire Wire Line
	6150 2350 5800 2350
Wire Wire Line
	5800 2250 6150 2250
Connection ~ 6150 2350
Text GLabel 4500 2600 0    60   Input ~ 0
analog_signal
Text Notes 7150 6050 0    60   ~ 0
Gauge pass-thru to \nstandalone controller\n(panel mount in front)
Text Notes 8400 2700 0    60   ~ 0
Gauge-in
$Comp
L gaugeInterfaceBoard-rescue:Conn_01x06 J1
U 1 1 5E301FE1
P 2150 4300
F 0 "J1" H 2150 4600 50  0000 C CNN
F 1 "Conn_01x06" H 2150 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 2150 4300 50  0001 C CNN
F 3 "" H 2150 4300 50  0001 C CNN
	1    2150 4300
	1    0    0    -1  
$EndComp
Text GLabel 1950 4200 0    60   Input ~ 0
24V_DC_HOT
Text GLabel 1950 4100 0    60   Input ~ 0
24V_DC_GND
Text Notes 8400 1800 0    60   ~ 0
gaugePin_1 - Electrical supply Positive\ngaugePin_2 - Electrical supply Negative\ngaugePin_3 - Pressure Signal\ngaugePin_4 - Gauge Identification\ngaugePin_5 - Signal ground/common\ngaugePin_6 - set-point output signal\ngaugePin_7 -  \t AIM: gauge enable (strike)\n \t APG: remote calibration\ngaugePin_8 - \t AIM: set-point trip level\n\t APG: not connected
Text GLabel 7100 4650 0    60   Input ~ 0
gaugePin_1
Text GLabel 7100 2950 0    60   Input ~ 0
gaugePin_4
Text GLabel 7100 4950 0    60   Input ~ 0
gaugePin_4
Text GLabel 7100 4850 0    60   Input ~ 0
gaugePin_3
Text GLabel 7100 4750 0    60   Input ~ 0
gaugePin_2
Text GLabel 7100 5050 0    60   Input ~ 0
gaugePin_5
Text GLabel 7100 5150 0    60   Input ~ 0
gaugePin_6
Text GLabel 7100 5250 0    60   Input ~ 0
gaugePin_7
Text GLabel 7100 5350 0    60   Input ~ 0
gaugePin_8
Text GLabel 5200 1400 0    60   Input ~ 0
24V_DC_HOT
Text GLabel 5200 1500 0    60   Input ~ 0
24V_DC_GND
Text GLabel 5800 1400 2    60   Input ~ 0
gaugePin_1
Text GLabel 5800 1500 2    60   Input ~ 0
gaugePin_2
Text Notes 4600 1050 0    60   ~ 0
2 sets of DIP switches to control \n•Voltage range (0-5V or 0-10V)\n•Power from external gauge controller\n\t or from own system
Text GLabel 1600 5950 2    60   Input ~ 0
24V_DC_GND
Text GLabel 1600 6400 2    60   Input ~ 0
24V_DC_GND
Text GLabel 1600 6850 2    60   Input ~ 0
24V_DC_GND
Text GLabel 1600 7300 2    60   Input ~ 0
24V_DC_GND
Text Notes 8400 4950 0    60   ~ 0
Gauge-out
Wire Wire Line
	6150 2850 7100 2850
Wire Wire Line
	6150 2350 6150 2850
$Comp
L Connector:RJ45 J2
U 1 1 5E31C4AF
P 7500 2950
F 0 "J2" H 7170 2954 50  0000 R CNN
F 1 "RJ45" H 7170 3045 50  0000 R CNN
F 2 "Connectors:RJ45_8" V 7500 2975 50  0001 C CNN
F 3 "~" V 7500 2975 50  0001 C CNN
	1    7500 2950
	-1   0    0    1   
$EndComp
$Comp
L Connector:RJ45 J3
U 1 1 5E31F077
P 7500 4950
F 0 "J3" H 7170 4954 50  0000 R CNN
F 1 "RJ45" H 7170 5045 50  0000 R CNN
F 2 "Connectors:RJ45_8" V 7500 4975 50  0001 C CNN
F 3 "~" V 7500 4975 50  0001 C CNN
	1    7500 4950
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_DIP_x02 SW1
U 1 1 5E3259E0
P 5500 1500
F 0 "SW1" H 5500 1867 50  0000 C CNN
F 1 "SW_DIP_x02" H 5500 1776 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x2_W7.62mm_Slide" H 5500 1500 50  0001 C CNN
F 3 "~" H 5500 1500 50  0001 C CNN
	1    5500 1500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x02 SW2
U 1 1 5E326376
P 5500 2350
F 0 "SW2" H 5500 2717 50  0000 C CNN
F 1 "SW_DIP_x02" H 5500 2626 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x2_W7.62mm_Slide" H 5500 2350 50  0001 C CNN
F 3 "~" H 5500 2350 50  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3050 6150 3050
$Comp
L Device:R R3
U 1 1 5E32736C
P 6150 3200
F 0 "R3" H 6080 3154 50  0000 R CNN
F 1 "0R" H 6080 3245 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6080 3200 50  0001 C CNN
F 3 "~" H 6150 3200 50  0001 C CNN
	1    6150 3200
	-1   0    0    1   
$EndComp
Connection ~ 6150 3050
Wire Wire Line
	6150 3050 7100 3050
$Comp
L Device:R R1
U 1 1 5E328A41
P 4250 2250
F 0 "R1" V 4350 2250 50  0000 C CNN
F 1 "10KR" V 4134 2250 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 4180 2250 50  0001 C CNN
F 3 "~" H 4250 2250 50  0001 C CNN
	1    4250 2250
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5E329314
P 4800 2250
F 0 "R2" V 4900 2250 50  0000 C CNN
F 1 "10KR" V 4684 2250 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 4730 2250 50  0001 C CNN
F 3 "~" H 4800 2250 50  0001 C CNN
	1    4800 2250
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5E32A378
P 1600 5850
F 0 "H1" H 1700 5899 50  0000 L CNN
F 1 "MountingHole_Pad" H 1700 6000 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1600 5850 50  0001 C CNN
F 3 "~" H 1600 5850 50  0001 C CNN
	1    1600 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5E32ACE9
P 1600 6300
F 0 "H2" H 1700 6349 50  0000 L CNN
F 1 "MountingHole_Pad" H 1700 6450 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1600 6300 50  0001 C CNN
F 3 "~" H 1600 6300 50  0001 C CNN
	1    1600 6300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5E32B0DD
P 1600 6750
F 0 "H3" H 1700 6799 50  0000 L CNN
F 1 "MountingHole_Pad" H 1700 6900 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1600 6750 50  0001 C CNN
F 3 "~" H 1600 6750 50  0001 C CNN
	1    1600 6750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5E32B699
P 1600 7200
F 0 "H4" H 1700 7249 50  0000 L CNN
F 1 "MountingHole_Pad" H 1700 7350 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1600 7200 50  0001 C CNN
F 3 "~" H 1600 7200 50  0001 C CNN
	1    1600 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2250 4500 2250
Wire Wire Line
	4500 2050 4500 2250
Connection ~ 4500 2250
Wire Wire Line
	4500 2250 4650 2250
Wire Wire Line
	7100 3350 7100 3500
Wire Wire Line
	7100 5350 7100 5500
Text GLabel 7100 3500 3    50   Input ~ 0
24V_DC_GND
Text GLabel 7100 5500 3    50   Input ~ 0
24V_DC_GND
$Comp
L myParts:ASSR-1611-100E U1
U 1 1 5E439443
P 2550 1750
F 0 "U1" H 2550 2075 50  0000 C CNN
F 1 "ASSR-1611-100E" H 2550 1984 50  0000 C CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 2350 1550 50  0001 L CIN
F 3 "" H 2500 1800 50  0001 L CNN
	1    2550 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5E33DDF7
P 2000 2350
F 0 "R4" H 2070 2396 50  0000 L CNN
F 1 "470R" H 2070 2305 50  0000 L CNN
F 2 "Resistors_SMD:R_2512" V 1930 2350 50  0001 C CNN
F 3 "~" H 2000 2350 50  0001 C CNN
	1    2000 2350
	1    0    0    -1  
$EndComp
Text GLabel 2000 2500 3    60   Input ~ 0
digital_gnd
Wire Wire Line
	2000 1750 2000 2200
Wire Wire Line
	2250 1750 2000 1750
Text GLabel 2250 1650 0    60   Input ~ 0
digital_signal
$EndSCHEMATC
