EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:nxp
LIBS:onsemi
LIBS:Oscillators
LIBS:Power_Management
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:gaugeInterfaceBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RJ45 J2
U 1 1 5E29A588
P 7550 3000
F 0 "J2" H 7750 3500 50  0000 C CNN
F 1 "RJ45" H 7400 3500 50  0000 C CNN
F 2 "Connectors:RJ45_8" H 7550 3000 50  0001 C CNN
F 3 "" H 7550 3000 50  0001 C CNN
	1    7550 3000
	0    1    1    0   
$EndComp
$Comp
L ASSR-1611-001E K1
U 1 1 5E29BA4E
P 1950 1650
F 0 "K1" H 2700 1950 50  0000 L CNN
F 1 "ASSR-1611-001E" H 2700 1850 50  0000 L CNN
F 2 "myParts:ASSR-1611-001E" H 2700 1750 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ASSR-1611-001E.pdf" H 2700 850 50  0001 L CNN
F 4 "Relay SSR 2.5A 60V AC/DC-o/p PDIP6 Solid State Relay 2.5 A PCB Mount MOSFET, 1.7 V" H 2700 1550 50  0001 L CNN "Description"
F 5 "4.7" H 2700 1450 50  0001 L CNN "Height"
F 6 "630-ASSR-1611-001E" H 2700 1350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=630-ASSR-1611-001E" H 2700 950 50  0001 L CNN "Mouser Price/Stock"
F 8 "Avago Technologies" H 2700 1150 50  0001 L CNN "Manufacturer_Name"
F 9 "ASSR-1611-001E" H 2700 1050 50  0001 L CNN "Manufacturer_Part_Number"
	1    1950 1650
	1    0    0    -1  
$EndComp
Text GLabel 1950 4100 0    60   Input ~ 0
digital_signal
Text GLabel 1950 4200 0    60   Input ~ 0
analog_signal
Text GLabel 1950 4300 0    60   Input ~ 0
digital_gnd
Text GLabel 1950 4400 0    60   Input ~ 0
analog_gnd
Text GLabel 7100 2650 0    60   Input ~ 0
gaugePin_1
Text GLabel 7100 2750 0    60   Input ~ 0
gaugePin_2
Text GLabel 5900 2850 0    60   Input ~ 0
gaugePin_3
Text GLabel 5900 3050 0    60   Input ~ 0
gaugePin_5
Text GLabel 7100 3150 0    60   Input ~ 0
gaugePin_6
Text GLabel 7100 3250 0    60   Input ~ 0
gaugePin_7
Text GLabel 7100 3350 0    60   Input ~ 0
gaugePin_8
Text GLabel 7900 3550 2    60   Input ~ 0
24V_DC_GND
Text GLabel 2850 1750 2    60   Input ~ 0
gaugePin_7
Text GLabel 2850 1650 2    60   Input ~ 0
gaugePin_2
Text GLabel 1950 1650 0    60   Input ~ 0
digital_signal
NoConn ~ 2850 1850
Wire Wire Line
	1950 1750 1700 1750
Wire Wire Line
	1700 1750 1700 2200
$Comp
L R R_in1
U 1 1 5E29C210
P 1700 2350
F 0 "R_in1" V 1780 2350 50  0000 C CNN
F 1 "470R" V 1700 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 1630 2350 50  0001 C CNN
F 3 "" H 1700 2350 50  0001 C CNN
	1    1700 2350
	1    0    0    -1  
$EndComp
Text GLabel 1700 2500 3    60   Input ~ 0
digital_gnd
Wire Wire Line
	5900 2850 7100 2850
$Comp
L R R_g1
U 1 1 5E29C859
P 4800 2250
F 0 "R_g1" V 4880 2250 50  0000 C CNN
F 1 "10KR" V 4800 2250 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 4730 2250 50  0001 C CNN
F 3 "" H 4800 2250 50  0001 C CNN
	1    4800 2250
	0    1    1    0   
$EndComp
$Comp
L R R_g2
U 1 1 5E29C898
P 4250 2250
F 0 "R_g2" V 4330 2250 50  0000 C CNN
F 1 "10Kr" V 4250 2250 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 4180 2250 50  0001 C CNN
F 3 "" H 4250 2250 50  0001 C CNN
	1    4250 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 2250 4650 2250
Wire Wire Line
	4100 2250 3950 2250
Connection ~ 4500 2250
Wire Wire Line
	4500 2250 4500 2050
Connection ~ 6150 2850
Text GLabel 4500 2050 0    60   Input ~ 0
analog_signal
Wire Wire Line
	7100 3050 5900 3050
Text GLabel 3950 2250 0    60   Input ~ 0
analog_gnd
Connection ~ 6150 3050
Wire Wire Line
	6150 3350 5900 3350
Text GLabel 5900 3350 0    60   Input ~ 0
analog_gnd
$Comp
L R R_g3
U 1 1 5E29CD66
P 6150 3200
F 0 "R_g3" V 6230 3200 50  0000 C CNN
F 1 "0R" V 6150 3200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6080 3200 50  0001 C CNN
F 3 "" H 6150 3200 50  0001 C CNN
	1    6150 3200
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x02 SW2
U 1 1 5E3019E6
P 5500 2350
F 0 "SW2" H 5500 2600 50  0000 C CNN
F 1 "SW_DIP_x02" H 5500 2200 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x2_W7.62mm_Slide" H 5500 2350 50  0001 C CNN
F 3 "" H 5500 2350 50  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2250 5200 2250
Wire Wire Line
	5200 2350 5000 2350
Wire Wire Line
	5000 2350 5000 2600
Wire Wire Line
	5000 2600 4500 2600
Wire Wire Line
	6150 2250 6150 2850
Wire Wire Line
	6150 2350 5800 2350
Wire Wire Line
	5800 2250 6150 2250
Connection ~ 6150 2350
Text GLabel 4500 2600 0    60   Input ~ 0
analog_signal
$Comp
L RJ45 J3
U 1 1 5E301F4F
P 7550 5000
F 0 "J3" H 7750 5500 50  0000 C CNN
F 1 "RJ45" H 7400 5500 50  0000 C CNN
F 2 "Connectors:RJ45_8" H 7550 5000 50  0001 C CNN
F 3 "" H 7550 5000 50  0001 C CNN
	1    7550 5000
	0    1    1    0   
$EndComp
Text Notes 7150 6050 0    60   ~ 0
Gauge pass-thru to \nstandalone controller\n(panel mount in front)
Text Notes 8400 2700 0    60   ~ 0
Gauge-in
$Comp
L Conn_01x06 J1
U 1 1 5E301FE1
P 2150 4300
F 0 "J1" H 2150 4600 50  0000 C CNN
F 1 "Conn_01x06" H 2150 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 2150 4300 50  0001 C CNN
F 3 "" H 2150 4300 50  0001 C CNN
	1    2150 4300
	1    0    0    -1  
$EndComp
Text GLabel 1950 4500 0    60   Input ~ 0
24V_DC_HOT
Text GLabel 1950 4600 0    60   Input ~ 0
24V_DC_GND
Text Notes 8400 1800 0    60   ~ 0
gaugePin_1 - Electrical supply Positive\ngaugePin_2 - Electrical supply Negative\ngaugePin_3 - Pressure Signal\ngaugePin_4 - Gauge Identification\ngaugePin_5 - Signal ground/common\ngaugePin_6 - set-point output signal\ngaugePin_7 - 	AIM: gauge enable (strike)\n																APG: remote calibration\ngaugePin_8 - AIM: set-point trip level\n																APG: not connected
Text GLabel 7900 5550 2    60   Input ~ 0
24V_DC_GND
Text GLabel 7100 4650 0    60   Input ~ 0
gaugePin_1
Text GLabel 7100 2950 0    60   Input ~ 0
gaugePin_4
Text GLabel 7100 4950 0    60   Input ~ 0
gaugePin_4
Text GLabel 7100 4850 0    60   Input ~ 0
gaugePin_3
Text GLabel 7100 4750 0    60   Input ~ 0
gaugePin_2
Text GLabel 7100 5050 0    60   Input ~ 0
gaugePin_5
Text GLabel 7100 5150 0    60   Input ~ 0
gaugePin_6
Text GLabel 7100 5250 0    60   Input ~ 0
gaugePin_7
Text GLabel 7100 5350 0    60   Input ~ 0
gaugePin_8
$Comp
L SW_DIP_x02 SW1
U 1 1 5E30245F
P 5500 1500
F 0 "SW1" H 5500 1750 50  0000 C CNN
F 1 "SW_DIP_x02" H 5500 1350 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x2_W7.62mm_Slide" H 5500 1500 50  0001 C CNN
F 3 "" H 5500 1500 50  0001 C CNN
	1    5500 1500
	1    0    0    -1  
$EndComp
Text GLabel 5200 1400 0    60   Input ~ 0
24V_DC_HOT
Text GLabel 5200 1500 0    60   Input ~ 0
24V_DC_GND
Text GLabel 5800 1400 2    60   Input ~ 0
gaugePin_1
Text GLabel 5800 1500 2    60   Input ~ 0
gaugePin_2
Text Notes 4600 1050 0    60   ~ 0
2 sets of DIP switches to control \n•Voltage range (0-5V or 0-10V)\n•Power from external gauge controller \n	or from own system
$Comp
L Mounting_Hole_PAD MK1
U 1 1 5E304026
P 1600 5850
F 0 "MK1" H 1600 6100 50  0000 C CNN
F 1 "Mounting_Hole_PAD" H 1600 6025 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5.3mm_M5_DIN965_Pad" H 1600 5850 50  0001 C CNN
F 3 "" H 1600 5850 50  0001 C CNN
	1    1600 5850
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole_PAD MK4
U 1 1 5E3041E6
P 1600 7200
F 0 "MK4" H 1600 7450 50  0000 C CNN
F 1 "Mounting_Hole_PAD" H 1600 7375 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5.3mm_M5_DIN965_Pad" H 1600 7200 50  0001 C CNN
F 3 "" H 1600 7200 50  0001 C CNN
	1    1600 7200
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole_PAD MK2
U 1 1 5E3040AF
P 1600 6300
F 0 "MK2" H 1600 6550 50  0000 C CNN
F 1 "Mounting_Hole_PAD" H 1600 6475 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5.3mm_M5_DIN965_Pad" H 1600 6300 50  0001 C CNN
F 3 "" H 1600 6300 50  0001 C CNN
	1    1600 6300
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole_PAD MK3
U 1 1 5E3041A5
P 1600 6750
F 0 "MK3" H 1600 7000 50  0000 C CNN
F 1 "Mounting_Hole_PAD" H 1600 6925 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5.3mm_M5_DIN965_Pad" H 1600 6750 50  0001 C CNN
F 3 "" H 1600 6750 50  0001 C CNN
	1    1600 6750
	1    0    0    -1  
$EndComp
Text GLabel 1600 5950 2    60   Input ~ 0
24V_DC_GND
Text GLabel 1600 6400 2    60   Input ~ 0
24V_DC_GND
Text GLabel 1600 6850 2    60   Input ~ 0
24V_DC_GND
Text GLabel 1600 7300 2    60   Input ~ 0
24V_DC_GND
Text Notes 8400 4950 0    60   ~ 0
Gauge-out
$Comp
L RJ45 J?
U 1 1 5E3057E5
P 4750 5350
F 0 "J?" H 4950 5850 50  0000 C CNN
F 1 "RJ45" H 4600 5850 50  0000 C CNN
F 2 "" H 4750 5350 50  0001 C CNN
F 3 "" H 4750 5350 50  0001 C CNN
	1    4750 5350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
