EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x06 conn_gauge_0
U 1 1 5E33E341
P 6800 2100
F 0 "conn_gauge_0" H 6600 2400 50  0000 L CNN
F 1 "Conn_01x06" H 6600 1700 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 6800 2100 50  0001 C CNN
F 3 "~" H 6800 2100 50  0001 C CNN
	1    6800 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 conn_gauge_1
U 1 1 5E33EAC0
P 8000 2100
F 0 "conn_gauge_1" H 7800 2400 50  0000 L CNN
F 1 "Conn_01x06" H 7800 1700 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 8000 2100 50  0001 C CNN
F 3 "~" H 8000 2100 50  0001 C CNN
	1    8000 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 conn_gauge_3
U 1 1 5E33F001
P 8000 3000
F 0 "conn_gauge_3" H 7800 3300 50  0000 L CNN
F 1 "Conn_01x06" H 7800 2600 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 8000 3000 50  0001 C CNN
F 3 "~" H 8000 3000 50  0001 C CNN
	1    8000 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 conn_gauge_2
U 1 1 5E33F89A
P 6800 3000
F 0 "conn_gauge_2" H 6600 3300 50  0000 L CNN
F 1 "Conn_01x06" H 6600 2600 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 6800 3000 50  0001 C CNN
F 3 "~" H 6800 3000 50  0001 C CNN
	1    6800 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 conn_MFC_3
U 1 1 5E343291
P 15600 2100
F 0 "conn_MFC_3" H 15400 2600 50  0000 L CNN
F 1 "Conn_01x10" H 15400 1500 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 15600 2100 50  0001 C CNN
F 3 "~" H 15600 2100 50  0001 C CNN
	1    15600 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J3
U 1 1 5E343C10
P 12650 5850
F 0 "J3" H 12600 6350 50  0000 L CNN
F 1 "Conn_01x10" H 12450 5250 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 12650 5850 50  0001 C CNN
F 3 "~" H 12650 5850 50  0001 C CNN
	1    12650 5850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J4
U 1 1 5E3444A6
P 13150 5850
F 0 "J4" H 13100 6350 50  0000 L CNN
F 1 "Conn_01x10" H 12950 5250 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 13150 5850 50  0001 C CNN
F 3 "~" H 13150 5850 50  0001 C CNN
	1    13150 5850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J7
U 1 1 5E3451E4
P 13700 5850
F 0 "J7" H 13650 6350 50  0000 L CNN
F 1 "Conn_01x10" H 13500 5250 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 13700 5850 50  0001 C CNN
F 3 "~" H 13700 5850 50  0001 C CNN
	1    13700 5850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J8
U 1 1 5E34631F
P 14300 5850
F 0 "J8" H 14250 6350 50  0000 L CNN
F 1 "Conn_01x10" H 14100 5250 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 14300 5850 50  0001 C CNN
F 3 "~" H 14300 5850 50  0001 C CNN
	1    14300 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_temp_1
U 1 1 5E34778A
P 12550 4700
F 0 "H_temp_1" H 12650 4749 50  0000 L CNN
F 1 "MountingHole_Pad" H 12650 4658 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 12550 4700 50  0001 C CNN
F 3 "~" H 12550 4700 50  0001 C CNN
	1    12550 4700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_temp_3
U 1 1 5E347FAD
P 12550 5050
F 0 "H_temp_3" H 12650 5099 50  0000 L CNN
F 1 "MountingHole_Pad" H 12650 5008 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 12550 5050 50  0001 C CNN
F 3 "~" H 12550 5050 50  0001 C CNN
	1    12550 5050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_temp_4
U 1 1 5E348688
P 13550 5050
F 0 "H_temp_4" H 13650 5099 50  0000 L CNN
F 1 "MountingHole_Pad" H 13650 5008 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 13550 5050 50  0001 C CNN
F 3 "~" H 13550 5050 50  0001 C CNN
	1    13550 5050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_temp_2
U 1 1 5E348E2F
P 13550 4700
F 0 "H_temp_2" H 13650 4749 50  0000 L CNN
F 1 "MountingHole_Pad" H 13650 4658 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 13550 4700 50  0001 C CNN
F 3 "~" H 13550 4700 50  0001 C CNN
	1    13550 4700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_MFC_1
U 1 1 5E3494DA
P 12200 900
F 0 "H_MFC_1" H 12300 949 50  0000 L CNN
F 1 "MountingHole_Pad" H 12300 858 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 12200 900 50  0001 C CNN
F 3 "~" H 12200 900 50  0001 C CNN
	1    12200 900 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_MFC_2
U 1 1 5E349AFB
P 13150 900
F 0 "H_MFC_2" H 13250 949 50  0000 L CNN
F 1 "MountingHole_Pad" H 13250 858 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 13150 900 50  0001 C CNN
F 3 "~" H 13150 900 50  0001 C CNN
	1    13150 900 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_MFC_4
U 1 1 5E34A033
P 13150 1250
F 0 "H_MFC_4" H 13250 1299 50  0000 L CNN
F 1 "MountingHole_Pad" H 13250 1208 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 13150 1250 50  0001 C CNN
F 3 "~" H 13150 1250 50  0001 C CNN
	1    13150 1250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_gauge_2
U 1 1 5E34AA94
P 6850 950
F 0 "H_gauge_2" H 6950 999 50  0000 L CNN
F 1 "MountingHole_Pad" H 6950 908 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 6850 950 50  0001 C CNN
F 3 "~" H 6850 950 50  0001 C CNN
	1    6850 950 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_gauge_3
U 1 1 5E34B07B
P 6000 1300
F 0 "H_gauge_3" H 6100 1349 50  0000 L CNN
F 1 "MountingHole_Pad" H 6100 1258 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 6000 1300 50  0001 C CNN
F 3 "~" H 6000 1300 50  0001 C CNN
	1    6000 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_gauge_4
U 1 1 5E34B46F
P 6850 1300
F 0 "H_gauge_4" H 6950 1349 50  0000 L CNN
F 1 "MountingHole_Pad" H 6950 1258 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 6850 1300 50  0001 C CNN
F 3 "~" H 6850 1300 50  0001 C CNN
	1    6850 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_gauge_1
U 1 1 5E34BA26
P 6000 950
F 0 "H_gauge_1" H 6100 999 50  0000 L CNN
F 1 "MountingHole_Pad" H 6100 908 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 6000 950 50  0001 C CNN
F 3 "~" H 6000 950 50  0001 C CNN
	1    6000 950 
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_3
U 1 1 5E34C21F
P 4750 1000
F 0 "conn_sol_3" H 4550 1200 50  0000 L CNN
F 1 "Conn_01x04" H 4550 700 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 4750 1000 50  0001 C CNN
F 3 "~" H 4750 1000 50  0001 C CNN
	1    4750 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_2
U 1 1 5E34DBB4
P 3700 1000
F 0 "conn_sol_2" H 3500 1200 50  0000 L CNN
F 1 "Conn_01x04" H 3500 700 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 3700 1000 50  0001 C CNN
F 3 "~" H 3700 1000 50  0001 C CNN
	1    3700 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_1
U 1 1 5E34E650
P 2650 1000
F 0 "conn_sol_1" H 2450 1200 50  0000 L CNN
F 1 "Conn_01x04" H 2450 700 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 2650 1000 50  0001 C CNN
F 3 "~" H 2650 1000 50  0001 C CNN
	1    2650 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_0
U 1 1 5E34EEB6
P 1600 1000
F 0 "conn_sol_0" H 1400 1200 50  0000 L CNN
F 1 "Conn_01x04" H 1400 700 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 1600 1000 50  0001 C CNN
F 3 "~" H 1600 1000 50  0001 C CNN
	1    1600 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_4
U 1 1 5E34F60F
P 1600 1750
F 0 "conn_sol_4" H 1400 1950 50  0000 L CNN
F 1 "Conn_01x04" H 1400 1450 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 1600 1750 50  0001 C CNN
F 3 "~" H 1600 1750 50  0001 C CNN
	1    1600 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_5
U 1 1 5E35009C
P 2650 1750
F 0 "conn_sol_5" H 2450 1950 50  0000 L CNN
F 1 "Conn_01x04" H 2450 1450 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 2650 1750 50  0001 C CNN
F 3 "~" H 2650 1750 50  0001 C CNN
	1    2650 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_6
U 1 1 5E35065F
P 3700 1750
F 0 "conn_sol_6" H 3500 1950 50  0000 L CNN
F 1 "Conn_01x04" H 3500 1450 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 3700 1750 50  0001 C CNN
F 3 "~" H 3700 1750 50  0001 C CNN
	1    3700 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_7
U 1 1 5E35101C
P 4750 1750
F 0 "conn_sol_7" H 4550 1950 50  0000 L CNN
F 1 "Conn_01x04" H 4550 1450 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 4750 1750 50  0001 C CNN
F 3 "~" H 4750 1750 50  0001 C CNN
	1    4750 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_8
U 1 1 5E35148B
P 1600 2500
F 0 "conn_sol_8" H 1400 2700 50  0000 L CNN
F 1 "Conn_01x04" H 1400 2200 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 1600 2500 50  0001 C CNN
F 3 "~" H 1600 2500 50  0001 C CNN
	1    1600 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_9
U 1 1 5E3521DA
P 2650 2500
F 0 "conn_sol_9" H 2450 2700 50  0000 L CNN
F 1 "Conn_01x04" H 2450 2200 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 2650 2500 50  0001 C CNN
F 3 "~" H 2650 2500 50  0001 C CNN
	1    2650 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_10
U 1 1 5E35280D
P 3700 2500
F 0 "conn_sol_10" H 3500 2700 50  0000 L CNN
F 1 "Conn_01x04" H 3500 2200 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 3700 2500 50  0001 C CNN
F 3 "~" H 3700 2500 50  0001 C CNN
	1    3700 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_11
U 1 1 5E35348C
P 4750 2500
F 0 "conn_sol_11" H 4550 2700 50  0000 L CNN
F 1 "Conn_01x04" H 4550 2200 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 4750 2500 50  0001 C CNN
F 3 "~" H 4750 2500 50  0001 C CNN
	1    4750 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_15
U 1 1 5E3541AB
P 4750 3250
F 0 "conn_sol_15" H 4550 3450 50  0000 L CNN
F 1 "Conn_01x04" H 4550 2950 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 4750 3250 50  0001 C CNN
F 3 "~" H 4750 3250 50  0001 C CNN
	1    4750 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_14
U 1 1 5E3547AE
P 3700 3250
F 0 "conn_sol_14" H 3500 3450 50  0000 L CNN
F 1 "Conn_01x04" H 3500 2950 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 3700 3250 50  0001 C CNN
F 3 "~" H 3700 3250 50  0001 C CNN
	1    3700 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_13
U 1 1 5E3552B3
P 2650 3250
F 0 "conn_sol_13" H 2450 3450 50  0000 L CNN
F 1 "Conn_01x04" H 2450 2950 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 2650 3250 50  0001 C CNN
F 3 "~" H 2650 3250 50  0001 C CNN
	1    2650 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn_sol_12
U 1 1 5E355E16
P 1600 3250
F 0 "conn_sol_12" H 1400 3450 50  0000 L CNN
F 1 "Conn_01x04" H 1400 2950 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 1600 3250 50  0001 C CNN
F 3 "~" H 1600 3250 50  0001 C CNN
	1    1600 3250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_arduino_1
U 1 1 5E35F34B
P 1750 6400
F 0 "H_arduino_1" H 1850 6449 50  0000 L CNN
F 1 "MountingHole_Pad" H 1850 6358 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1750 6400 50  0001 C CNN
F 3 "~" H 1750 6400 50  0001 C CNN
	1    1750 6400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_arduino_2
U 1 1 5E35FCDA
P 1750 6700
F 0 "H_arduino_2" H 1850 6749 50  0000 L CNN
F 1 "MountingHole_Pad" H 1850 6658 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1750 6700 50  0001 C CNN
F 3 "~" H 1750 6700 50  0001 C CNN
	1    1750 6700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_arduino_3
U 1 1 5E36069F
P 1750 7000
F 0 "H_arduino_3" H 1850 7049 50  0000 L CNN
F 1 "MountingHole_Pad" H 1850 6958 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1750 7000 50  0001 C CNN
F 3 "~" H 1750 7000 50  0001 C CNN
	1    1750 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_arduino_4
U 1 1 5E360E74
P 1750 7300
F 0 "H_arduino_4" H 1850 7349 50  0000 L CNN
F 1 "MountingHole_Pad" H 1850 7258 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1750 7300 50  0001 C CNN
F 3 "~" H 1750 7300 50  0001 C CNN
	1    1750 7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_arduino_5
U 1 1 5E36172B
P 1750 7600
F 0 "H_arduino_5" H 1850 7649 50  0000 L CNN
F 1 "MountingHole_Pad" H 1850 7558 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1750 7600 50  0001 C CNN
F 3 "~" H 1750 7600 50  0001 C CNN
	1    1750 7600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_arduino_6
U 1 1 5E361EBD
P 1750 7900
F 0 "H_arduino_6" H 1850 7949 50  0000 L CNN
F 1 "MountingHole_Pad" H 1850 7858 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 1750 7900 50  0001 C CNN
F 3 "~" H 1750 7900 50  0001 C CNN
	1    1750 7900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_1
U 1 1 5E3627D8
P 9650 9950
F 0 "H_mount_1" H 9750 10000 50  0000 L CNN
F 1 "MountingHole_Pad" H 9750 9908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9650 9950 50  0001 C CNN
F 3 "~" H 9650 9950 50  0001 C CNN
	1    9650 9950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_2
U 1 1 5E3631D3
P 9650 10250
F 0 "H_mount_2" H 9750 10299 50  0000 L CNN
F 1 "MountingHole_Pad" H 9750 10208 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9650 10250 50  0001 C CNN
F 3 "~" H 9650 10250 50  0001 C CNN
	1    9650 10250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_3
U 1 1 5E363A73
P 9650 10550
F 0 "H_mount_3" H 9750 10599 50  0000 L CNN
F 1 "MountingHole_Pad" H 9750 10508 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9650 10550 50  0001 C CNN
F 3 "~" H 9650 10550 50  0001 C CNN
	1    9650 10550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_4
U 1 1 5E364263
P 9650 10850
F 0 "H_mount_4" H 9750 10899 50  0000 L CNN
F 1 "MountingHole_Pad" H 9750 10808 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9650 10850 50  0001 C CNN
F 3 "~" H 9650 10850 50  0001 C CNN
	1    9650 10850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_5
U 1 1 5E364B45
P 10550 9950
F 0 "H_mount_5" H 10650 9999 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 9908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 10550 9950 50  0001 C CNN
F 3 "~" H 10550 9950 50  0001 C CNN
	1    10550 9950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_6
U 1 1 5E365305
P 10550 10250
F 0 "H_mount_6" H 10650 10299 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 10208 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 10550 10250 50  0001 C CNN
F 3 "~" H 10550 10250 50  0001 C CNN
	1    10550 10250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_7
U 1 1 5E365C03
P 10550 10550
F 0 "H_mount_7" H 10650 10599 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 10508 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 10550 10550 50  0001 C CNN
F 3 "~" H 10550 10550 50  0001 C CNN
	1    10550 10550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_mount_8
U 1 1 5E3666B1
P 10550 10850
F 0 "H_mount_8" H 10650 10899 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 10808 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 10550 10850 50  0001 C CNN
F 3 "~" H 10550 10850 50  0001 C CNN
	1    10550 10850
	1    0    0    -1  
$EndComp
Text Notes 9750 9750 0    50   ~ 0
mounting holes to rack mount chassis
Text GLabel 1400 1000 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 1400 900  0    50   Input ~ 0
24V_DC_GND
Text GLabel 2450 1000 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 3500 1000 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 4550 1000 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 1400 1750 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 2450 1750 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 3500 1750 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 4550 1750 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 1400 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 2450 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 3500 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 4550 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 1400 3250 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 2450 3250 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 3500 3250 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 4550 3250 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 2450 900  0    50   Input ~ 0
24V_DC_GND
Text GLabel 3500 900  0    50   Input ~ 0
24V_DC_GND
Text GLabel 4550 900  0    50   Input ~ 0
24V_DC_GND
Text GLabel 1400 1650 0    50   Input ~ 0
24V_DC_GND
Text GLabel 2450 1650 0    50   Input ~ 0
24V_DC_GND
Text GLabel 3500 1650 0    50   Input ~ 0
24V_DC_GND
Text GLabel 4550 1650 0    50   Input ~ 0
24V_DC_GND
Text GLabel 1400 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 2450 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 3500 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 4550 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 1400 3150 0    50   Input ~ 0
24V_DC_GND
Text GLabel 2450 3150 0    50   Input ~ 0
24V_DC_GND
Text GLabel 3500 3150 0    50   Input ~ 0
24V_DC_GND
Text GLabel 4550 3150 0    50   Input ~ 0
24V_DC_GND
Text GLabel 2450 1100 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 3500 1100 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 4550 1100 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 4550 1850 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 4550 2600 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 4550 3350 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 3500 3350 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 3500 2600 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 3500 1850 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 2450 1850 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 2450 2600 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 2450 3350 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 1400 1100 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 1400 1850 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 1400 2600 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 1400 3350 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 1400 1200 0    50   Input ~ 0
arduino_digital_25
Text GLabel 2450 1200 0    50   Input ~ 0
arduino_digital_29
Text GLabel 3500 1200 0    50   Input ~ 0
arduino_digital_33
Text GLabel 4550 1200 0    50   Input ~ 0
arduino_digital_37
Text GLabel 1400 1950 0    50   Input ~ 0
arduino_digital_41
Text GLabel 2450 1950 0    50   Input ~ 0
arduino_digital_45
Text GLabel 3500 1950 0    50   Input ~ 0
arduino_digital_49
Text GLabel 4550 1950 0    50   Input ~ 0
arduino_digital_53
Text GLabel 1400 2700 0    50   Input ~ 0
arduino_digital_23
Text GLabel 2450 2700 0    50   Input ~ 0
arduino_digital_27
Text GLabel 3500 2700 0    50   Input ~ 0
arduino_digital_31
Text GLabel 1400 3450 0    50   Input ~ 0
arduino_digital_39
Text GLabel 2450 3450 0    50   Input ~ 0
arduino_digital_43
Text GLabel 3500 3450 0    50   Input ~ 0
arduino_digital_47
Text GLabel 4550 3450 0    50   Input ~ 0
arduino_digital_51
Text GLabel 6000 1100 2    50   Input ~ 0
24V_DC_GND
Text GLabel 6850 1100 2    50   Input ~ 0
24V_DC_GND
Text GLabel 6000 1450 2    50   Input ~ 0
24V_DC_GND
Text GLabel 6850 1450 2    50   Input ~ 0
24V_DC_GND
Wire Wire Line
	6000 1050 6000 1100
Wire Wire Line
	6850 1050 6850 1100
Wire Wire Line
	6850 1400 6850 1450
Wire Wire Line
	6000 1400 6000 1450
$Comp
L Mechanical:MountingHole_Pad H_MFC_3
U 1 1 5E34A637
P 12200 1250
F 0 "H_MFC_3" H 12300 1299 50  0000 L CNN
F 1 "MountingHole_Pad" H 12300 1208 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 12200 1250 50  0001 C CNN
F 3 "~" H 12200 1250 50  0001 C CNN
	1    12200 1250
	1    0    0    -1  
$EndComp
Text GLabel 12200 1050 2    50   Input ~ 0
24V_DC_GND
Text GLabel 13150 1050 2    50   Input ~ 0
24V_DC_GND
Text GLabel 12200 1400 2    50   Input ~ 0
24V_DC_GND
Text GLabel 13150 1400 2    50   Input ~ 0
24V_DC_GND
Wire Wire Line
	12200 1000 12200 1050
Wire Wire Line
	13150 1000 13150 1050
Wire Wire Line
	13150 1350 13150 1400
Wire Wire Line
	12200 1400 12200 1350
Text GLabel 12550 4850 2    50   Input ~ 0
24V_DC_GND
Text GLabel 13550 4850 2    50   Input ~ 0
24V_DC_GND
Text GLabel 12550 5200 2    50   Input ~ 0
24V_DC_GND
Text GLabel 13550 5200 2    50   Input ~ 0
24V_DC_GND
Wire Wire Line
	12550 4800 12550 4850
Wire Wire Line
	13550 4800 13550 4850
Wire Wire Line
	13550 5150 13550 5200
Wire Wire Line
	12550 5150 12550 5200
Text GLabel 6600 1900 0    50   Input ~ 0
24V_DC_GND
Text GLabel 7800 1900 0    50   Input ~ 0
24V_DC_GND
Text GLabel 6600 2800 0    50   Input ~ 0
24V_DC_GND
Text GLabel 7800 2800 0    50   Input ~ 0
24V_DC_GND
Text GLabel 6600 2000 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 7800 2000 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 6600 2900 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 7800 2900 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 6600 2100 0    50   Input ~ 0
arduino_analog_gnd
Text GLabel 7800 2100 0    50   Input ~ 0
arduino_analog_gnd
Text GLabel 7800 3000 0    50   Input ~ 0
arduino_analog_gnd
Text GLabel 6600 3000 0    50   Input ~ 0
arduino_analog_gnd
Text GLabel 6600 2200 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 7800 2200 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 6600 3100 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 7800 3100 0    50   Input ~ 0
arduino_dig_gnd
Text GLabel 7800 2300 0    50   Input ~ 0
gauge_1_signal
Text GLabel 6600 3200 0    50   Input ~ 0
gauge_2_signal
Text GLabel 7800 3200 0    50   Input ~ 0
gauge_3_signal
Text GLabel 6600 2400 0    50   Input ~ 0
gauge_0_enable
Text GLabel 7800 2400 0    50   Input ~ 0
gauge_1_enable
Text GLabel 6600 3300 0    50   Input ~ 0
gauge_2_enable
Text GLabel 7800 3300 0    50   Input ~ 0
gauge_3_enable
Text GLabel 15400 2600 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 15400 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 15400 2300 0    50   Input ~ 0
24V_DC_GND
Text GLabel 15400 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 15400 2200 0    50   Input ~ 0
MFC_3_serial_TX
Text GLabel 15400 2100 0    50   Input ~ 0
MFC_3_serial_RX
Text GLabel 15400 1900 0    50   Input ~ 0
MFC_3_PWM_sig
Text GLabel 15400 2000 0    50   Input ~ 0
arduino_PWM_gnd
Text GLabel 15400 1700 0    50   Input ~ 0
MFC_3_analog_sig
Text GLabel 15400 1800 0    50   Input ~ 0
arduino_analog_gnd
$Comp
L Connector_Generic:Conn_01x10 conn_MFC_2
U 1 1 5E41017C
P 14400 2100
F 0 "conn_MFC_2" H 14200 2600 50  0000 L CNN
F 1 "Conn_01x10" H 14200 1500 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 14400 2100 50  0001 C CNN
F 3 "~" H 14400 2100 50  0001 C CNN
	1    14400 2100
	1    0    0    -1  
$EndComp
Text GLabel 14200 2600 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 14200 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 14200 2300 0    50   Input ~ 0
24V_DC_GND
Text GLabel 14200 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 14200 2200 0    50   Input ~ 0
MFC_2_serial_TX
Text GLabel 14200 2100 0    50   Input ~ 0
MFC_2_serial_RX
Text GLabel 14200 1900 0    50   Input ~ 0
MFC_2_PWM_sig
Text GLabel 14200 2000 0    50   Input ~ 0
arduino_PWM_gnd
Text GLabel 14200 1700 0    50   Input ~ 0
MFC_2_analog_sig
Text GLabel 14200 1800 0    50   Input ~ 0
arduino_analog_gnd
$Comp
L Connector_Generic:Conn_01x10 conn_MFC_1
U 1 1 5E41907F
P 13250 2100
F 0 "conn_MFC_1" H 13050 2600 50  0000 L CNN
F 1 "Conn_01x10" H 13050 1500 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 13250 2100 50  0001 C CNN
F 3 "~" H 13250 2100 50  0001 C CNN
	1    13250 2100
	1    0    0    -1  
$EndComp
Text GLabel 13050 2600 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 13050 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 13050 2300 0    50   Input ~ 0
24V_DC_GND
Text GLabel 13050 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 13050 2200 0    50   Input ~ 0
MFC_1_serial_TX
Text GLabel 13050 2100 0    50   Input ~ 0
MFC_1_serial_RX
Text GLabel 13050 1900 0    50   Input ~ 0
MFC_1_PWM_sig
Text GLabel 13050 2000 0    50   Input ~ 0
arduino_PWM_gnd
Text GLabel 13050 1700 0    50   Input ~ 0
MFC_1_analog_sig
Text GLabel 13050 1800 0    50   Input ~ 0
arduino_analog_gnd
$Comp
L Connector_Generic:Conn_01x10 conn_MFC_0
U 1 1 5E41A5C6
P 12050 2100
F 0 "conn_MFC_0" H 11850 2600 50  0000 L CNN
F 1 "Conn_01x10" H 11850 1500 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 12050 2100 50  0001 C CNN
F 3 "~" H 12050 2100 50  0001 C CNN
	1    12050 2100
	1    0    0    -1  
$EndComp
Text GLabel 11850 2600 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 11850 2500 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 11850 2300 0    50   Input ~ 0
24V_DC_GND
Text GLabel 11850 2400 0    50   Input ~ 0
24V_DC_GND
Text GLabel 11850 2200 0    50   Input ~ 0
MFC_0_serial_TX
Text GLabel 11850 2100 0    50   Input ~ 0
MFC_0_serial_RX
Text GLabel 11850 1900 0    50   Input ~ 0
MFC_0_PWM_sig
Text GLabel 11850 2000 0    50   Input ~ 0
arduino_PWM_gnd
Text GLabel 11850 1700 0    50   Input ~ 0
MFC_0_analog_sig
Text GLabel 11850 1800 0    50   Input ~ 0
arduino_analog_gnd
$Comp
L Mechanical:MountingHole_Pad H_DC_supplies_1
U 1 1 5E35CD52
P 10550 8400
F 0 "H_DC_supplies_1" H 10650 8449 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 8358 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 10550 8400 50  0001 C CNN
F 3 "~" H 10550 8400 50  0001 C CNN
	1    10550 8400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_DC_supplies_2
U 1 1 5E35E0CA
P 10550 8700
F 0 "H_DC_supplies_2" H 10650 8749 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 8658 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 10550 8700 50  0001 C CNN
F 3 "~" H 10550 8700 50  0001 C CNN
	1    10550 8700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J5
U 1 1 5E36000A
P 13150 8650
F 0 "J5" H 13068 8225 50  0000 C CNN
F 1 "Screw_Terminal_01x04" H 13068 8316 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-4_P5.08mm" H 13150 8650 50  0001 C CNN
F 3 "~" H 13150 8650 50  0001 C CNN
	1    13150 8650
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 5E362559
P 13150 9400
F 0 "J6" H 13068 9075 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 13068 9166 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 13150 9400 50  0001 C CNN
F 3 "~" H 13150 9400 50  0001 C CNN
	1    13150 9400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J1
U 1 1 5E367CC8
P 11950 8650
F 0 "J1" H 11868 8225 50  0000 C CNN
F 1 "Screw_Terminal_01x04" H 11868 8316 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-4_P5.08mm" H 11950 8650 50  0001 C CNN
F 3 "~" H 11950 8650 50  0001 C CNN
	1    11950 8650
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5E367CCE
P 11950 9400
F 0 "J2" H 11868 9075 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11868 9166 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 11950 9400 50  0001 C CNN
F 3 "~" H 11950 9400 50  0001 C CNN
	1    11950 9400
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_DC_supplies_4
U 1 1 5E35F54D
P 10550 9300
F 0 "H_DC_supplies_4" H 10650 9349 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 9258 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 10550 9300 50  0001 C CNN
F 3 "~" H 10550 9300 50  0001 C CNN
	1    10550 9300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H_DC_supplies_3
U 1 1 5E35EAE8
P 10550 9000
F 0 "H_DC_supplies_3" H 10650 9049 50  0000 L CNN
F 1 "MountingHole_Pad" H 10650 8958 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 10550 9000 50  0001 C CNN
F 3 "~" H 10550 9000 50  0001 C CNN
	1    10550 9000
	1    0    0    -1  
$EndComp
Text GLabel 13350 8450 2    50   Input ~ 0
24V_DC_HOT
Text GLabel 13350 8550 2    50   Input ~ 0
24V_DC_HOT
Text GLabel 13350 8650 2    50   Input ~ 0
24V_DC_GND
Text GLabel 13350 8750 2    50   Input ~ 0
24V_DC_GND
Text GLabel 12150 9300 2    50   Input ~ 0
240V_AC_line
Text GLabel 12150 9400 2    50   Input ~ 0
240V_AC_neutral
Text GLabel 13350 9300 2    50   Input ~ 0
240V_AC_line
Text GLabel 13350 9400 2    50   Input ~ 0
240V_AC_neutral
Text GLabel 12150 8450 2    50   Input ~ 0
5V_DC_HOT
Text GLabel 12150 8550 2    50   Input ~ 0
5V_DC_HOT
Text GLabel 12150 8650 2    50   Input ~ 0
5V_DC_GND
Text GLabel 12150 8750 2    50   Input ~ 0
5V_DC_GND
$Comp
L Connector:Screw_Terminal_01x03 J12
U 1 1 5E52214E
P 14300 8550
F 0 "J12" H 14218 8225 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 14218 8316 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 14300 8550 50  0001 C CNN
F 3 "~" H 14300 8550 50  0001 C CNN
	1    14300 8550
	-1   0    0    1   
$EndComp
Text GLabel 14500 8450 2    50   Input ~ 0
240V_AC_line
Text GLabel 14500 8550 2    50   Input ~ 0
240V_AC_neutral
Text GLabel 14500 8650 2    50   Input ~ 0
240V_AC_gnd
Text Notes 2350 9600 2    50   ~ 0
Arduino connections here\nprobably ribbon connectors for individual \nsections - analog ins, digital outs, pwm outs
Text GLabel 1250 9000 0    50   Input ~ 0
SCL_1
Text GLabel 1250 8900 0    50   Input ~ 0
SDA_1
Text GLabel 1250 8800 0    50   Input ~ 0
A_REF
Text GLabel 1250 8600 0    50   Input ~ 0
arduino_digital_13
Text GLabel 1250 8500 0    50   Input ~ 0
arduino_digital_12
Text GLabel 1250 8400 0    50   Input ~ 0
arduino_digital_11
Text GLabel 1250 8300 0    50   Input ~ 0
arduino_digital_10
Text GLabel 1250 8200 0    50   Input ~ 0
arduino_digital_09
Text GLabel 1250 8100 0    50   Input ~ 0
arduino_digital_08
Text GLabel 1250 7900 0    50   Input ~ 0
arduino_digital_07
Text GLabel 1250 8700 0    50   Input ~ 0
arduino_gnd
Text GLabel 1250 7800 0    50   Input ~ 0
arduino_digital_06
Text GLabel 1250 7700 0    50   Input ~ 0
arduino_digital_05
Text GLabel 1250 7600 0    50   Input ~ 0
arduino_digital_04
Text GLabel 1250 7500 0    50   Input ~ 0
arduino_digital_03
Text GLabel 1250 7400 0    50   Input ~ 0
arduino_digital_02
Text GLabel 1250 7300 0    50   Input ~ 0
arduino_digital_01
NoConn ~ 1250 7100
Text GLabel 1250 7000 0    50   Input ~ 0
arduino_digital_14
Text GLabel 1250 6900 0    50   Input ~ 0
arduino_digital_15
Text GLabel 1250 6800 0    50   Input ~ 0
arduino_digital_16
Text GLabel 1250 6600 0    50   Input ~ 0
arduino_digital_18
Text GLabel 1250 6700 0    50   Input ~ 0
arduino_digital_17
Text GLabel 1250 6500 0    50   Input ~ 0
arduino_digital_19
Text GLabel 1250 6400 0    50   Input ~ 0
arduino_digital_20
NoConn ~ 1250 8000
Text GLabel 1250 6300 0    50   Input ~ 0
arduino_digital_21
Text GLabel 1550 5350 3    50   Input ~ 0
arduino_5V
Text GLabel 1550 4850 1    50   Input ~ 0
arduino_5V
Text GLabel 1650 5350 3    50   Input ~ 0
arduino_digital_22
Text GLabel 1750 5350 3    50   Input ~ 0
arduino_digital_24
Text GLabel 1850 5350 3    50   Input ~ 0
arduino_digital_26
Text GLabel 1950 5350 3    50   Input ~ 0
arduino_digital_28
Text GLabel 2050 5350 3    50   Input ~ 0
arduino_digital_30
Text GLabel 2150 5350 3    50   Input ~ 0
arduino_digital_32
Text GLabel 2250 5350 3    50   Input ~ 0
arduino_digital_34
Text GLabel 2350 5350 3    50   Input ~ 0
arduino_digital_36
Text GLabel 2450 5350 3    50   Input ~ 0
arduino_digital_38
Text GLabel 2550 5350 3    50   Input ~ 0
arduino_digital_40
Text GLabel 2650 5350 3    50   Input ~ 0
arduino_digital_42
Text GLabel 2750 5350 3    50   Input ~ 0
arduino_digital_44
Text GLabel 2850 5350 3    50   Input ~ 0
arduino_digital_46
Text GLabel 2950 5350 3    50   Input ~ 0
arduino_digital_48
Text GLabel 3050 5350 3    50   Input ~ 0
arduino_digital_50
Text GLabel 3150 5350 3    50   Input ~ 0
arduino_digital_52
Text GLabel 3250 5350 3    50   Input ~ 0
arduino_gnd
Text GLabel 1650 4850 1    50   Input ~ 0
arduino_digital_23
Text GLabel 1750 4850 1    50   Input ~ 0
arduino_digital_25
Text GLabel 1850 4850 1    50   Input ~ 0
arduino_digital_27
Text GLabel 1950 4850 1    50   Input ~ 0
arduino_digital_29
Text GLabel 2050 4850 1    50   Input ~ 0
arduino_digital_31
Text GLabel 2150 4850 1    50   Input ~ 0
arduino_digital_33
Text GLabel 2250 4850 1    50   Input ~ 0
arduino_digital_35
Text GLabel 2350 4850 1    50   Input ~ 0
arduino_digital_37
Text GLabel 2450 4850 1    50   Input ~ 0
arduino_digital_39
Text GLabel 2550 4850 1    50   Input ~ 0
arduino_digital_41
Text GLabel 2650 4850 1    50   Input ~ 0
arduino_digital_43
Text GLabel 2750 4850 1    50   Input ~ 0
arduino_digital_45
Text GLabel 2850 4850 1    50   Input ~ 0
arduino_digital_47
Text GLabel 2950 4850 1    50   Input ~ 0
arduino_digital_49
Text GLabel 3050 4850 1    50   Input ~ 0
arduino_digital_51
Text GLabel 3150 4850 1    50   Input ~ 0
arduino_digital_53
Text GLabel 3250 4850 1    50   Input ~ 0
arduino_gnd
Text GLabel 3850 6450 0    50   Input ~ 0
arduino_analog_15
Text GLabel 3850 6550 0    50   Input ~ 0
arduino_analog_14
Text GLabel 3850 6650 0    50   Input ~ 0
arduino_analog_13
Text GLabel 3850 6750 0    50   Input ~ 0
arduino_analog_12
Text GLabel 3850 6850 0    50   Input ~ 0
arduino_analog_11
Text GLabel 3850 6950 0    50   Input ~ 0
arduino_analog_10
Text GLabel 3850 7050 0    50   Input ~ 0
arduino_analog_09
Text GLabel 3850 7150 0    50   Input ~ 0
arduino_analog_08
Text GLabel 3850 7350 0    50   Input ~ 0
arduino_analog_07
Text GLabel 3850 7450 0    50   Input ~ 0
arduino_analog_06
Text GLabel 3850 7550 0    50   Input ~ 0
arduino_analog_05
Text GLabel 3850 7650 0    50   Input ~ 0
arduino_analog_04
Text GLabel 3850 7750 0    50   Input ~ 0
arduino_analog_03
NoConn ~ 3850 7250
Text GLabel 3850 7850 0    50   Input ~ 0
arduino_analog_02
Text GLabel 3850 7950 0    50   Input ~ 0
arduino_analog_01
Text GLabel 3850 8050 0    50   Input ~ 0
arduino_analog_00
NoConn ~ 3850 8150
Text GLabel 3850 8250 0    50   Input ~ 0
V_in
Text GLabel 3850 8350 0    50   Input ~ 0
arduino_gnd
Text GLabel 3850 8450 0    50   Input ~ 0
arduino_gnd
Text GLabel 3850 8550 0    50   Input ~ 0
arduino_5V
Text GLabel 3850 8650 0    50   Input ~ 0
arduino_3V3
Text GLabel 3850 8750 0    50   Input ~ 0
arduino_reset
Text GLabel 3850 8850 0    50   Input ~ 0
arduino_IOREF
Text GLabel 3850 8950 0    50   Input ~ 0
arduino_unlabeled
$Comp
L Connector_Generic:Conn_01x26 J11
U 1 1 5E587489
P 4050 7650
F 0 "J11" H 4000 9000 50  0000 L CNN
F 1 "arduino_LHS" H 3800 6200 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x26_P2.54mm_Vertical" H 4050 7650 50  0001 C CNN
F 3 "~" H 4050 7650 50  0001 C CNN
	1    4050 7650
	1    0    0    -1  
$EndComp
Text GLabel 4550 2700 0    50   Input ~ 0
arduino_digital_35
Text GLabel 15600 3000 0    50   Input ~ 0
MFC_3_analog_sig
Text GLabel 15600 3300 0    50   Input ~ 0
MFC_3_PWM_sig
Text GLabel 15600 3100 0    50   Input ~ 0
arduino_analog_00
Text GLabel 15600 3400 0    50   Input ~ 0
arduino_digital_05
Text GLabel 14350 3000 0    50   Input ~ 0
MFC_2_analog_sig
Text GLabel 14350 3300 0    50   Input ~ 0
MFC_2_PWM_sig
Text GLabel 14350 3100 0    50   Input ~ 0
arduino_analog_01
Text GLabel 14350 3400 0    50   Input ~ 0
arduino_digital_04
Text GLabel 13200 3000 0    50   Input ~ 0
MFC_1_analog_sig
Text GLabel 13200 3300 0    50   Input ~ 0
MFC_1_PWM_sig
Text GLabel 13200 3100 0    50   Input ~ 0
arduino_analog_02
Text GLabel 13200 3400 0    50   Input ~ 0
arduino_digital_03
Text GLabel 12000 3000 0    50   Input ~ 0
MFC_0_analog_sig
Text GLabel 12000 3300 0    50   Input ~ 0
MFC_0_PWM_sig
Text GLabel 12000 3100 0    50   Input ~ 0
arduino_analog_03
Text GLabel 12000 3400 0    50   Input ~ 0
arduino_digital_02
Wire Wire Line
	12000 3000 12100 3000
Wire Wire Line
	12100 3000 12100 3100
Wire Wire Line
	12100 3100 12000 3100
Wire Wire Line
	12000 3300 12100 3300
Wire Wire Line
	12100 3300 12100 3400
Wire Wire Line
	12100 3400 12000 3400
Wire Wire Line
	13200 3000 13300 3000
Wire Wire Line
	13300 3000 13300 3100
Wire Wire Line
	13300 3100 13200 3100
Wire Wire Line
	13200 3300 13300 3300
Wire Wire Line
	13300 3300 13300 3400
Wire Wire Line
	13300 3400 13200 3400
Wire Wire Line
	14350 3000 14450 3000
Wire Wire Line
	14450 3000 14450 3100
Wire Wire Line
	14450 3100 14350 3100
Wire Wire Line
	14350 3300 14450 3300
Wire Wire Line
	14450 3300 14450 3400
Wire Wire Line
	14450 3400 14350 3400
Wire Wire Line
	15600 3000 15700 3000
Wire Wire Line
	15700 3000 15700 3100
Wire Wire Line
	15700 3100 15600 3100
Wire Wire Line
	15600 3300 15700 3300
Wire Wire Line
	15700 3300 15700 3400
Wire Wire Line
	15700 3400 15600 3400
Text GLabel 6600 2300 0    50   Input ~ 0
gauge_0_signal
Text GLabel 6600 3600 0    50   Input ~ 0
gauge_0_signal
Text GLabel 6600 3800 0    50   Input ~ 0
gauge_0_enable
Text GLabel 6600 4300 0    50   Input ~ 0
gauge_2_signal
Text GLabel 6600 4500 0    50   Input ~ 0
gauge_2_enable
Text GLabel 7800 3600 0    50   Input ~ 0
gauge_1_signal
Text GLabel 7800 3800 0    50   Input ~ 0
gauge_1_enable
Text GLabel 7800 4300 0    50   Input ~ 0
gauge_3_signal
Text GLabel 7800 4500 0    50   Input ~ 0
gauge_3_enable
Text GLabel 6600 3700 0    50   Input ~ 0
arduino_analog_07
Text GLabel 6600 3900 0    50   Input ~ 0
arduino_digital_28
Text GLabel 7800 3700 0    50   Input ~ 0
arduino_analog_06
Text GLabel 7800 3900 0    50   Input ~ 0
arduino_digital_26
Text GLabel 6600 4400 0    50   Input ~ 0
arduino_analog_05
Text GLabel 6600 4600 0    50   Input ~ 0
arduino_digital_24
Text GLabel 7800 4600 0    50   Input ~ 0
arduino_digital_22
Text GLabel 7800 4400 0    50   Input ~ 0
arduino_analog_04
Wire Wire Line
	6600 3600 6700 3600
Wire Wire Line
	6700 3600 6700 3700
Wire Wire Line
	6700 3700 6600 3700
Wire Wire Line
	6600 3800 6700 3800
Wire Wire Line
	6700 3800 6700 3900
Wire Wire Line
	6700 3900 6600 3900
Wire Wire Line
	7800 3600 7900 3600
Wire Wire Line
	7900 3600 7900 3700
Wire Wire Line
	7900 3700 7800 3700
Wire Wire Line
	7800 3800 7900 3800
Wire Wire Line
	7900 3800 7900 3900
Wire Wire Line
	7900 3900 7800 3900
Wire Wire Line
	7800 4300 7900 4300
Wire Wire Line
	7900 4300 7900 4400
Wire Wire Line
	7900 4400 7800 4400
Wire Wire Line
	7800 4500 7900 4500
Wire Wire Line
	7900 4500 7900 4600
Wire Wire Line
	7900 4600 7800 4600
Wire Wire Line
	6600 4300 6700 4300
Wire Wire Line
	6700 4300 6700 4400
Wire Wire Line
	6700 4400 6600 4400
Wire Wire Line
	6600 4500 6700 4500
Wire Wire Line
	6700 4500 6700 4600
Wire Wire Line
	6700 4600 6600 4600
Text GLabel 1850 6550 2    50   Input ~ 0
24V_DC_GND
Text GLabel 1850 6850 2    50   Input ~ 0
24V_DC_GND
Text GLabel 1850 7150 2    50   Input ~ 0
24V_DC_GND
Text GLabel 1850 7450 2    50   Input ~ 0
24V_DC_GND
Text GLabel 1850 7750 2    50   Input ~ 0
24V_DC_GND
Text GLabel 1850 8050 2    50   Input ~ 0
24V_DC_GND
Wire Wire Line
	1750 6500 1750 6550
Wire Wire Line
	1750 6550 1850 6550
Wire Wire Line
	1750 6800 1750 6850
Wire Wire Line
	1750 6850 1850 6850
Wire Wire Line
	1750 7100 1750 7150
Wire Wire Line
	1750 7150 1850 7150
Wire Wire Line
	1750 7400 1750 7450
Wire Wire Line
	1750 7450 1850 7450
Wire Wire Line
	1750 7700 1750 7750
Wire Wire Line
	1750 7750 1850 7750
Wire Wire Line
	1750 8000 1750 8050
Wire Wire Line
	1750 8050 1850 8050
NoConn ~ 14100 5450
NoConn ~ 14100 5550
NoConn ~ 14100 5650
NoConn ~ 14100 5750
NoConn ~ 14100 5850
NoConn ~ 14100 5950
NoConn ~ 14100 6150
NoConn ~ 14100 6050
NoConn ~ 14100 6250
NoConn ~ 14100 6350
NoConn ~ 13500 6350
NoConn ~ 13500 6250
NoConn ~ 13500 6150
NoConn ~ 13500 6050
NoConn ~ 13500 5950
NoConn ~ 13500 5850
NoConn ~ 13500 5750
NoConn ~ 13500 5650
NoConn ~ 13500 5550
NoConn ~ 13500 5450
NoConn ~ 12950 5450
NoConn ~ 12950 5550
NoConn ~ 12950 5650
NoConn ~ 12950 5750
NoConn ~ 12950 5850
NoConn ~ 12950 5950
NoConn ~ 12950 6050
NoConn ~ 12950 6150
NoConn ~ 12950 6250
NoConn ~ 12950 6350
NoConn ~ 12450 6350
NoConn ~ 12450 6250
NoConn ~ 12450 6150
NoConn ~ 12450 6050
NoConn ~ 12450 5950
NoConn ~ 12450 5850
NoConn ~ 12450 5750
NoConn ~ 12450 5650
NoConn ~ 12450 5550
NoConn ~ 12450 5450
Text GLabel 10700 8550 2    50   Input ~ 0
24V_DC_GND
Text GLabel 10700 8850 2    50   Input ~ 0
24V_DC_GND
Text GLabel 10700 9150 2    50   Input ~ 0
24V_DC_GND
Text GLabel 10700 9450 2    50   Input ~ 0
24V_DC_GND
Wire Wire Line
	10550 8500 10550 8550
Wire Wire Line
	10550 8550 10700 8550
Wire Wire Line
	10550 8800 10550 8850
Wire Wire Line
	10550 8850 10700 8850
Wire Wire Line
	10550 9100 10550 9150
Wire Wire Line
	10550 9150 10700 9150
Wire Wire Line
	10550 9400 10550 9450
Wire Wire Line
	10550 9450 10700 9450
Text GLabel 9800 10100 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 10700 10100 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 10700 10400 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 10700 10700 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 10700 11000 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 9800 11000 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 9800 10700 2    50   Input ~ 0
240V_AC_gnd
Text GLabel 9800 10400 2    50   Input ~ 0
240V_AC_gnd
Wire Wire Line
	9650 10050 9650 10100
Wire Wire Line
	9650 10100 9800 10100
Wire Wire Line
	10550 10050 10550 10100
Wire Wire Line
	10550 10100 10700 10100
Wire Wire Line
	10550 10350 10550 10400
Wire Wire Line
	10550 10400 10700 10400
Wire Wire Line
	10550 10650 10550 10700
Wire Wire Line
	10550 10700 10700 10700
Wire Wire Line
	10550 10950 10550 11000
Wire Wire Line
	10550 11000 10700 11000
Wire Wire Line
	9650 10350 9650 10400
Wire Wire Line
	9650 10400 9800 10400
Wire Wire Line
	9650 10650 9650 10700
Wire Wire Line
	9650 10700 9800 10700
Wire Wire Line
	9650 10950 9650 11000
Wire Wire Line
	9650 11000 9800 11000
$Comp
L Connector:USB_A J13
U 1 1 5E3B58D6
P 14550 9300
F 0 "J13" H 14607 9767 50  0000 C CNN
F 1 "USB_A" H 14607 9676 50  0000 C CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 14700 9250 50  0001 C CNN
F 3 " ~" H 14700 9250 50  0001 C CNN
	1    14550 9300
	1    0    0    -1  
$EndComp
Text GLabel 14850 9100 2    50   Input ~ 0
5V_DC_HOT
Text GLabel 14550 9700 2    50   Input ~ 0
5V_DC_GND
NoConn ~ 14850 9300
NoConn ~ 14850 9400
Text Notes 14950 9500 0    50   ~ 0
Power to Raspberry Pi\n5V DC at a bunch of amps\nUSB A connector, goes to\nUSB C on R-Pi
$Comp
L Connector_Generic:Conn_01x28 J9
U 1 1 5E3E161F
P 1450 7600
F 0 "J9" H 1400 9050 50  0000 L CNN
F 1 "arduino_RHS" H 1250 6050 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x28_P2.54mm_Vertical" H 1450 7600 50  0001 C CNN
F 3 "~" H 1450 7600 50  0001 C CNN
	1    1450 7600
	1    0    0    -1  
$EndComp
Text GLabel 1250 7200 0    50   Input ~ 0
arduino_digital_00
Text GLabel 2650 10250 3    50   Input ~ 0
SCL_1
Text GLabel 2550 10250 3    50   Input ~ 0
SDA_1
Text GLabel 4700 6550 0    50   Input ~ 0
A_REF
Text GLabel 2350 10250 3    50   Input ~ 0
arduino_digital_13
Text GLabel 2250 10250 3    50   Input ~ 0
arduino_digital_12
Text GLabel 2150 10250 3    50   Input ~ 0
arduino_digital_11
Text GLabel 2050 10250 3    50   Input ~ 0
arduino_digital_10
Text GLabel 1850 10250 3    50   Input ~ 0
arduino_digital_01
Text GLabel 1550 10250 3    50   Input ~ 0
arduino_digital_14
Text GLabel 1450 10250 3    50   Input ~ 0
arduino_digital_15
Text GLabel 1350 10250 3    50   Input ~ 0
arduino_digital_16
Text GLabel 1150 10250 3    50   Input ~ 0
arduino_digital_18
Text GLabel 1250 10250 3    50   Input ~ 0
arduino_digital_17
Text GLabel 1050 10250 3    50   Input ~ 0
arduino_digital_19
Text GLabel 950  10250 3    50   Input ~ 0
arduino_digital_20
Text GLabel 850  10250 3    50   Input ~ 0
arduino_digital_21
Text GLabel 1750 10250 3    50   Input ~ 0
arduino_digital_00
NoConn ~ 1750 10250
NoConn ~ 1850 10250
NoConn ~ 2050 10250
NoConn ~ 2150 10250
NoConn ~ 2250 10250
NoConn ~ 2350 10250
NoConn ~ 2550 10250
NoConn ~ 2650 10250
NoConn ~ 850  10250
NoConn ~ 950  10250
NoConn ~ 1050 10250
NoConn ~ 1150 10250
NoConn ~ 1250 10250
NoConn ~ 1350 10250
NoConn ~ 1450 10250
NoConn ~ 1550 10250
Text GLabel 2850 10250 3    50   Input ~ 0
arduino_analog_15
Text GLabel 2950 10250 3    50   Input ~ 0
arduino_analog_14
Text GLabel 3050 10250 3    50   Input ~ 0
arduino_analog_13
Text GLabel 3150 10250 3    50   Input ~ 0
arduino_analog_12
Text GLabel 3350 10250 3    50   Input ~ 0
V_in
Text GLabel 3550 10250 3    50   Input ~ 0
arduino_3V3
Text GLabel 3650 10250 3    50   Input ~ 0
arduino_reset
Text GLabel 3750 10250 3    50   Input ~ 0
arduino_IOREF
Text GLabel 3850 10250 3    50   Input ~ 0
arduino_unlabeled
NoConn ~ 2850 10250
NoConn ~ 2950 10250
NoConn ~ 3050 10250
NoConn ~ 3150 10250
NoConn ~ 3850 10250
NoConn ~ 3750 10250
NoConn ~ 3650 10250
NoConn ~ 3550 10250
NoConn ~ 3350 10250
Text Notes 4250 7050 0    50   ~ 0
Analog voltages referenced against onboard 5V\nOnly need to connect this to a reference voltage \nshould the analog voltages need to be referenced\nagainst anything that isnt 5V
Text GLabel 15600 3700 0    50   Input ~ 0
MFC_3_serial_TX
Text GLabel 15600 3600 0    50   Input ~ 0
MFC_3_serial_RX
Text GLabel 14350 3700 0    50   Input ~ 0
MFC_2_serial_TX
Text GLabel 14350 3600 0    50   Input ~ 0
MFC_2_serial_RX
Text GLabel 13200 3700 0    50   Input ~ 0
MFC_1_serial_TX
Text GLabel 13200 3600 0    50   Input ~ 0
MFC_1_serial_RX
Text GLabel 12000 3700 0    50   Input ~ 0
MFC_0_serial_TX
Text GLabel 12000 3600 0    50   Input ~ 0
MFC_0_serial_RX
NoConn ~ 12000 3600
NoConn ~ 12000 3700
NoConn ~ 13200 3600
NoConn ~ 13200 3700
NoConn ~ 14350 3600
NoConn ~ 14350 3700
NoConn ~ 15600 3600
NoConn ~ 15600 3700
Text GLabel 9600 8500 0    50   Input ~ 0
24V_DC_GND
Text GLabel 9600 8600 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 9600 8750 0    50   Input ~ 0
5V_DC_GND
Text GLabel 9600 8850 0    50   Input ~ 0
5V_DC_HOT
Text GLabel 9600 9000 0    50   Input ~ 0
240V_AC_line
Text GLabel 9600 9100 0    50   Input ~ 0
240V_AC_neutral
Text GLabel 9600 9200 0    50   Input ~ 0
240V_AC_gnd
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E4DA11E
P 9600 8600
F 0 "#FLG0101" H 9600 8675 50  0001 C CNN
F 1 "PWR_FLAG" V 9600 8728 50  0000 L CNN
F 2 "" H 9600 8600 50  0001 C CNN
F 3 "~" H 9600 8600 50  0001 C CNN
	1    9600 8600
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E4DAED2
P 9600 8850
F 0 "#FLG0102" H 9600 8925 50  0001 C CNN
F 1 "PWR_FLAG" V 9600 8978 50  0000 L CNN
F 2 "" H 9600 8850 50  0001 C CNN
F 3 "~" H 9600 8850 50  0001 C CNN
	1    9600 8850
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E4DBFAE
P 9600 9000
F 0 "#FLG0103" H 9600 9075 50  0001 C CNN
F 1 "PWR_FLAG" V 9600 9128 50  0000 L CNN
F 2 "" H 9600 9000 50  0001 C CNN
F 3 "~" H 9600 9000 50  0001 C CNN
	1    9600 9000
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5E4DCE71
P 9600 9100
F 0 "#FLG0104" H 9600 9175 50  0001 C CNN
F 1 "PWR_FLAG" V 9600 9228 50  0000 L CNN
F 2 "" H 9600 9100 50  0001 C CNN
F 3 "~" H 9600 9100 50  0001 C CNN
	1    9600 9100
	0    1    1    0   
$EndComp
$Comp
L power:GNDPWR #PWR0103
U 1 1 5E4E2C6B
P 9600 9200
F 0 "#PWR0103" H 9600 9000 50  0001 C CNN
F 1 "GNDPWR" V 9605 9092 50  0000 R CNN
F 2 "" H 9600 9150 50  0001 C CNN
F 3 "" H 9600 9150 50  0001 C CNN
	1    9600 9200
	0    -1   -1   0   
$EndComp
Text GLabel 5250 10250 3    50   Input ~ 0
arduino_analog_11
Text GLabel 5350 10250 3    50   Input ~ 0
arduino_analog_10
Text GLabel 5450 10250 3    50   Input ~ 0
arduino_analog_09
Text GLabel 5550 10250 3    50   Input ~ 0
arduino_analog_08
NoConn ~ 5250 10250
NoConn ~ 5350 10250
NoConn ~ 5450 10250
NoConn ~ 5550 10250
Text GLabel 6100 10250 3    50   Input ~ 0
arduino_digital_09
Text GLabel 6000 10250 3    50   Input ~ 0
arduino_digital_08
Text GLabel 5900 10250 3    50   Input ~ 0
arduino_digital_07
Text GLabel 5800 10250 3    50   Input ~ 0
arduino_digital_06
NoConn ~ 5800 10250
NoConn ~ 5900 10250
NoConn ~ 6000 10250
NoConn ~ 6100 10250
Text Notes 5750 10200 0    50   ~ 0
reserved for\nTemperature\nControl
NoConn ~ 4700 6550
$Comp
L power:GNDREF #PWR0101
U 1 1 5E50CF79
P 9600 8500
F 0 "#PWR0101" H 9600 8250 50  0001 C CNN
F 1 "GNDREF" V 9605 8372 50  0000 R CNN
F 2 "" H 9600 8500 50  0001 C CNN
F 3 "" H 9600 8500 50  0001 C CNN
	1    9600 8500
	0    -1   -1   0   
$EndComp
Text GLabel 4100 10250 3    50   Input ~ 0
arduino_digital_32
Text GLabel 4200 10250 3    50   Input ~ 0
arduino_digital_34
Text GLabel 4300 10250 3    50   Input ~ 0
arduino_digital_36
Text GLabel 4400 10250 3    50   Input ~ 0
arduino_digital_38
Text GLabel 4500 10250 3    50   Input ~ 0
arduino_digital_40
Text GLabel 4600 10250 3    50   Input ~ 0
arduino_digital_42
Text GLabel 4700 10250 3    50   Input ~ 0
arduino_digital_44
Text GLabel 4800 10250 3    50   Input ~ 0
arduino_digital_46
Text GLabel 4900 10250 3    50   Input ~ 0
arduino_digital_48
Text GLabel 5000 10250 3    50   Input ~ 0
arduino_digital_50
Text GLabel 5100 10250 3    50   Input ~ 0
arduino_digital_52
NoConn ~ 4100 10250
NoConn ~ 4200 10250
NoConn ~ 4300 10250
NoConn ~ 4400 10250
NoConn ~ 4500 10250
NoConn ~ 4600 10250
NoConn ~ 4700 10250
NoConn ~ 4800 10250
NoConn ~ 4900 10250
NoConn ~ 5000 10250
NoConn ~ 5100 10250
Text GLabel 4000 10250 3    50   Input ~ 0
arduino_digital_30
NoConn ~ 4000 10250
$Comp
L Connector_Generic:Conn_02x18_Odd_Even J10
U 1 1 5E5CADB0
P 2350 5050
F 0 "J10" V 2354 5929 50  0000 L CNN
F 1 "arduino_bottom" V 2445 5929 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x18_P2.54mm_Vertical" H 2350 5050 50  0001 C CNN
F 3 "~" H 2350 5050 50  0001 C CNN
	1    2350 5050
	0    -1   1    0   
$EndComp
$Comp
L power:GNDREF #PWR0102
U 1 1 5E6113F7
P 9600 8750
F 0 "#PWR0102" H 9600 8500 50  0001 C CNN
F 1 "GNDREF" V 9605 8622 50  0000 R CNN
F 2 "" H 9600 8750 50  0001 C CNN
F 3 "" H 9600 8750 50  0001 C CNN
	1    9600 8750
	0    -1   -1   0   
$EndComp
Text GLabel 9600 8300 0    50   Input ~ 0
arduino_gnd
$Comp
L power:GNDREF #PWR0104
U 1 1 5E43C9EE
P 9600 8300
F 0 "#PWR0104" H 9600 8050 50  0001 C CNN
F 1 "GNDREF" V 9605 8172 50  0000 R CNN
F 2 "" H 9600 8300 50  0001 C CNN
F 3 "" H 9600 8300 50  0001 C CNN
	1    9600 8300
	0    -1   -1   0   
$EndComp
$Comp
L Connector:USB_A J14
U 1 1 5E4473E0
P 13700 7500
F 0 "J14" H 13757 7967 50  0000 C CNN
F 1 "USB_A" H 13757 7876 50  0000 C CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 13850 7450 50  0001 C CNN
F 3 " ~" H 13850 7450 50  0001 C CNN
	1    13700 7500
	1    0    0    -1  
$EndComp
Text GLabel 14000 7300 2    50   Input ~ 0
5V_DC_HOT
Text GLabel 13700 7900 2    50   Input ~ 0
5V_DC_GND
NoConn ~ 14000 7500
NoConn ~ 14000 7600
Text GLabel 13700 8000 2    50   Input ~ 0
5V_DC_GND
Wire Wire Line
	13600 7900 13600 8000
Wire Wire Line
	13600 8000 13700 8000
Text GLabel 14550 9800 2    50   Input ~ 0
5V_DC_GND
Wire Wire Line
	14450 9700 14450 9800
Wire Wire Line
	14450 9800 14550 9800
Text Notes 14550 7650 0    50   ~ 0
Power to Arduino in case RPi fails\n5V DC, USB A connector goes to\nbarrel jack connector? USB B?\nAllows use of ethernet for comms\nto arduino from RPi
$EndSCHEMATC
