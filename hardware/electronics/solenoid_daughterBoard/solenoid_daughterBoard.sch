EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 conn_stab_1
U 1 1 5E33205B
P 1500 1000
F 0 "conn_stab_1" H 1580 992 50  0000 L CNN
F 1 "Conn_01x02" H 1580 901 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1500 1000 50  0001 C CNN
F 3 "~" H 1500 1000 50  0001 C CNN
	1    1500 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 conn_stab_2
U 1 1 5E3326EA
P 1500 1350
F 0 "conn_stab_2" H 1580 1342 50  0000 L CNN
F 1 "Conn_01x02" H 1580 1251 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1500 1350 50  0001 C CNN
F 3 "~" H 1500 1350 50  0001 C CNN
	1    1500 1350
	1    0    0    -1  
$EndComp
$Comp
L myParts:ASSR-1611-100E U1
U 1 1 5E334FCC
P 3550 2800
F 0 "U1" V 3596 2620 50  0000 R CNN
F 1 "ASSR-1611-100E" V 3505 2620 50  0000 R CNN
F 2 "myParts:ASSR-1611-100E" H 3350 2600 50  0001 L CIN
F 3 "" H 3500 2850 50  0001 L CNN
	1    3550 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5E339D80
P 4850 2350
F 0 "R2" V 4643 2350 50  0000 C CNN
F 1 "2KR" V 4734 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 4780 2350 50  0001 C CNN
F 3 "~" H 4850 2350 50  0001 C CNN
	1    4850 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5E33AA2D
P 4850 2950
F 0 "R3" V 4643 2950 50  0000 C CNN
F 1 "100R" V 4734 2950 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 4780 2950 50  0001 C CNN
F 3 "~" H 4850 2950 50  0001 C CNN
	1    4850 2950
	0    1    1    0   
$EndComp
$Comp
L Device:D D1
U 1 1 5E33BC4E
P 5300 2950
F 0 "D1" H 5300 3166 50  0000 C CNN
F 1 "D" H 5300 3075 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P7.62mm_Horizontal" H 5300 2950 50  0001 C CNN
F 3 "~" H 5300 2950 50  0001 C CNN
	1    5300 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 conn_LED_1
U 1 1 5E33C3C2
P 5150 2150
F 0 "conn_LED_1" V 5114 1962 50  0000 R CNN
F 1 "Conn_01x02" V 5023 1962 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 5150 2150 50  0001 C CNN
F 3 "~" H 5150 2150 50  0001 C CNN
	1    5150 2150
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 conn_sol_1
U 1 1 5E346D6E
P 5150 1600
F 0 "conn_sol_1" V 5114 1412 50  0000 R CNN
F 1 "Conn_01x02" V 5023 1412 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 5150 1600 50  0001 C CNN
F 3 "~" H 5150 1600 50  0001 C CNN
	1    5150 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3550 2500 3550 2350
Wire Wire Line
	3550 2350 4700 2350
Wire Wire Line
	4700 2350 4700 1800
Wire Wire Line
	4700 1800 5150 1800
Connection ~ 4700 2350
Wire Wire Line
	5250 1800 5650 1800
Wire Wire Line
	5650 1800 5650 2350
Wire Wire Line
	5650 2350 5250 2350
Wire Wire Line
	5150 2350 5000 2350
Wire Wire Line
	4700 2350 4700 2950
Wire Wire Line
	5000 2950 5150 2950
Wire Wire Line
	5650 2350 5650 2950
Wire Wire Line
	5650 2950 5450 2950
Connection ~ 5650 2350
Wire Wire Line
	5650 2950 5650 3200
Connection ~ 5650 2950
Wire Wire Line
	1450 2700 1450 2500
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5E332FD0
P 1450 2900
F 0 "J1" V 1322 3080 50  0000 L CNN
F 1 "Conn_01x04" V 1600 2600 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04_Pitch2.54mm" H 1450 2900 50  0001 C CNN
F 3 "~" H 1450 2900 50  0001 C CNN
	1    1450 2900
	0    1    1    0   
$EndComp
Text GLabel 3450 3100 3    50   Input ~ 0
digital_signal
Text GLabel 3850 3400 2    50   Input ~ 0
digital_ground
Text GLabel 1250 2700 1    50   Input ~ 0
digital_signal
Text GLabel 1350 2700 1    50   Input ~ 0
digital_ground
Text GLabel 1950 2500 2    50   Input ~ 0
24V_DC_HOT
Text GLabel 1950 2700 2    50   Input ~ 0
24V_DC_GND
Text GLabel 5650 3200 3    50   Input ~ 0
24V_DC_GND
Text GLabel 3450 2500 1    50   Input ~ 0
24V_DC_HOT
NoConn ~ 3650 2500
Text GLabel 1300 1000 0    50   Input ~ 0
24V_DC_GND
Text GLabel 1300 1100 0    50   Input ~ 0
24V_DC_GND
Text GLabel 1300 1350 0    50   Input ~ 0
24V_DC_GND
Text GLabel 1300 1450 0    50   Input ~ 0
24V_DC_GND
$Comp
L Device:R R1
U 1 1 5E34E71C
P 3700 3400
F 0 "R1" V 3493 3400 50  0000 C CNN
F 1 "470R" V 3584 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 3630 3400 50  0001 C CNN
F 3 "~" H 3700 3400 50  0001 C CNN
	1    3700 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 3100 3550 3400
Wire Wire Line
	1550 2700 1950 2700
Wire Wire Line
	1450 2500 1950 2500
$EndSCHEMATC
