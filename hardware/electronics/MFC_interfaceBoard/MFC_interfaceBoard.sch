EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8500 1800 0    60   ~ 0
Connections to base board:\n•1x 1x6 header - analog signal, analog gnd\n	pwm signal, pwm gnd, serial TX &RX\n•1x 1x4 header - 24V Hot and GND - 2 pins \n	each\nConnection to DSUB/DIN for MFC\n•1x 2x5 header for 9 pin DSUB or 8 pin DIN\nConnection to other MFC boards\n•2x 1x2 header for stablilising on other boards\n	connected to power ground maybe
Text Notes 8500 2650 0    60   ~ 0
Alicat MFCs already have 0-5V output and can\ntake 0-5V analog set point input, so no amplification\nor reduction required. \nOrder MFC with locking 6 pin DIN connector\nSerial TX and RX hooked up to base board but currently\nunused. Will hopefully be used in future to provide much\nmore information from MFCs
$Comp
L Device:C C1
U 1 1 5E2B1E75
P 1850 1250
F 0 "C1" H 1875 1350 50  0000 L CNN
F 1 "C" H 1875 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_2220" H 1888 1100 50  0001 C CNN
F 3 "" H 1850 1250 50  0001 C CNN
	1    1850 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5E2B1EE0
P 1450 1100
F 0 "R1" V 1530 1100 50  0000 C CNN
F 1 "R" V 1450 1100 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 1380 1100 50  0001 C CNN
F 3 "" H 1450 1100 50  0001 C CNN
	1    1450 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 1100 1850 1100
Connection ~ 1850 1100
Text GLabel 1850 1400 3    50   Input ~ 0
pwm_gnd
Text GLabel 1300 1100 0    50   Input ~ 0
pwm_signal
Text GLabel 3850 1100 2    50   Input ~ 0
mfc_analog_setPoint
Text GLabel 2100 5400 3    50   Input ~ 0
serial_TX
Text GLabel 2200 5400 3    50   Input ~ 0
serial_RX
Text GLabel 1900 5400 3    50   Input ~ 0
pwm_signal
Text GLabel 2000 5400 3    50   Input ~ 0
pwm_gnd
Text GLabel 1700 5400 3    50   Input ~ 0
analog_signal
Text GLabel 1800 5400 3    50   Input ~ 0
analog_gnd
Text GLabel 2500 5400 3    50   Input ~ 0
24V_DC_HOT
Text GLabel 2600 5400 3    50   Input ~ 0
24V_DC_HOT
Text GLabel 2300 5400 3    50   Input ~ 0
24V_DC_GND
Text GLabel 2400 5400 3    50   Input ~ 0
24V_DC_GND
$Comp
L Connector:RJ45 J2
U 1 1 5E3718D8
P 10500 4750
F 0 "J2" H 10170 4754 50  0000 R CNN
F 1 "RJ45" H 10170 4845 50  0000 R CNN
F 2 "Connectors:RJ45_8" V 10500 4775 50  0001 C CNN
F 3 "~" V 10500 4775 50  0001 C CNN
	1    10500 4750
	-1   0    0    1   
$EndComp
Text Notes 8900 5600 0    50   ~ 0
Cable wired to DIN plug on back. RJ45 is a convenient\nmezzanine connector. Allows for easy removal and \nassembly of boards if/when necessary.
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5E373D6B
P 9400 5900
F 0 "H1" H 9500 5949 50  0000 L CNN
F 1 "MountingHole_Pad" H 9500 5858 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 9400 5900 50  0001 C CNN
F 3 "~" H 9400 5900 50  0001 C CNN
	1    9400 5900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5E374233
P 9400 6250
F 0 "H2" H 9500 6299 50  0000 L CNN
F 1 "MountingHole_Pad" H 9500 6208 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 9400 6250 50  0001 C CNN
F 3 "~" H 9400 6250 50  0001 C CNN
	1    9400 6250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5E374747
P 10300 5900
F 0 "H3" H 10400 5949 50  0000 L CNN
F 1 "MountingHole_Pad" H 10400 5858 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 10300 5900 50  0001 C CNN
F 3 "~" H 10300 5900 50  0001 C CNN
	1    10300 5900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5E374A1C
P 10300 6300
F 0 "H4" H 10400 6349 50  0000 L CNN
F 1 "MountingHole_Pad" H 10400 6258 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad_Via" H 10300 6300 50  0001 C CNN
F 3 "~" H 10300 6300 50  0001 C CNN
	1    10300 6300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 5E375346
P 2100 5200
F 0 "J1" V 2317 5146 50  0000 C CNN
F 1 "Conn_01x10" V 2226 5146 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x10_Pitch2.54mm" H 2100 5200 50  0001 C CNN
F 3 "~" H 2100 5200 50  0001 C CNN
	1    2100 5200
	0    -1   -1   0   
$EndComp
Text Notes 1550 6400 0    50   ~ 0
Single ribbon cable to base board. \nNested so that furthest inside goes \nto board 0, next one to board 1 etc.
Text Notes 5900 1250 0    50   ~ 0
Switching mechanism to allow 0-10V or 0-5V range\nUse DIP switches. Label on board if possible or at \nleast in documentation which config of switches means\nwhat voltage range. Should allow for all different kinds \nof MFCs
$Comp
L Device:R R3
U 1 1 5E37738E
P 9700 3150
F 0 "R3" V 9600 3150 50  0000 C CNN
F 1 "1KR" V 9700 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 9630 3150 50  0001 C CNN
F 3 "~" H 9700 3150 50  0001 C CNN
	1    9700 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5E37876B
P 9200 3150
F 0 "R2" V 9100 3150 50  0000 C CNN
F 1 "1KR" V 9200 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 9130 3150 50  0001 C CNN
F 3 "~" H 9200 3150 50  0001 C CNN
	1    9200 3150
	0    1    1    0   
$EndComp
Text GLabel 9050 3150 0    50   Input ~ 0
analog_gnd
Wire Wire Line
	9850 3150 9950 3150
$Comp
L Switch:SW_DIP_x02 SW1
U 1 1 5E379693
P 9150 3800
F 0 "SW1" H 9150 4167 50  0000 C CNN
F 1 "SW_DIP_x02" H 9150 4076 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x2_W7.62mm_Slide" H 9150 3800 50  0001 C CNN
F 3 "~" H 9150 3800 50  0001 C CNN
	1    9150 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3150 9450 3150
Wire Wire Line
	9450 3150 9450 3700
Connection ~ 9450 3150
Wire Wire Line
	9450 3150 9550 3150
Wire Wire Line
	9950 3150 9950 3800
Wire Wire Line
	9950 3800 9450 3800
Connection ~ 9950 3150
Wire Wire Line
	9950 3150 10100 3150
Wire Wire Line
	8850 3700 8850 3800
Connection ~ 8850 3800
Wire Wire Line
	8850 3800 8850 4050
Text GLabel 8850 4050 0    50   Input ~ 0
analog_signal
Text GLabel 10100 4450 0    50   Input ~ 0
24V_DC_HOT
Text GLabel 10100 4550 0    50   Input ~ 0
serial_TX
Text GLabel 10100 4650 0    50   Input ~ 0
serial_RX
Text GLabel 10100 4750 0    50   Input ~ 0
mfc_analog_setPoint
Wire Wire Line
	10100 4850 9250 4850
Text GLabel 9250 4800 0    50   Input ~ 0
pwm_gnd
Text GLabel 9250 4900 0    50   Input ~ 0
24V_DC_GND
Text GLabel 9250 5000 0    50   Input ~ 0
analog_gnd
Wire Wire Line
	9250 4800 9250 4850
Connection ~ 9250 4850
Wire Wire Line
	9250 4850 9250 5000
Text GLabel 10100 4950 0    50   Input ~ 0
mfc_analog_signal
Text GLabel 10100 3150 2    50   Input ~ 0
mfc_analog_signal
NoConn ~ 10100 5050
NoConn ~ 10100 5150
Text GLabel 9400 6000 2    50   Input ~ 0
24V_DC_GND
Text GLabel 9400 6350 2    50   Input ~ 0
24V_DC_GND
Text GLabel 10300 6000 2    50   Input ~ 0
24V_DC_GND
Text GLabel 10300 6400 2    50   Input ~ 0
24V_DC_GND
$Comp
L Switch:SW_DIP_x02 SW2
U 1 1 5E39289C
P 3550 1200
F 0 "SW2" H 3550 1567 50  0000 C CNN
F 1 "SW_DIP_x02" H 3550 1476 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x2_W7.62mm_Slide" H 3550 1200 50  0001 C CNN
F 3 "~" H 3550 1200 50  0001 C CNN
	1    3550 1200
	1    0    0    -1  
$EndComp
Text GLabel 3850 1200 2    50   Input ~ 0
mfc_analog_setPoint
Wire Wire Line
	1850 1100 2050 1100
Wire Wire Line
	2050 1100 2050 2050
Wire Wire Line
	2050 2050 2350 2050
Connection ~ 2050 1100
Wire Wire Line
	2050 1100 3250 1100
Wire Wire Line
	3250 1200 3250 2150
Wire Wire Line
	3250 2150 2950 2150
$Comp
L Device:R R4
U 1 1 5E3994F3
P 1700 2250
F 0 "R4" V 1600 2250 50  0000 C CNN
F 1 "10KR" V 1700 2250 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 1630 2250 50  0001 C CNN
F 3 "~" H 1700 2250 50  0001 C CNN
	1    1700 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	1550 2250 1550 2650
Text GLabel 2550 1850 2    50   Input ~ 0
24V_DC_HOT
$Comp
L Device:R R5
U 1 1 5E39BA5C
P 3050 2750
F 0 "R5" V 3150 2750 50  0000 C CNN
F 1 "10KR" V 3050 2750 50  0000 C CNN
F 2 "Resistors_SMD:R_2512" V 2980 2750 50  0001 C CNN
F 3 "~" H 3050 2750 50  0001 C CNN
	1    3050 2750
	0    -1   -1   0   
$EndComp
Text GLabel 2550 2450 2    50   Input ~ 0
24V_DC_GND
Wire Wire Line
	1850 2250 2050 2250
Wire Wire Line
	3250 2150 3250 2750
Wire Wire Line
	3250 2750 3200 2750
Connection ~ 3250 2150
Wire Wire Line
	2050 2250 2050 2750
Wire Wire Line
	2050 2750 2900 2750
Connection ~ 2050 2250
Wire Wire Line
	2050 2250 2350 2250
Text GLabel 1550 2650 0    50   Input ~ 0
pwm_gnd
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E3A1B7B
P 3950 5250
F 0 "#FLG0101" H 3950 5325 50  0001 C CNN
F 1 "PWR_FLAG" H 3950 5423 50  0000 C CNN
F 2 "" H 3950 5250 50  0001 C CNN
F 3 "~" H 3950 5250 50  0001 C CNN
	1    3950 5250
	1    0    0    -1  
$EndComp
Text GLabel 3950 5250 3    50   Input ~ 0
24V_DC_HOT
Text GLabel 4400 5250 3    50   Input ~ 0
24V_DC_GND
$Comp
L power:GND #PWR0101
U 1 1 5E3A3BF3
P 4400 5250
F 0 "#PWR0101" H 4400 5000 50  0001 C CNN
F 1 "GND" H 4405 5077 50  0000 C CNN
F 2 "" H 4400 5250 50  0001 C CNN
F 3 "" H 4400 5250 50  0001 C CNN
	1    4400 5250
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:AD8603 U1
U 1 1 5E3AB14D
P 2650 2150
F 0 "U1" H 2994 2196 50  0000 L CNN
F 1 "AD8603" H 2994 2105 50  0000 L CNN
F 2 "myParts:AD_8603_op_amp" H 2650 2150 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8603_8607_8609.pdf" H 2650 2350 50  0001 C CNN
	1    2650 2150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
