EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MFC_interfaceBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8500 1800 0    60   ~ 0
Connections to base board:\n•1x 1x6 header - analog signal, analog gnd\n	pwm signal, pwm gnd, serial TX &RX\n•1x 1x4 header - 24V Hot and GND - 2 pins \n	each\nConnection to DSUB/DIN for MFC\n•1x 2x5 header for 9 pin DSUB or 8 pin DIN\nConnection to other MFC boards\n•2x 1x2 header for stablilising on other boards\n	connected to power ground maybe
Text Notes 8500 2650 0    60   ~ 0
Alicat MFCs already have 0-5V output and can\ntake 0-5V analog set point input, so no amplification\nor reduction required. \nSerial TX and RX hooked up to base board but currently\nunused. Will hopefully be used in future to provide much\nmore information from MFCs
$Comp
L Conn_01x06 J?
U 1 1 5E2B1D5C
P 1900 5200
F 0 "J?" H 1900 5500 50  0000 C CNN
F 1 "Conn_01x06" H 1900 4800 50  0000 C CNN
F 2 "" H 1900 5200 50  0001 C CNN
F 3 "" H 1900 5200 50  0001 C CNN
	1    1900 5200
	0    -1   -1   0   
$EndComp
$Comp
L Conn_01x04 J?
U 1 1 5E2B1DD8
P 4050 5200
F 0 "J?" H 4050 5400 50  0000 C CNN
F 1 "Conn_01x04" H 4050 4900 50  0000 C CNN
F 2 "" H 4050 5200 50  0001 C CNN
F 3 "" H 4050 5200 50  0001 C CNN
	1    4050 5200
	0    -1   -1   0   
$EndComp
$Comp
L Conn_02x05_Counter_Clockwise J?
U 1 1 5E2B1E45
P 5400 1800
F 0 "J?" H 5450 2100 50  0000 C CNN
F 1 "Conn_02x05_Counter_Clockwise" H 5450 1500 50  0000 C CNN
F 2 "" H 5400 1800 50  0001 C CNN
F 3 "" H 5400 1800 50  0001 C CNN
	1    5400 1800
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5E2B1E75
P 2550 2000
F 0 "C?" H 2575 2100 50  0000 L CNN
F 1 "C" H 2575 1900 50  0000 L CNN
F 2 "" H 2588 1850 50  0001 C CNN
F 3 "" H 2550 2000 50  0001 C CNN
	1    2550 2000
	-1   0    0    1   
$EndComp
$Comp
L R R?
U 1 1 5E2B1EE0
P 2150 1850
F 0 "R?" V 2230 1850 50  0000 C CNN
F 1 "R" V 2150 1850 50  0000 C CNN
F 2 "" V 2080 1850 50  0001 C CNN
F 3 "" H 2150 1850 50  0001 C CNN
	1    2150 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 1850 2950 1850
Connection ~ 2550 1850
Text GLabel 2550 2150 3    60   Input ~ 0
pwm_gnd
Text GLabel 2000 1850 0    60   Input ~ 0
pwm_signal
Text GLabel 2950 1850 2    60   Input ~ 0
mfc_analog_setPoint
Text GLabel 2100 5400 3    60   Input ~ 0
serial_TX
Text GLabel 2200 5400 3    60   Input ~ 0
serial_RX
Text GLabel 1900 5400 3    60   Input ~ 0
pwm_signal
Text GLabel 2000 5400 3    60   Input ~ 0
pwm_gnd
Text GLabel 1700 5400 3    60   Input ~ 0
analog_signal
Text GLabel 1800 5400 3    60   Input ~ 0
analog_gnd
Text GLabel 3950 5400 3    60   Input ~ 0
24V_DC_Hot
Text GLabel 4050 5400 3    60   Input ~ 0
24V_DC_Hot
Text GLabel 4150 5400 3    60   Input ~ 0
24V_DC_gnd
Text GLabel 4250 5400 3    60   Input ~ 0
24V_DC_gnd
Text Notes 4350 2600 0    60   ~ 0
Not yet decided how connection from board to plug for MFC will work\nProbably agricultural connection to header from cable and plug
$EndSCHEMATC
