# Open Source ALD System

## Hardware
Custom PCB layouts based around Arduino Mega 2560 to control 16 solenoid valves, 4 vacuum gauges and 8 mass flow controllers

## Software
Arduino Firmware <br>
Python control software and GUI for creating, editing, and running processes.
